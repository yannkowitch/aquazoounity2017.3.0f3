﻿using System;
using UnityEngine;

public class WwiseManager : MonoBehaviour
{

    private Manager manager;
    private Setting setting;

    private void Start()
    {
        if (setting.ApplicationTyp == Setting.Application.Land) {
            AkSoundEngine.PostEvent("LandWind", gameObject);
        }
        else {
            AkSoundEngine.PostEvent("TiefseeUferAtmo", gameObject);
        }

    }
    private void Awake()
    {
        manager = Manager.Instance;
        setting = Setting.Instance;
    }

    private void OnEnable()
    {
        manager.OnPlayerPrefabDestroyed += OnPlayerPrefabDestroyed;
        manager.EnvironmentLevelHalfChanged += OnEnvironmentLevelHalfChanged;
        manager.TimerStarted += OnTimerStarted;
        manager.TimerEnded += OnTimerIsEnded;
        manager.EnvironmentLevelCompletelyChanged += OnEnvironmentLevelCompletelyChanged;
        //   if(setting.CurrentApplication == Setting.Application.TiefseeUfer)
        manager.TiktaalikCrawled += OnTiktaalikCrawled;
        manager.PederpesWalked += OnPederpesWalked;
        manager.UserCollisionEnter += OnUserCollisionEnter;
        manager.UserCollisionExit += OnUserCollisionExit;
    }

    private void OnDisable()
    {
        manager.OnPlayerPrefabDestroyed -= OnPlayerPrefabDestroyed;
        manager.EnvironmentLevelHalfChanged -= OnEnvironmentLevelHalfChanged;
        manager.TimerStarted -= OnTimerStarted;
        manager.TimerEnded -= OnTimerIsEnded;
        manager.EnvironmentLevelCompletelyChanged -= OnEnvironmentLevelCompletelyChanged;
        //    if (setting.CurrentApplication == Setting.Application.TiefseeUfer)
        manager.TiktaalikCrawled -= OnTiktaalikCrawled;
        manager.PederpesWalked -= OnPederpesWalked;
        manager.UserCollisionEnter -= OnUserCollisionEnter;
        manager.UserCollisionExit -= OnUserCollisionExit;
    }

    private void OnUserCollisionEnter(object sender, Manager.CollisionEventArgs e)
    {
        if (e.collisionEnter.transform.tag == "triggerButton") {
            AkSoundEngine.PostEvent("Button", gameObject);
        }
        else if (e.collisionEnter.transform.tag == "interactableRock") {
            AkSoundEngine.PostEvent("Stone", gameObject);
        }
    }
    private void OnUserCollisionExit(object sender, Manager.CollisionEventArgs e)
    {
        if (e.collisionExit.transform.tag == "triggerButton") {
            StopPlayingButtonSound();
        }

    }

    private void OnPederpesWalked(object sender, EventArgs e)
    {
        //  AkSoundEngine.PostEvent("TiktalikFootstep", gameObject);
        AkSoundEngine.PostEvent("Stone", gameObject);
    }

    private void OnTiktaalikCrawled(object sender, EventArgs e)
    {
        AkSoundEngine.PostEvent("TiktalikFootstep", gameObject);
    }

    private void OnEnvironmentLevelCompletelyChanged(object sender, Manager.EnvironmentLevelEventArgs e)
    {
        if (e.environmentLevel == Environment.Level.Ufer) {
            AkSoundEngine.SetRTPCValue("TiefseeUferFader", 100, gameObject);
        }
        else if (e.environmentLevel == Environment.Level.Tiefsee) {
            AkSoundEngine.SetRTPCValue("TiefseeUferFader", 0, gameObject);
        }
    }

    private void OnTimerIsEnded(object sender, EventArgs e)
    {
        AkSoundEngine.PostEvent("ClockTickingStop", gameObject);
    }

    private void OnTimerStarted(object sender, EventArgs e)
    {
        AkSoundEngine.PostEvent("ClockTicking", gameObject);

    }

    private void OnEnvironmentLevelHalfChanged(object sender, Manager.EnvironmentLevelEventArgs e)
    {
        AkSoundEngine.PostEvent("Transformation", gameObject);
    }



    private void OnPlayerPrefabDestroyed(object sender, Manager.CollisionEventArgs e)
    {
        StopPlayingButtonSound();
    }





    private void StopPlayingButtonSound()
    {
        AkSoundEngine.PostEvent("ButtonStop", gameObject);

    }
}
