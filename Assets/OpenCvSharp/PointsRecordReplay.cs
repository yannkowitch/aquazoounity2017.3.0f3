﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets;
using System.Xml.Serialization;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PointsRecordReplay : MonoBehaviour
{
    [SerializeField] private CorrespondenceAcquisition pointsHolder;
    private Manager manager;

    void Awake()
    {
        pointsHolder = FindObjectOfType<CorrespondenceAcquisition>() as CorrespondenceAcquisition;
        manager = Manager.Instance;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            RecordCurrentPoints();
            Debug.Log("Recording complete");
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            LoadSavedPoints();
            Debug.Log("Loading complete");
        }
    }

    private void LoadSavedPoints()
    {
        string fileName;
#if UNITY_EDITOR
        // fileName = EditorUtility.OpenFilePanel("Choose file path to load the recorded unity-3dworldPoints and projector-2dimagePoints", "", "xml");
        fileName = EditorUtility.OpenFilePanel("Choose file to calibrate Projector_Unity", "", "xml");

#endif
#if UNITY_EDITOR == false

        fileName = "recording.xml";
#endif

        var recording = Recording.LoadFromFile(fileName);
        pointsHolder.ReplayRecordedPoints(recording.worldPointsV3, recording.ImagePointsV3((double)Screen.width, (double)Screen.height));
    }

    private void RecordCurrentPoints()
    {
        string fileName;
#if UNITY_EDITOR
        // fileName = EditorUtility.SaveFilePanel("Choose the file path to save the unity-3dworldPoints and projector-2dimagePoints ", "", "pointsRecording", "xml");
        fileName = EditorUtility.SaveFilePanel("Choose the file to calibrate Kinect_Unity ", "", "pointsRecording", "xml");
#endif
#if UNITY_EDITOR == false
         
       fileName = "recording.xml";
#endif

        SaveToFile(pointsHolder.GetImagePoints(), pointsHolder.GetWorldPoints(), fileName);
    }

    private static void SaveToFile(List<Vector3> imagePoints, List<Vector3> worldPoints, string fileName)
    {
        var recording = new Recording(imagePoints, worldPoints, (double)Screen.width, (double)Screen.height);
        recording.SaveToFile(fileName);
    }

    public void SaveData()
    {
        RecordCurrentPoints();
        Debug.Log("Recording complete" + this.name);
    }

    public void LoadSavedRecordedPoints()
    {
        LoadSavedPoints();
        manager.OnSavedRecordedPointsLoaded();
 //       pointsHolder.ToggleCalibrating();
       

    }
}
