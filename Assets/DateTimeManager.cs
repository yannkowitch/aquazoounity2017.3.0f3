﻿using System;
using System.Collections;
using UnityEngine;
public class DateTimeManager : MonoBehaviour
{
    [SerializeField] private int _hour = 10;
    [SerializeField] private int _minute = 51;
    private bool quitApplication;
    private UI_ConfirmationHandler uI_ConfirmationHandler;
    private SceneFader sceneFader;
    [SerializeField] private int waitingTime = 30;

    private Manager manager;
    private Setting setting;

    public int WaitingTime
    {
        get
        {
            return waitingTime;
        }

        private set
        {
            waitingTime = value;
        }
    }

    private void Awake()
    {
        manager = Manager.Instance;
        setting = Setting.Instance;
    }
    // Use this for initialization
    private void Start()
    {
        uI_ConfirmationHandler = FindObjectOfType<UI_ConfirmationHandler>();
        sceneFader = FindObjectOfType<SceneFader>();
        if (uI_ConfirmationHandler == null) {

            Debug.LogError("no object of type ui-confirmation  founded");
        }
        if (sceneFader == null) {

            Debug.LogError("no object of type sceneFader  founded");
        }
        Debug.Log("call CheckTime Method in 15 seconds--------------");

        if (setting.AutoHerunterfahrenActiv == false) return;

        InvokeRepeating("CheckTime", 5, 60);
    }

    // Update is called once per frame
    private void CheckTime()
    {
        Debug.Log("execute CheckTime Method");
        if (quitApplication == true) return;

        var Now = DateTime.Now;

        if (Now.Hour.CompareTo(_hour) < 0) return;

        if (Now.Minute.CompareTo(_minute) < 0) return;


        manager.OnMaxTimeReached(_hour + ":" + _minute + " min");

        StartCoroutine(QuitApplicationIE());


        quitApplication = true;
    }

    private IEnumerator QuitApplicationIE()
    {

        uI_ConfirmationHandler.OnAppExitConfirmed(true);
        yield return new WaitForSeconds(waitingTime);

        StartCoroutine(sceneFader.QuitApplication());

    }
}
