﻿using UnityEngine;
using System.Collections;

public class AudioEngine : MonoBehaviour 
{

	public static AudioEngine Instance;
    private AudioSource[] sources;
    public AudioClip[] clips;	
	public float[] clipsVolume;
	
	private AudioSource[] backgroundSources;
    public AudioClip[] backgroundClips; //100  volume - 100% of the time
    public float[] backgroundClipsVolume;

    float[] parameterValuesCurrent;
	float[] parameterValuesTarget;

	public float speed = 1.0f;

	public bool debug = false;
	
	void Start () 
	{
		Instance = this;

		//dynamic
		sources = new AudioSource[clips.Length];
		parameterValuesCurrent = new float[clips.Length];
		parameterValuesTarget = new float[clips.Length];

		GameObject child = new GameObject ();
		child.transform.parent = this.gameObject.transform;
		child.name = "AudioSources";

		for(int i = 0;i<clips.Length;i++)
		{
			//create audiosource
			sources[i] = child.AddComponent<AudioSource>();
			sources[i].clip = clips[i];
			sources[i].loop = true;
			sources[i].volume = 1.0f;
			sources[i].playOnAwake = false;
			sources[i].Play();

			//create "database"
			parameterValuesTarget[i] = 0.0f;
			parameterValuesCurrent[i] = 0.0f;
		}

		//static
		backgroundSources = new AudioSource[backgroundClips.Length];		
		for(int i = 0;i<backgroundClips.Length;i++)
		{
			backgroundSources[i] = child.AddComponent<AudioSource>();
			backgroundSources[i].clip = backgroundClips[i];
			backgroundSources[i].loop = true;
			backgroundSources[i].volume = backgroundClipsVolume[i];
			backgroundSources[i].playOnAwake = false;
			backgroundSources[i].Play ();
		}
		
	}

	public void SetParameter(int _id, float _value)
	{
		parameterValuesTarget [_id] = Mathf.Clamp(_value,0.0f,1.0f);
	}

	public void DebugAudioEngine()
	{
		string s = "";
		for(int i = 0;i<sources.Length;i++)
		{
			s+= "(" + (int)(100*sources[i].volume) + "/"  + (int)(100*parameterValuesCurrent[i]) + "/" + (int)(100*parameterValuesTarget[i]) + ")\t";
		}
		Debug.Log (s);
	}
	
	void Update () 
	{
		if(debug)
			DebugAudioEngine ();

		for(int i = 0;i<sources.Length;i++)
		{					
			if(parameterValuesTarget[i]>parameterValuesCurrent[i])
			{
				parameterValuesCurrent[i] += speed * Time.deltaTime;
			}
			else if(parameterValuesTarget[i]<parameterValuesCurrent[i])
			{
				parameterValuesCurrent[i] -= speed * Time.deltaTime;
			}

			sources[i].volume = parameterValuesCurrent[i];
		}

		if(Input.GetKey(KeyCode.P))
		{
			for(int i = 0;i<sources.Length;i++)
			{
				sources[i].Play();
			}
		}

		if(Input.GetKey(KeyCode.R))
		{
			for(int i = 0;i<sources.Length;i++)
			{
				SetParameter(i,Random.Range(0.0f,1.0f));
			}
		}
	}
}
