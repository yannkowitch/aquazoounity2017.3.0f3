﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextTyper : MonoBehaviour
{
    private Coroutine m_TextTypingCoroutine = null;
    protected bool isTextTypeCompleted;
    protected TypeState typeState = TypeState.None;

    protected enum TypeState
    {
        Begin,
        Typing,
        Complete,
        None
    }

    public void TypeText(Text textComponent, string sentence)
    {
        if (m_TextTypingCoroutine != null)
        {
            StopCoroutine(m_TextTypingCoroutine);
            m_TextTypingCoroutine = null;
        }

        m_TextTypingCoroutine = StartCoroutine(TypeTextIE(textComponent, sentence));
    }

    private IEnumerator TypeTextIE(Text textComponent, string sentence)
    {
        textComponent.text = "";
        yield return new WaitForSeconds(0.25f);
        typeState = TypeState.Begin;
        foreach (char letter in sentence.ToCharArray())
        {
            textComponent.text += letter;
            typeState = (textComponent.text.Length == sentence.Length) ? TypeState.Complete : TypeState.Typing;          
            // wait a single frame
            //  yield return null;
            yield return new WaitForSeconds(0.1f);
        }
    }




    public void TypeText(TextMesh textComponent, string sentence)
    {
        if (m_TextTypingCoroutine != null)
        {
            StopCoroutine(m_TextTypingCoroutine);
            m_TextTypingCoroutine = null;
        }

        m_TextTypingCoroutine = StartCoroutine(TypeTextIE(textComponent, sentence));
    }

    private IEnumerator TypeTextIE(TextMesh textComponent, string sentence)
    {
       textComponent.text = "";
        yield return new WaitForSeconds(0.25f);
        typeState = TypeState.Begin;
        foreach (char letter in sentence.ToCharArray())
        {
            textComponent.text += letter;
            typeState = (textComponent.text.Length == sentence.Length) ? TypeState.Complete : TypeState.Typing;
            // wait a single frame
            //  yield return null;
            yield return new WaitForSeconds(0.1f);
        }
    }

}
