﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneFader : MonoBehaviour
{
    //http://gamedevelopertips.com/unity-how-fade-between-scenes/

    #region FIELDS
    [SerializeField] protected GameObject gameobject;
    private IClient iGameobj;

    #endregion
    #region MONOBHEAVIOR
    private void Awake()
    {
        iGameobj = new ImageFader(gameobject);
        iGameobj.fadeSpeed = 0.8f;
    }

    private void Start()
    {

    }

    void OnEnable()
    {
        StartCoroutine(Fade(Fader.FadeDirection.Out));
    }
    #endregion

    #region FADE
    private IEnumerator Fade(Fader.FadeDirection fadeDirection)
    {
        float alpha = (fadeDirection == Fader.FadeDirection.Out) ? 1 : 0;
        float fadeEndValue = (fadeDirection == Fader.FadeDirection.Out) ? 0 : 1;

        if (fadeDirection == Fader.FadeDirection.Out)
        {
            while (alpha >= fadeEndValue)
            {
                SetColorImage(ref alpha, fadeDirection);
                yield return null; // make the loop wait until the next frame before continuing
            }
            gameobject.GetComponent<Image>().enabled = false;
        }
        else if (fadeDirection == Fader.FadeDirection.In)
        {
            gameobject.GetComponent<Image>().enabled = true;
            while (alpha <= fadeEndValue)
            {
                SetColorImage(ref alpha, fadeDirection);
                yield return null;
            }
        }
    }
    #endregion

    #region HELPERS
    public IEnumerator FadeAndLoadScene(Fader.FadeDirection fadeDirection, string sceneToLoad)
    {
        yield return Fade(fadeDirection);
        SceneManager.LoadScene(sceneToLoad);
    }

    public IEnumerator FadeAndLoadScene(Fader.FadeDirection fadeDirection, int i)
    {
        yield return Fade(fadeDirection);
        SceneManager.LoadSceneAsync(i);
    }

    public IEnumerator QuitApplication()
    {
        yield return Fade(Fader.FadeDirection.In);

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
		    Application.Quit();
#endif
    }

    private void SetColorImage(ref float alpha, Fader.FadeDirection fadeDirection)
    {
        iGameobj.SetColorImage(ref alpha, fadeDirection);
    }

    /*
    private void SetColorImage(ref float alpha, Fader.FadeDirection fadeDirection)
    {
        if (fadeOutUIImage != null)
        {
            fadeOutUIImage.color = new Color(fadeOutUIImage.color.r, fadeOutUIImage.color.g, fadeOutUIImage.color.b, alpha);
            alpha += Time.deltaTime * (1.0f / iGameobj.fadeSpeed) * ((fadeDirection == Fader.FadeDirection.Out) ? -1 : 1);
        }
       
    }
    */
    #endregion
}
