﻿using UnityEngine;
using System.IO;
using Assets;
using System.Xml.Serialization;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class DataManager
{
    static string fileToLoadName;

    /// <summary>
    ///  This Method saves captured and displays the "save file" dialog calibrationdata .
    /// </summary>
    public static void SaveData(ProCamData calibrationData)
    {
        string fileName;

#if UNITY_EDITOR
        fileName = EditorUtility.SaveFilePanel("Choose the file to calibrate Kinect_Unity", "", "CapturedPoints", "xml");
#endif
#if UNITY_EDITOR == false
            fileName = "capturing.xml";
#endif


        //string fileName = EditorUtility.SaveFilePanel("Choose the file path", "", "CapturedPoints", "xml");
        File.WriteAllText(fileName, calibrationData.SerializeObject());

    }

    public static ProCamData LoadData()
    {
        // if (File.Exists("CapturedPoints.xml")) { }

#if UNITY_EDITOR
        if (fileToLoadName == null)

            fileToLoadName = EditorUtility.OpenFilePanel("Choose the file to calibrate Kinect_Unity", "", "xml");
        //fileToLoadName = EditorUtility.OpenFilePanel("Choose the file path to load the recorded unity-3dworldPoints and kinect-3dPoints", "", "xml");
        else
            Debug.Log("File already loaded");

#endif
#if UNITY_EDITOR == false        
            fileToLoadName = "capturing.xml";
        
              
#endif
      
        var data = File.ReadAllText(fileToLoadName);
        XmlSerializer xmlSerializer = new XmlSerializer(typeof(ProCamData));

        using (StringReader textReader = new StringReader(data))
        {
            ProCamData proCamData = (ProCamData)xmlSerializer.Deserialize(textReader);

            Manager.Instance.OnKinectUnityCalibrationDataLoaded(proCamData);
            return proCamData;
            
        }
        
    }
    /*
    public static ProCamData LoadData()
    {
        if (fileToLoadName == null)
        {
            fileToLoadName = EditorUtility.OpenFilePanel("Choose the file path", "", "xml");
        }
        else
        {
            Debug.Log("File already loaded");
        }

       
        var data = System.IO.File.ReadAllText(fileToLoadName);
        XmlSerializer xmlSerializer = new XmlSerializer(typeof(ProCamData));

        using (StringReader textReader = new StringReader(data))
        {
            return (ProCamData)xmlSerializer.Deserialize(textReader);
        }
        
    }
    */
}
