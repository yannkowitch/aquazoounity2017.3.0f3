﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RadialProgress : MonoBehaviour
{
    public GameObject LoadingText;
    public Text ProgressIndicator;
    public Image LoadingBar;
    float currentValue;
    public float speed;

    public float CurrentValue
    {
        set
        {
            currentValue = value;
            Debug.Log("halloooooooooooo");
        }
        get
        {
            return currentValue;
        }
    }
    // Use this for initialization

    Manager manager;

    private void Awake()
    {
        manager = Manager.Instance;
    }
    void Start()
    {

    }

    // Update is called once per frame
    public void IncrementValue()
    {
        //Debug.Log("UpdateRadialProgress");
        if (currentValue <= 100)
        {
            currentValue += speed * Time.deltaTime;
            manager.OnLoadBarIncrementing();
            ProgressIndicator.text = ((int)currentValue).ToString() + "%";
            LoadingText.SetActive(true);
        }
        else if (currentValue > 100)
        {
            LoadingText.SetActive(false);
            ProgressIndicator.text = "Done";
          //  manager.LoadingBarIsFilled(this);
        }

        LoadingBar.fillAmount = currentValue / 100;
      
        if (LoadingBar.fillAmount >= 1)
        {
           
            currentValue = 0;
            LoadingBar.fillAmount = 0;
            manager.OnLoadingBarFilled(this);
        }
        
       
    }

    public void DecrementValue()
    {
        //Debug.Log("Update2RadialProgress");
        if (currentValue < 100)
        {
            currentValue -= speed * Time.deltaTime;
            manager.OnLoadBarDecrementing();
            ProgressIndicator.text = ((int)currentValue).ToString() + "%";
            LoadingText.SetActive(true);

            if (currentValue <= 0)
            {
                currentValue = 0;
                gameObject.SetActive(false);
            }
           // LoadingBar.fillAmount = currentValue / 100;
        }
        LoadingBar.fillAmount = currentValue / 100;
    }
}
