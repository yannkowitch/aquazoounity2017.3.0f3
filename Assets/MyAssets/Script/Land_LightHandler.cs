﻿
using UnityEngine;

public class Land_LightHandler : MonoBehaviour
{
    [SerializeField] private Light _Light;
    [SerializeField] private float intensity = 2;

    private void Awake()
    {
        if (_Light == null)
            _Light = FindObjectOfType<Light>().GetComponent<Light>() as Light;
    }

    // Use this for initialization
    void Start()
    {
        SetLightIntensity( intensity);
        Debug.Log("intensity:" + intensity);
    }

    /// <summary>
    /// set the light intensity
    /// </summary>
    /// <param name="intensity"></param>
    /// 
    private void SetLightIntensity( float intensity)
    {
        _Light.intensity = intensity;
        
    }
}
