﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class KinectAvatarScript : MonoBehaviour
{
    KinectUnitySpaceCalibrator kinectUnityCoordinateMapper;
    [SerializeField] GameObject KinectAvatar;
    Manager manager;

    private void Awake()
    {
        kinectUnityCoordinateMapper = KinectUnitySpaceCalibrator.Instance;
        if (KinectAvatar == null)
            KinectAvatar = GameObject.FindGameObjectWithTag("kinectModel");
        manager = Manager.Instance;
    }
    private void OnEnable()
    {
        manager.CalibrationMatrixCalculated += OnCalibrationMatrixCalculated;
    }

    private void OnDisable()
    {
        manager.CalibrationMatrixCalculated -= OnCalibrationMatrixCalculated;
    }


    private void OnCalibrationMatrixCalculated(object sender, EventArgs e)
    {
        if (KinectAvatar == null) return;
        KinectAvatar.transform.localRotation = kinectUnityCoordinateMapper.GetKinectOrientation();
        KinectAvatar.transform.localPosition = kinectUnityCoordinateMapper.GetKinectPositionInUnity();
    }
 
}
