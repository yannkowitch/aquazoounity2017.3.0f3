﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalTracerManager : MonoBehaviour
{
    private GameObject target;
    private Manager manager;
    [SerializeField] private GameObject Tracker;

    private void Awake()
    {
        manager = Manager.Instance;
    }
    private void OnEnable()
    {
        manager.OnEusthenopteronCreated += OnEusthenopteronCreated;
        manager.OnEusthenopteronDestroyed += OnEusthenopteronDestroyed;
        manager.TiktalikCreated += OnTiktalikCreated;
        manager.TiktalikDestroyed += OnTiktalikDestroyed;
        manager.AnimalTrackerTriggerColliderEnter += OnAnimalTrackerTriggerColliderEnter;
        manager.AnimalTrackerTriggerColliderExit += OnAnimalTrackerTriggerColliderExit;
    }

  

    private void OnDisable()
    {
        manager.OnEusthenopteronCreated -= OnEusthenopteronCreated;
        manager.OnEusthenopteronDestroyed -= OnEusthenopteronDestroyed;
        manager.TiktalikCreated -= OnTiktalikCreated;
        manager.TiktalikDestroyed -= OnTiktalikDestroyed;

        manager.AnimalTrackerTriggerColliderEnter -= OnAnimalTrackerTriggerColliderEnter;
        manager.AnimalTrackerTriggerColliderExit -= OnAnimalTrackerTriggerColliderExit;
    }

    private void OnEusthenopteronDestroyed(object sender, Manager.EusthenopteronEventArgs e)
    {
        target = null;
    }

    private void OnEusthenopteronCreated(object sender, Manager.EusthenopteronEventArgs e)
    {
        target = e.Eusthenopteron_Gameobject;
    }

    private void OnTiktalikDestroyed(object sender, Manager.TiktalikEventArgs e)
    {
        target = null;
    }

    private void OnTiktalikCreated(object sender, Manager.TiktalikEventArgs e)
    {
        target = e.Tiktalik_Gameobject;
    }

    private void Update()
    {
        if (target != null)
            Tracker.transform.position = new Vector3(target.transform.position.x, Tracker.transform.position.y, target.transform.position.z);
    }

    private void OnAnimalTrackerTriggerColliderExit(object sender, Manager.TriggerEventArgs e)
    {
        if (e.otherCollider.gameObject.tag != "AnimalPositionEventTrigger") return;
        if (target.name.Equals("Eusthenopteron"))
            manager.OnEusthenopteronAwayFromTheStairs(target);

        else if (target.name.Equals("Tiktalik"))
            manager.OnTiktalikAwayFromTheStairs(target);
    }

    private void OnAnimalTrackerTriggerColliderEnter(object sender, Manager.TriggerEventArgs e)
    {
        if (e.otherCollider.gameObject.tag != "AnimalPositionEventTrigger") return;
        if (target.name.Equals("Eusthenopteron"))
            manager.OnEusthenopteronUnderTheStairs(target);

        else if (target.name.Equals("Tiktalik"))
            manager.OnTiktalikUnderTheStairs(target);
    }

    
}
