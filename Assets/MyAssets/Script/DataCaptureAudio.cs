﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class DataCaptureAudio : MonoBehaviour
{

    public AudioClip captureSong;
    private AudioSource audioSource;
    Manager manager;

    // Use this for initialization
    private void Awake()
    {
        manager = Manager.Instance;
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        manager.PointCaptured += OnPointCaptured;
    }

    private void OnDisable()
    {
        manager.PointCaptured -= OnPointCaptured;
    }

    private void OnPointCaptured(object sender, EventArgs e)
    {
        Debug.Log("Play song");
        PlayCaptureSong();
    }

    private void PlayCaptureSong()
    {
        audioSource.PlayOneShot(captureSong);
    }
}
