﻿
using UnityEngine;
using UnityEngine.UI;
public class ImageFader : IClient
{
    [SerializeField] private Image fadeOutUIImage;
    public float fadeSpeed { set; get; }


    public ImageFader(GameObject gameobject)
    {
        fadeOutUIImage = gameobject.GetComponent<Image>();
    }

    void IClient.SetColorImage(ref float alpha, Fader.FadeDirection fadeDirection)
    {
        if (fadeOutUIImage != null)
        {
            fadeOutUIImage.color = new Color(fadeOutUIImage.color.r, fadeOutUIImage.color.g, fadeOutUIImage.color.b, alpha);
            alpha += Time.deltaTime * (1.0f / fadeSpeed) * ((fadeDirection == Fader.FadeDirection.Out) ? -1 : 1);
        }
    }

}
