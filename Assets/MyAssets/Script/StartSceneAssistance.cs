﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class StartSceneAssistance : CalibrationAssistanceBase
{
    [SerializeField] private float delay = 0.4f;
    private MainMenu MainMenu;
    private bool isApplySettingMessageDisplayed;
 

    private void Awake()
    {
        MainMenu = FindObjectOfType<MainMenu>();
        AwakeBase();
    }

    private void OnEnable()
    {
        manager.TextTypingEnded += OnTextTypingEnded;
        manager.TextTypingBegan += OnTextTypingBegan;
        manager.AnySettingsValueChanged += OnAnySettingsValueChanged;
    }

    private void OnDisable()
    {
        manager.AnySettingsValueChanged -= OnAnySettingsValueChanged;
        manager.TextTypingEnded -= OnTextTypingEnded;
        manager.TextTypingBegan -= OnTextTypingBegan;
    }
    // string typedText;
    private void OnTextTypingEnded(object sender, TextTypingEventArgs e)
    {
    }

    private void OnTextTypingBegan(object sender, TextTypingEventArgs e)
    {
        //throw new NotImplementedException();
    }

    public void OnSettingApplyButtonClicked()
    {
        isApplySettingMessageDisplayed = false;
        dialogAssistance.TypeText(instructions[6], delay);
    }

    public void OnCalibrationButtonClicked()
    {
        dialogAssistance.ActivateDialogCanvas(false, 0.1f);
    }

    public void OnSimulationButtonClicked()
    {
        dialogAssistance.ActivateDialogCanvas(false, 0.1f);
    }

    private void OnAnySettingsValueChanged(object sender, EventArgs e)
    {
        if (isApplySettingMessageDisplayed == true)
        {
            Debug.Log("Apllysetting message is already displayed");
            return;
        }
        dialogAssistance.TypeText(instructions[5], delay);
        isApplySettingMessageDisplayed = true;
    }


    public void OnCalibrationButtonPointerEnter()
    {
        if (MainMenu.isSettingsPanelOpen == true) return;
        dialogAssistance.TypeText(instructions[1], delay);
    }

    public void OnCalibrationButtonPointerExit()
    {
        dialogAssistance.TypeText("", delay);
    }

    public void OnSimulationButtonPointerEnter()
    {
        if (MainMenu.isSettingsPanelOpen == true) return;
        dialogAssistance.TypeText(instructions[2], delay);
    }

    public void OnSimulationButtonPointerExit()
    {
        dialogAssistance.TypeText("", delay);
    }

    public void OnSettingButtonPointerEnter()
    {
        if (MainMenu.isSettingsPanelOpen == true) return;
        dialogAssistance.TypeText(instructions[3], delay);
    }

    public void OnSettingButtonPointerExit()
    {
        dialogAssistance.TypeText("", delay);
    }

    public void OnExitButtonPointerEnter()
    {
        if (MainMenu.isSettingsPanelOpen == true) return;
        dialogAssistance.TypeText(instructions[4], delay);
    }

    public void OnExitButtonPointerExit()
    {
        dialogAssistance.TypeText("", delay);
    }

    /*
    private void OnTextTypingBegan(object sender, EventArgs e)
    {
        Debug.Log("TextTyping Began");
        
    }

    private void OnTextTypingEnded(object sender, EventArgs e)
    {
        Debug.Log("TextTyping Ended");
        
    }
    */
    private void Start()
    {
        StartBase();
        dialogAssistance.TypeText(instructions[0], 1f);
    }


}
