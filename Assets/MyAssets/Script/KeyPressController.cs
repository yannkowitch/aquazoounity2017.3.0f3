﻿using UnityEngine;

public class KeyPressController : MonoBehaviour
{
    [SerializeField] GameObject DeveloperPanel;
    private int keyPressCounter = 0;
    private float time = 0;
    [SerializeField] [Range(2, 7)] int tapAmountToEnablePanel;
    private MainMenu MainMenu;


    private void Awake()
    {
        MainMenu = FindObjectOfType<MainMenu>() as MainMenu;
    }


    private void Update()
    {
        if (MainMenu.isSettingsPanelOpen == true)
        {
            if (DeveloperPanel == null) throw new System.Exception("no  DeveloperPanel assigned");
            DeveloperPanel.SetActive(true);

            if (DeveloperPanel.activeInHierarchy == true)
                Manager.Instance.OnSettingPanelOpened();
        }
        else if (MainMenu.isSettingsPanelOpen == false)
        {
            if (DeveloperPanel == null) throw new System.Exception("no  DeveloperPanel assigned");
            DeveloperPanel.SetActive(false);

            if (DeveloperPanel.activeInHierarchy == false)
                Manager.Instance.OnSettingPanelClosed();
        }

        //TapMoreTime();

    }

    /*
    void TapMoreTime()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            keyPressCounter++;
            if (keyPressCounter == tapAmountToEnablePanel)
            {
                Debug.Log(keyPressCounter);
                if (DeveloperPanel == null) throw new System.Exception("no  DeveloperPanel assigned");
                DeveloperPanel.SetActive(true);

                if (DeveloperPanel.activeInHierarchy == true)
                    Manager.Instance.SettingPanelIsOpened();
            }

        }
        while (keyPressCounter != 0)
        {
            time += Time.deltaTime;

            if (time < (0.5 * tapAmountToEnablePanel) / 2) return;
            //  Debug.Log(time);
            time = 0;
            keyPressCounter = 0;
            break;

        }
        */
}

