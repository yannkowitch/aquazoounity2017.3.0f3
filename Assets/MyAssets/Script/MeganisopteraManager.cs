﻿
using System;
using System.Collections;
using UnityEngine;

public class MeganisopteraManager : MonoBehaviour
{
    [SerializeField] public GameObject[] MeganisopterTargets;
    [SerializeField] private GameObject MeganisopterPrefab;
    public float flySpeed { private set; get; }
    [SerializeField] float initialSpeed;
    [SerializeField] float flyAwaySpeed;
    private GameObject meganisopter;
    public bool fly { set; get; }
    public int targetIndex;
    public float rotationSpeed;
    public bool isFollowTarget { set; get; }
    private Manager manager;
    private Setting setting;

    bool isTiefSeeTiktalik;

    [Header("waitingtimeRange for spawning Meganisoptera by start")]
    [Range(0.0f, 7f)] [SerializeField] float MinTime;
    [Range(7f, 15.0f)] [SerializeField] float MaxTime;

    [Header("waitingtimeRange for spawning Meganisoptera ")]
    [Range(0.0f, 15.0f)] [SerializeField] float minTime;
    [Range(15, 40.0f)] [SerializeField] float maxTime;

    [Header("waitingtimeRange for reseting TargetPosition ")]
    [Range(0.0f, 5.0f)] [SerializeField] float min_Time;
    [Range(5, 25.0f)] [SerializeField] float max_Time;
    Coroutine SpawnMeganisopteraCoroutine;
    Coroutine ResetFollowTargetCoroutine;

    // Use this for initialization
    /*
    private IEnumerator Start()
    {
        Debug.Log("CurrentProjection: "+setting.CurrentProjection);

        if (setting.CurrentProjection == Setting.Projection.Land) {
         
            yield return new WaitForSeconds(UnityEngine.Random.Range(MinTime, MaxTime));
            //Spawn(" Meganisoptera", MeganisopterPrefab.transform.position, Quaternion.identity);
            SpawnMeganisoptera();
        }

       
    }*/
    private void Awake()
    {
        manager = Manager.Instance;
        setting = Setting.Instance;
        //  InitializeMeganisopteraSettings();
    }

    private void Start()
    {
        Debug.Log("CurrentProjection: " + setting.ApplicationTyp);
        InvokeRepeating("SetNextPoint", 0, UnityEngine.Random.Range(2, 7));

        if (setting.ApplicationTyp == Setting.Application.Land)
        {
            SpawnMeganisopter();
        }
    }

    private void OnEnable()
    {
        manager.OnMeganisopterCreated += OnMeganisopterCreated;
        manager.OnMeganisopterDestroyed += OnMeganisopterDestroyed;
        manager.UserTriggerEnter += OnUserTriggerEnter;       
        manager.TiktalikCreated += OnTiktalikCreated;
        manager.TiktalikDestroyed += OnTiktalikDestroyed;
    }

    private void OnDisable()
    {
        manager.OnMeganisopterCreated -= OnMeganisopterCreated;
        manager.OnMeganisopterDestroyed -= OnMeganisopterDestroyed;
        manager.UserTriggerEnter -= OnUserTriggerEnter;
        manager.TiktalikCreated -= OnTiktalikCreated;
        manager.TiktalikDestroyed -= OnTiktalikDestroyed;
    }
   
    private IEnumerator ResetFollowTargetIE(float time)
    {
        yield return new WaitForSeconds(time);
        isFollowTarget = false;
    }

    private void ResetFollowTarget()
    {
       
        if (ResetFollowTargetCoroutine != null)
        {
            StopCoroutine(ResetFollowTargetCoroutine);
            ResetFollowTargetCoroutine = null;
        }

        float time = UnityEngine.Random.Range(min_Time, max_Time);
        ResetFollowTargetCoroutine = StartCoroutine(ResetFollowTargetIE(time));
    }

    private void OnMeganisopterCreated(object sender, Manager.MeganisopterEventArgs e)
    {
        ResetFollowTarget();

        if (setting.ApplicationTyp != Setting.Application.TiefseeUfer) return;
        if (isTiefSeeTiktalik) return;
    
        Destroy(meganisopter);

    }

    private void SpawnMeganisopter()
    {
        InitializeMeganisopteraSettings();
       
        if (SpawnMeganisopteraCoroutine != null)
        {
            StopCoroutine(SpawnMeganisopteraCoroutine);
            SpawnMeganisopteraCoroutine = null;
        }
        float time = UnityEngine.Random.Range(min_Time, max_Time);
        SpawnMeganisopteraCoroutine = StartCoroutine(SpawnMeganisopteraIE(time));
    }

    private void OnMeganisopterDestroyed(object sender, Manager.MeganisopterEventArgs e)
    {
        if (setting.ApplicationTyp == Setting.Application.TiefseeUfer)
        {
            if (isTiefSeeTiktalik == false) return;
        }

        SpawnMeganisopter();
    }

    private void OnUserTriggerEnter(object sender, Manager.TriggerEventArgs e)
    {
        if (e.triggerEnter.gameObject.tag == "MeganisopteraSensor")
        {
            Destroy(e.triggerEnter.gameObject);
            MeganisopteraMoveForward(flyAwaySpeed);
        }
    }

    private void OnTiktalikDestroyed(object sender, Manager.TiktalikEventArgs e)
    {
        if (setting.ApplicationTyp == Setting.Application.Land) return;
        isTiefSeeTiktalik = false;
        MeganisopteraMoveForward(flyAwaySpeed);
    }

    private void OnTiktalikCreated(object sender, Manager.TiktalikEventArgs e)
    {
        if (setting.ApplicationTyp == Setting.Application.Land) return;
        isTiefSeeTiktalik = true;
        SpawnMeganisopter();

    }


    private void MeganisopteraMoveForward(float _flyAwaySpeed)
    {
        isFollowTarget = false;
        flySpeed = _flyAwaySpeed;
    }

    private void MeganisopteraMoveTowardTarget(float _flyAwaySpeed)
    {
        isFollowTarget = true;
        flySpeed = _flyAwaySpeed;
    }
    /*
    private void SpawnMeganisoptera()
    {
        Spawn("Meganisoptera", MeganisopterPrefab.transform.position, Quaternion.identity);
    }
    */
    private IEnumerator SpawnMeganisopteraIE(float time)
    {
        yield return new WaitForSeconds(time);
    //    Spawn("Meganisoptera", MeganisopterPrefab.transform.position, Quaternion.identity);

        if (meganisopter == null)
        {
            meganisopter = Instantiate(MeganisopterPrefab, MeganisopterPrefab.transform.position, Quaternion.identity);
            //    Debug.Log(" meganispter is spawned");
            int scale = UnityEngine.Random.Range(3, 5);
            meganisopter.transform.localScale = new Vector3(scale, scale, scale);
            meganisopter.name = "Meganisoptera";
        }
        else
        {
            Debug.Log("there is a Meganispter Instance already");
        }
    }

    private void SetNextPoint()
    {
        int newTargetIndex = UnityEngine.Random.Range(0, MeganisopterTargets.Length);

        while (targetIndex == newTargetIndex)
        {
            newTargetIndex = UnityEngine.Random.Range(0, MeganisopterTargets.Length);
        }

        targetIndex = newTargetIndex;

        if (targetIndex >= MeganisopterTargets.Length)
            targetIndex = targetIndex % MeganisopterTargets.Length;

    }

    private void InitializeMeganisopteraSettings()
    {
        fly = true;
        //isFollowTarget = true;
        MeganisopteraMoveTowardTarget(initialSpeed);

        targetIndex = UnityEngine.Random.Range(0, MeganisopterTargets.Length);
        //  InvokeRepeating("SetNextPoint", 0, UnityEngine.Random.Range(2, 7));
    }
    /*
    private void Spawn(string name, Vector3 position, Quaternion rotation)
    {
        if (meganisopter == null)
        {
            meganisopter = Instantiate(MeganisopterPrefab, position, rotation);
            //    Debug.Log(" meganispter is spawned");
            int scale = UnityEngine.Random.Range(3, 5);
            meganisopter.transform.localScale = new Vector3(scale, scale, scale);
            meganisopter.name = name;
        }
        else
        {
            Debug.Log("there is a Meganispter Instance already");
        }
    }
    */
}
