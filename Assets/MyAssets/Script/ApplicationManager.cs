﻿using UnityEngine;

public class ApplicationManager : MonoBehaviour
{
    private bool mChangeLevel = true;

    // Coroutine m_ActivationConfirmation = null;


    public void Quit()
    {
        Debug.Log("quit");
        if (mChangeLevel)
        {
            StartCoroutine(GameObject.FindObjectOfType<SceneFader>().QuitApplication());
            /*
            #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
            #else
		        Application.Quit();
            #endif
            */
            mChangeLevel = false;
            FindObjectOfType<SettingController>().DesastivateDevelopperPanel();
        }

    }
}
