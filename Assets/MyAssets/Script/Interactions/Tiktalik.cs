﻿using System.Collections;
using UnityEngine;


public class Tiktalik : MonoBehaviour
{
    private TiktalikManager tiktalikManager;
    public Rigidbody rb;
    private bool isApplicationQuitting = false;
    private bool isdestroyable;
    public static int counter = 0;
    public EnvironmentState environmentState { get; private set; }
    //    private bool colEnter, colExit;
    private Manager manager;
    private Animator animator;

    private void Awake()
    {
        manager = Manager.Instance;
        animator = this.GetComponent<Animator>();
    }

    // Use this for initialization
    private IEnumerator Start()
    {
        counter = 0;
        isdestroyable = false;
        tiktalikManager = FindObjectOfType<TiktalikManager>();
        rb = GetComponent<Rigidbody>();
        manager.OnTiktalikCreated(this);

        yield return new WaitForSeconds(3);
        isdestroyable = true;

    }


    private void FixedUpdate()
    {
        if (tiktalikManager.crawl || tiktalikManager.swim)
            Moveforward(tiktalikManager.speed);


    }
    private void Update()
    {
        animator.SetBool("crawl", tiktalikManager.crawl);
        animator.SetBool("swim", tiktalikManager.swim);
    }
    private void Moveforward(float movespeed)
    {
        rb.MovePosition(transform.position - transform.forward * movespeed * Time.deltaTime);
    }

    private void OnApplicationQuit()
    {
        isApplicationQuitting = true;
    }

    private void OnDestroy()
    {
        if (!isApplicationQuitting)
            manager.OnTiktalikDestroyed(this);
    }

    private void OnTriggerEnter(Collider otherCollider)
    {
        if (otherCollider.gameObject.tag == "animalDestroyerCollider")
        {
            if (!isdestroyable) return;

            Destroy(this.gameObject);

            Debug.Log("tiktalik Trigger Entered " + otherCollider.gameObject.name);
        }
        else if (otherCollider.gameObject.tag == "EnvironmentSensor")
        {
            Debug.Log("tiktalik Trigger Entered " + otherCollider.gameObject.name);
            environmentState = EnvironmentState.Water;
            //  if (!colEnter) {
            counter++;
            Debug.Log("counter:" + counter);
            //colEnter = true;
            //  }

            manager.OnTiktalikEnvironmentStateChanged(environmentState);
            //  colEnter = true;
        }


    }

    private void OnTriggerExit(Collider otherCollider)
    {
        if (otherCollider.gameObject.tag == "EnvironmentSensor")
        {
            Debug.Log("tiktalik Trigger Exited " + otherCollider.gameObject.name);
            environmentState = EnvironmentState.Land;
            counter++;
            Debug.Log("counter:" + counter);
            manager.OnTiktalikEnvironmentStateChanged(environmentState);
        }
    }

    public enum EnvironmentState
    {
        Water,
        Land,
        None
    }

    public void OnTiktaalikCrawled(int value)
    {
        if (value == 1)
        {
            manager.OnTiktaalikCrawled();
        }
    }

}
