﻿using System.Collections;
using UnityEngine;

public class Eusthenopteron : MonoBehaviour
{
    private EusthenopteronManager eusthenopteronManager;
    private bool isApplicationQuitting = false;
    private bool isdestroyable;
    private Manager manager;

    private void Awake()
    {
        manager = Manager.Instance;
    }

    // Use this for initialization
    private IEnumerator Start()
    {
        isdestroyable = false;
        eusthenopteronManager = FindObjectOfType<EusthenopteronManager>();
        manager.EusthenopteronIsCreated(this);
        yield return new WaitForSeconds(3);
        isdestroyable = true;
    }
    private void Turn()
    {
        Vector3 direction = (eusthenopteronManager.eusthenopteronTarget.transform.position - this.transform.position).normalized;
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), eusthenopteronManager.rotationSpeed * Time.deltaTime);
    }

    private void SwimForward()
    {
        this.transform.Translate(Vector3.forward * Time.deltaTime * eusthenopteronManager.translationSpeed, Space.Self);
    }

    // Update is called once per frame
    private void Update()
    {
        if (!eusthenopteronManager.move) return;
        SwimForward();
        if (!eusthenopteronManager.isTurning) return;
        eusthenopteronManager.SetTargetYPosition();
        Turn();
    }

    private void RandomPositionY()
    {
        float randomize = Random.Range(0, 2500);
        if (randomize < 10)
        {
            eusthenopteronManager.eusthenopteronTarget.transform.position = new Vector3(eusthenopteronManager.eusthenopteronTarget.transform.position.x, Random.Range(eusthenopteronManager.minDepht, eusthenopteronManager.maxDepht), eusthenopteronManager.eusthenopteronTarget.transform.position.z);
        }
    }

    private void OnApplicationQuit()
    {
        isApplicationQuitting = true;
    }

    private void OnDestroy()
    {
        if (!isApplicationQuitting)
            manager.EusthenopteronIsDestroyed(this);
    }

    private void OnTriggerEnter(Collider otherCollider)
    {
        if (otherCollider.gameObject.tag != "animalDestroyerCollider") return;
        if (!isdestroyable) return;

        Destroy(this.gameObject);


    }
}
