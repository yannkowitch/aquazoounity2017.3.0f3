﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GlobalFlockManager : MonoBehaviour
{
    #region MEMBER_VARIABLES
    public int numFish = 10;
    public int tanksize = 10;

    [Header("Fish Settings")]
    [Range(0.0f, 5.0f)]
    public float minSpeed = 3.0f;
    [Range(0.0f, 10.0f)]
    public float maxSpeed = 5.0f;

    [Range(0.0f, 10.0f)]
    public float rotationSpeed = 3.0f;
    [Range(1.0f, 10.0f)]
    public float neighbourDistance = 10.0f;
    public float FishAvoidDistance = 1;
    [SerializeField] public GameObject[] flockPrefabs;
    public float animSpeed = 1;
    private GameObject flockPrefab;
    public GameObject FlocksContainer;
    public GameObject flockTarget;

    [SerializeField] private int depth = -5;
    private bool userFootPrefabInstantiated;
    private bool mousePrefabCreated;
    private Vector3 leftFootPositionUnity;
    private Vector3 mousetPosition;
    private Vector3 playerPosition;
    private bool isHalfTransitionTimeReached;
    private Environment.Level environmentState;

    Manager manager;
    Setting setting;

    #endregion // MEMBER_VARIABLES

    #region  EIGENTSCHAFTENSMETHODEN
    public GameObject[] AllFish { get; private set; }

    public Vector3 GoalPos { get; private set; }

    public int Tanksize
    {
        get
        {
            return tanksize;
        }
    }
    #endregion  GETTER AND SETTER


    private void Awake()
    {
        manager = Manager.Instance;
        setting = Setting.Instance;
    }

    private void Start()
    {
        if (flockTarget == null)
            flockTarget = GameObject.FindGameObjectWithTag("fishTarget");

        flockPrefab = flockPrefabs[UnityEngine.Random.Range(0, 2)];

        GoalPos = flockTarget.transform.position;

        numFish = setting.FishAmount;

        AllFish = new GameObject[numFish];

        for (int i = 0; i < numFish; i++)
        {
            Vector3 position = new Vector3(UnityEngine.Random.Range(-tanksize, tanksize), depth, UnityEngine.Random.Range(-tanksize, tanksize));
            SpawnFish(i, flockPrefab, position);
        }

    }

    private void SpawnFish(int i, GameObject fishPrefab, Vector3 position)
    {
        GameObject fish = (GameObject)Instantiate(fishPrefab, position, Quaternion.identity);
        fish.name = "Fish " + (i + 1);
        fish.transform.SetParent(FlocksContainer.transform, false);
        AllFish[i] = fish;
    }

    private void OnEnable()
    {
        manager.EnvironmentLevelCompletelyChanged += OnEnvironmentLevelCompletelyChanged;
        manager.EnvironmentLevelHalfChanged += OnEnvironmentLevelHalfChanged;
        manager.PlayerPrefabMouseCreated += OnPlayerPrefabMouseCreated;
        manager.PlayerPrefabMouseDestroyed += OnPlayerPrefabMouseDestroyed;
        manager.leftFootTracked += OnleftFootTracked;
        manager.PlayerPrefabLeftFootInstantiated += OnPlayerPrefabLeftFootInstantiated;
        manager.PlayerPrefabLeftDestroyed += OnPlayerPrefabLeftDestroyed;
        //EventsManager.OnBodyLost += _OnUserBodyLost;
        manager.BodyLost += OnBodyLost;
    }

    private void OnDisable()
    {
        manager.EnvironmentLevelCompletelyChanged -= OnEnvironmentLevelCompletelyChanged;
        manager.EnvironmentLevelHalfChanged -= OnEnvironmentLevelHalfChanged;
        manager.PlayerPrefabMouseCreated -= OnPlayerPrefabMouseCreated;
        manager.PlayerPrefabMouseDestroyed -= OnPlayerPrefabMouseDestroyed;
        manager.leftFootTracked -= OnleftFootTracked;
        manager.PlayerPrefabLeftFootInstantiated -= OnPlayerPrefabLeftFootInstantiated;
        manager.PlayerPrefabLeftDestroyed -= OnPlayerPrefabLeftDestroyed;
        // EventsManager.OnBodyLost -= _OnUserBodyLost;
        manager.BodyLost -= OnBodyLost;

    }

    private void OnBodyLost(object sender, Manager.KinectSensorEventArgs e)
    {
        if (e.usersAmount == 0)
        {
            userFootPrefabInstantiated = false;
        }
    }

    private void OnPlayerPrefabLeftDestroyed(object sender, Manager.PlayerPrefabFootEventArgs e)
    {
        userFootPrefabInstantiated = false;
    }

    private void OnPlayerPrefabLeftFootInstantiated(object sender, Manager.PlayerPrefabFootEventArgs e)
    {
        userFootPrefabInstantiated = true;
    }

    private void OnleftFootTracked(object sender, Manager.KinectSensorEventArgs e)
    {
        // _OnleftFootTracked(e.leftFootPositionInUnity, e.userIndex);
        this.leftFootPositionUnity = KinectUnitySpaceCalibrator.Instance.MapKinect3DPointToUnity3DPoint(e.leftFootPositionInUnity);
    }


    #region  EVENTHADLERS
    /*
    private void _OnleftFootTracked(Vector3 userKinectleftFootPosition, int userID)
    {
        this.leftFootPositionUnity = KinectUnityCoordinateMapper.singleton.MapKinect3DPointToUnity3DPoint(userKinectleftFootPosition);
    }
    
    private void _OnPlayerPrefabLeftFootInstantiated(int userIndex)
    {
        userFootPrefabInstantiated = true;
    }
  
    private void _OnPlayerPrefabLeftDestroyed(int userIndex)
    {
        userFootPrefabInstantiated = false;
    }
     
    private void OnPlayerMouseCreated(Vector3 playerPrefabMousePosition)
    {
        mousePrefabCreated = true;
        mousetPosition = playerPrefabMousePosition;
    }

    private void OnPlayerMouseDestroyed(Vector3 playerPrefabMousePosition)
    {
        mousePrefabCreated = false;
    }

    private void _OnUserBodyLost(int userAmount, int userIndex)
    {
        if (userAmount == 0)
        {
            userFootPrefabInstantiated = false;
        }
    }
     */
    private void OnPlayerPrefabMouseDestroyed(object sender, Manager.PlayerPrefabMouseEventArgs e)
    {
        //OnPlayerMouseDestroyed(e.PlayerPrefabClone.transform.position);
        mousePrefabCreated = false;
    }

    private void OnPlayerPrefabMouseCreated(object sender, Manager.PlayerPrefabMouseEventArgs e)
    {
        mousePrefabCreated = true;
        mousetPosition = e.PlayerPrefabClone.transform.position;
    }

    private void OnEnvironmentLevelHalfChanged(object sender, Manager.EnvironmentLevelEventArgs wp)
    {
        isHalfTransitionTimeReached = true;
    }

    private void OnEnvironmentLevelCompletelyChanged(object sender, Manager.EnvironmentLevelEventArgs wp)
    {
        environmentState = wp.environmentLevel;
        isHalfTransitionTimeReached = false;
    }

    #endregion  EVENTHADLERS

    // Muss mit laufender Kinect getestet werden. von P_0 erfolgt sprung auf p_2, dann wird normal gezählt. Bei Kinect auch?
    private void Update()
    {
        switch (environmentState)
        {
            case Environment.Level.Tiefsee:
                if (isHalfTransitionTimeReached)
                {
                    SwimAway();
                    // Debug.Log("swimm awy");
                }
                else
                {
                    if (userFootPrefabInstantiated || mousePrefabCreated)
                    {
                        if (userFootPrefabInstantiated)
                            FollowUser(leftFootPositionUnity);

                        if (mousePrefabCreated)
                            FollowUser(mousetPosition);
                    }
                    else if (!userFootPrefabInstantiated && !mousePrefabCreated)
                    {
                        SwimRandom();
                    }
                }

                break;
            case Environment.Level.Ufer:
                SwimAway();
                break;
        }

    }

    private void SwimAway()
    {
        GoalPos = new Vector3(GoalPos.x, GoalPos.y, -tanksize);
        flockTarget.transform.position = GoalPos;
    }

    private void SwimRandom()
    {
        float randomize = UnityEngine.Random.Range(0, 4000);
        if (randomize < 50)
        {
            // flockTargetPosition = new Vector3(Random.Range(-tanksize, tanksize), depth, Random.Range(-tanksize, tanksize));
            GoalPos = new Vector3(UnityEngine.Random.Range(-tanksize, tanksize), UnityEngine.Random.Range(depth, -1f), UnityEngine.Random.Range(-tanksize, tanksize));
            flockTarget.transform.position = GoalPos;
        }
    }

    private void FollowUser(Vector3 leftFootPositionUnity)
    {
        GoalPos = new Vector3(leftFootPositionUnity.x, UnityEngine.Random.Range(depth, -1f), leftFootPositionUnity.z); //-4
        flockTarget.transform.position = GoalPos;
    }

}
