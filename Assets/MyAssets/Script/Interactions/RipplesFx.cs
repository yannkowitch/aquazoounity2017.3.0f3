﻿using System;
using UnityEngine;

public class RipplesFx : MonoBehaviour
{
    #region MEMBER_VARIABLES
    // private Transform suimono_Surface;
    [SerializeField] private GameObject playerPrefab;
    private const int MAX_TRACKABLE_USERS = 6;
    public GameObject[] playerPrefabLeftFoots { get; private set; }
    public GameObject[] playerPrefabRightFoots { get; private set; }
    public FloorInterface iFloor;
    private Manager manager;
    private Setting setting;
    private double floortoJointMinDistance; //0.01 meter
    private KinectUnitySpaceCalibrator KinectUnityCoordinateMapperSingleton;

    #endregion // MEMBER_VARIABLES


    private void Awake()
    {
        manager = Manager.Instance;
        setting = Setting.Instance;

        floortoJointMinDistance = setting.FloortoFootMinDistance;

        if (manager == null) throw new Exception("no manager");

        KinectUnityCoordinateMapperSingleton = KinectUnitySpaceCalibrator.Instance;
        if (!KinectUnityCoordinateMapperSingleton) throw new Exception("no KinectUnityCoordinateMapperSingleton");

    }

    private void Start()
    {

        iFloor = FindObjectOfType<FloorManager>().iFloor;

        if (iFloor == null) throw new Exception("no iFloor");

        if (iFloor != null) {
            Debug.Log("height: " + iFloor.GetFloorToSensorDistance());
            Debug.Log("DistanceFromZero: " + iFloor.GetFloorTo3DPointDistance(Vector3.zero));
            iFloor.GetFloorTo3DPointDistance(Vector3.zero);
        }


        playerPrefabLeftFoots = new GameObject[MAX_TRACKABLE_USERS];
        playerPrefabRightFoots = new GameObject[MAX_TRACKABLE_USERS];

    }

    private void OnEnable()
    {
        manager.leftFootTracked += OnleftFootTracked;
        manager.RightFootTracked += OnRightFootTracked;
        manager.BodyLost += OnBodyLost;

    }

    private void OnDisable()
    {
        manager.leftFootTracked -= OnleftFootTracked;
        manager.RightFootTracked -= OnRightFootTracked;
        manager.BodyLost -= OnBodyLost;


    }



    #region EVENTHANDLER FUNCTIONS

    private void OnBodyLost(object sender, Manager.KinectSensorEventArgs e)
    {
        if (playerPrefabRightFoots[e.userID] != null) {
            Destroy(playerPrefabRightFoots[e.userID]);
            manager.OnPlayerPrefabRightFootDestroyed(this, e.userID, null, playerPrefabRightFoots);
        }

        if (playerPrefabLeftFoots[e.userID] != null) {
            Destroy(playerPrefabLeftFoots[e.userID]);
            manager.OnPlayerPrefabLeftFootDestroyed(this, e.userID, playerPrefabLeftFoots, null);
        }

        manager.OnPlayerPrefabLeftFootDestroyed(this, e.userID, playerPrefabLeftFoots, null);

        Debug.Log("playerPrefabIndex " + e.userID + " destroy");
    }

    private void OnRightFootTracked(object sender, Manager.KinectSensorEventArgs e)
    {
        // calculate the Distance between The rightFootjoint and the Floor
        float distanceRightFootTofloor = iFloor.GetFloorTo3DPointDistance(e.rightFootPositionInUnity);
        // map the userrightfootposition (from kinect) in unity 
        Vector3 userUnityMappedRightFootPosition = KinectUnityCoordinateMapperSingleton.MapKinect3DPointToUnity3DPoint(e.rightFootPositionInUnity);

        bool isUserRightFootGrounded;
        if (setting.IsDistanceToFloorCheck) {
            // check if the rightfoot is on the ground
            isUserRightFootGrounded = (distanceRightFootTofloor <= floortoJointMinDistance) ? true : false;
        }
        else
            isUserRightFootGrounded = true;

        if (isUserRightFootGrounded) {
            if (playerPrefabRightFoots[e.userID] == null) {
                Vector3 playerPrefabPosition = new Vector3(userUnityMappedRightFootPosition.x, userUnityMappedRightFootPosition.y, userUnityMappedRightFootPosition.z);
                playerPrefabRightFoots[e.userID] = Instantiate(playerPrefab, playerPrefabPosition, transform.rotation);
                playerPrefabRightFoots[e.userID].name = "FootRightUser" + e.userID;

                manager.OnPlayerPrefabRightFootInstantiated(this, e.userID, null, playerPrefabRightFoots);
            }

            playerPrefabRightFoots[e.userID].transform.position = new Vector3(userUnityMappedRightFootPosition.x, userUnityMappedRightFootPosition.y, userUnityMappedRightFootPosition.z);
            playerPrefabRightFoots[e.userID].transform.rotation = Quaternion.RotateTowards(playerPrefabRightFoots[e.userID].transform.rotation, Quaternion.LookRotation(Vector3.down), 3);

        }
        else if (!isUserRightFootGrounded) {
            if (playerPrefabRightFoots[e.userID] != null) {
                Destroy(playerPrefabRightFoots[e.userID]);
                manager.OnPlayerPrefabRightFootDestroyed(this, e.userID, null, playerPrefabRightFoots);
                //playerPrefabRightFoots[e.userIndex] = null;
            }
        }

    }

    private void OnleftFootTracked(object sender, Manager.KinectSensorEventArgs e)
    {
        // calculate the Distance between The leftFootjoint and the Floor
        float distanceLeftFootTofloor = iFloor.GetFloorTo3DPointDistance(e.leftFootPositionInUnity);
        // map the userleftfootposition (from kinect) in unity 
        Vector3 userUnityMappedLeftFootPosition = KinectUnityCoordinateMapperSingleton.MapKinect3DPointToUnity3DPoint(e.leftFootPositionInUnity);

        bool isUserLeftFootGrounded;

        if (setting.IsDistanceToFloorCheck) {
            // check if the left foot is on the ground
            isUserLeftFootGrounded = (distanceLeftFootTofloor <= floortoJointMinDistance) ? true : false;
        }
        else
            isUserLeftFootGrounded = true;


        if (isUserLeftFootGrounded) {
            if (playerPrefabLeftFoots[e.userID] == null) {
                Vector3 playerPrefabPosition = new Vector3(userUnityMappedLeftFootPosition.x, userUnityMappedLeftFootPosition.y, userUnityMappedLeftFootPosition.z);
                playerPrefabLeftFoots[e.userID] = Instantiate(playerPrefab, playerPrefabPosition, transform.rotation);
                playerPrefabLeftFoots[e.userID].name = "FootLeftUser" + e.userID;

                manager.OnPlayerPrefabLeftFootInstantiated(this, e.userID, playerPrefabLeftFoots, null);
            }

            playerPrefabLeftFoots[e.userID].transform.position = new Vector3(userUnityMappedLeftFootPosition.x, userUnityMappedLeftFootPosition.y, userUnityMappedLeftFootPosition.z);
            playerPrefabLeftFoots[e.userID].transform.rotation = Quaternion.RotateTowards(playerPrefabLeftFoots[e.userID].transform.rotation, Quaternion.LookRotation(Vector3.down), 3);

        }
        else if (!isUserLeftFootGrounded) {
            if (playerPrefabLeftFoots[e.userID] != null) {
                Destroy(playerPrefabLeftFoots[e.userID]);
                manager.OnPlayerPrefabLeftFootDestroyed(this, e.userID, playerPrefabLeftFoots, null);
            }


        }

    }

    #endregion //EVENTHANDLER FUNCTIONS

}
