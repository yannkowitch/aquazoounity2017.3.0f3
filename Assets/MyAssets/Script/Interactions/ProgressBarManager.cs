﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ProgressBarManager : MonoBehaviour
{
    [SerializeField] private GameObject partikelPrefab;
    private bool onCollisionStay = false;
    private float time = 0;
    private bool isLoadingBarFilled;
    [SerializeField] GameObject CircularProgressBar;
    private Coroutine m_resetCircularProgressBarCoroutine;

    Manager manager;
    private void Awake()
    {
        manager = Manager.Instance;
    }

    private void OnEnable()
    {
        manager.TriggerButtonCreated += OnTriggerButtonCreated;
        manager.OnTriggerButtonDestroyed += OnTriggerButtonDestroyed;
        manager.OnPlayerPrefabDestroyed += OnPlayerPrefabDestroyed;
        manager.UserCollisionEnter += OnUserCollisionEnter;
        manager.UserCollisionStay += OnUserCollisionStay;
        manager.UserCollisionExit += OnUserCollisionExit;
        manager.LoadingBarFilled += OnLoadingBarFilled;
    }

    private void OnDisable()
    {
        manager.TriggerButtonCreated -= OnTriggerButtonCreated;
        manager.OnTriggerButtonDestroyed -= OnTriggerButtonDestroyed;
        manager.OnPlayerPrefabDestroyed -= OnPlayerPrefabDestroyed;
        manager.UserCollisionEnter -= OnUserCollisionEnter;
        manager.UserCollisionStay -= OnUserCollisionStay;
        manager.UserCollisionExit -= OnUserCollisionExit;
        manager.LoadingBarFilled -= OnLoadingBarFilled;
    }

    private void OnTriggerButtonDestroyed(object sender, EventArgs e)
    {      
        CircularProgressBar.SetActive(false);
        isLoadingBarFilled = false;
    }

    private void OnTriggerButtonCreated(object sender, EventArgs e)
    {
        isLoadingBarFilled = false;
        CircularProgressBar.SetActive(false);
 
    }

    private void OnLoadingBarFilled(object sender, EventArgs e)
    {
        isLoadingBarFilled = true;
    }

    private void OnUserCollisionExit(object sender, Manager.CollisionEventArgs e)
    {
        if (e.collisionExit.transform.tag == "triggerButton")
        {
            onCollisionStay = false;
            if (m_resetCircularProgressBarCoroutine!= null)
            {
                StopCoroutine(resetCircularProgressBar());
                m_resetCircularProgressBarCoroutine = null;
            }

            m_resetCircularProgressBarCoroutine = StartCoroutine(resetCircularProgressBar());
           // manager.TriggerButtonisExited();
        }

    }

    private void OnUserCollisionStay(object sender, Manager.CollisionEventArgs e)
    {
        if (e.collisionStay.transform.tag == "triggerButton")
        {
            onCollisionStay = true;

            if (isLoadingBarFilled)
            {
                isLoadingBarFilled = false;
                Transform otherObjectTransform = e.collisionStay.transform;
                Vector3 hitPoint = e.collisionStay.contacts[0].point;
                
                Destroy(otherObjectTransform.gameObject);
                /*
                GameObject partikelClone = Instantiate(partikelPrefab, hitPoint, Quaternion.identity) as GameObject;
                partikelClone.GetComponent<Renderer>().material.color = otherObjectTransform.GetComponent<MeshRenderer>().material.color;
                Destroy(partikelClone, 3);
                */
            }
            else
            {
                CircularProgressBar.GetComponent<RadialProgress>().IncrementValue();
            }
           

        }
    }

    private void OnUserCollisionEnter(object sender, Manager.CollisionEventArgs e)
    {
        if (e.collisionEnter.transform.tag == "triggerButton")
        {

            onCollisionStay = false;
            isLoadingBarFilled = false;
            CircularProgressBar.SetActive(true);
         
       //     manager.TriggerButtonisEntered();
        }
    }

    private void OnPlayerPrefabDestroyed(object sender, Manager.CollisionEventArgs e)
    {
        if (!isLoadingBarFilled && CircularProgressBar.activeInHierarchy)
        {
            onCollisionStay = false;

            if (m_resetCircularProgressBarCoroutine != null)
            {
                StopCoroutine(resetCircularProgressBar());
                m_resetCircularProgressBarCoroutine = null;
            }

            m_resetCircularProgressBarCoroutine = StartCoroutine(resetCircularProgressBar());
        }
    }

    private IEnumerator resetCircularProgressBar()
    {
        while (time < 5)
        {
            time += Time.deltaTime;

            if (onCollisionStay || CircularProgressBar.GetComponent<RadialProgress>().CurrentValue <= 0)
            {
                //Debug.Log("break");
                break;
            }
           
            CircularProgressBar.GetComponent<RadialProgress>().DecrementValue();
            
            yield return null;
        }

        time = 0;
    }
    
}
