﻿using System.Collections;
using UnityEngine;


public class Pederpes : MonoBehaviour
{
    private PederpesManager pederpesManager;
    private bool isApplicationQuitting = false;
    private Rigidbody rb;
    private bool isdestroyable;
    private Animator animator;
    private Manager manager;

    private void Awake()
    {
        manager = Manager.Instance;
        animator = this.GetComponent<Animator>();
    }

    // Use this for initialization
    private IEnumerator Start()
    {
        isdestroyable = false;
        pederpesManager = FindObjectOfType<PederpesManager>();
        rb = GetComponent<Rigidbody>();
        manager.PederpesIsCreated(this);
        yield return new WaitForSeconds(3);
        isdestroyable = true;
    }

    private void FixedUpdate()
    {
        if (pederpesManager.walk)
            Moveforward(pederpesManager.speed);
    }
    private void Update()
    {
        animator.SetBool("walk", pederpesManager.walk);
    }
    private void Moveforward(float movespeed)
    {
        rb.MovePosition(transform.position - transform.forward * movespeed * Time.deltaTime);
    }

    private void OnApplicationQuit()
    {
        isApplicationQuitting = true;
    }

    private void OnDestroy()
    {
        if (!isApplicationQuitting)
            manager.PederpesIsDestroyed(this);

    }

    private void OnTriggerEnter(Collider otherCollider)
    {
        if (otherCollider.gameObject.tag != "animalDestroyerCollider") return;

        if (isdestroyable)
            Destroy(this.gameObject);

        Debug.Log("collision enter" + otherCollider.gameObject.name);

    }

    public void OnPederpesWalked(int value)
    {
        manager.OnPederpesWalked();
    }
}

