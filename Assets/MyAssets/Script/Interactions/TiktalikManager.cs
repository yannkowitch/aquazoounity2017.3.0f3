﻿using System.Collections;
using UnityEngine;

public class TiktalikManager : MonoBehaviour
{
    public GameObject TiktalikPrefab;
    private GameObject TiktalikInstance;
    public float speed = 1.5f;
    public bool move, crawl, swim;
    public GameObject TiktalikInitialPosition;
    public GameObject UnityCoordinateSystem;
    private Environment.Level environmentLevel;
    private Coroutine m_PlayAnimationForSecondsAndStopCoroutine;
    private Manager manager;
    private Vector3 pos;
    private Quaternion rot;
    private bool isOtherAnimalDestroyed;
    private Tiktalik.EnvironmentState environmentState;
    private Setting setting;
    float threshhold = 3;

    private void Awake()
    {
        manager = Manager.Instance;
        setting = Setting.Instance;
    }



    private void OnEnable()
    {
        manager.TiktalikCreated += OnTiktalikCreated;
        manager.EnvironmentLevelInitialized += OnEnvironmentLevelInitialized;
        manager.OnTriggerButtonDestroyed += OnTriggerButtonDestroyed;

        if (setting.ApplicationTyp == Setting.Application.Land) {
            manager.OnPederpesDestroyed += OnPederpesDestroyed;
        }
        else if (setting.ApplicationTyp == Setting.Application.TiefseeUfer) {
            manager.OnEusthenopteronDestroyed += OnEusthenopteronDestroyed;
            manager.TiktalikEnvironmentStateChanged += OnTiktalikEnvironmentStateChanged;
        }


    }

    private void OnDisable()
    {
        manager.TiktalikCreated -= OnTiktalikCreated;
        manager.EnvironmentLevelInitialized -= OnEnvironmentLevelInitialized;
        manager.OnTriggerButtonDestroyed -= OnTriggerButtonDestroyed;

        if (setting.ApplicationTyp == Setting.Application.Land) {
            manager.OnPederpesDestroyed -= OnPederpesDestroyed;
        }
        else if (setting.ApplicationTyp == Setting.Application.TiefseeUfer) {
            manager.OnEusthenopteronDestroyed -= OnEusthenopteronDestroyed;
            manager.TiktalikEnvironmentStateChanged -= OnTiktalikEnvironmentStateChanged;
        }

    }

    private void SwimOrCrawl()
    {
        switch (environmentState) {
            case Tiktalik.EnvironmentState.Water:
                swim = true;
                break;
            case Tiktalik.EnvironmentState.Land:
                crawl = true;
                break;

        }
    }


    private void OnEnvironmentLevelInitialized(object sender, Manager.EnvironmentLevelEventArgs e)
    {
        environmentLevel = e.environmentLevel;
    }

    private void OnTiktalikCreated(object sender, Manager.TiktalikEventArgs e)
    {
        isOtherAnimalDestroyed = false;

        if (environmentLevel == Environment.Level.Land)
            PlayAnimationBasedOnDistanceCoroutine(false, true);
        else {
            crawl = true;
            swim = false;
        }

    }

    private void OnTiktalikEnvironmentStateChanged(object sender, Manager.TiktalikEnvironmentStateEventArgs e)
    {
        environmentState = e.environmentState;

        switch (environmentState) {
            case Tiktalik.EnvironmentState.Land:

                if (Tiktalik.counter <= 1) PlayAnimationBasedOnDistanceCoroutine(false, true);

                else {
                    crawl = true;
                    swim = false;
                }

                break;
            case Tiktalik.EnvironmentState.Water:

                if (Tiktalik.counter <= 1) PlayAnimationBasedOnDistanceCoroutine(true, false);
                else {
                    swim = true;
                    crawl = false;
                }

                break;
        }

    }

    private void OnEusthenopteronDestroyed(object sender, Manager.EusthenopteronEventArgs e)
    {
        isOtherAnimalDestroyed = true;

        AssignPoseOtherAnimalToTiktalik(e.Eusthenopteron_Gameobject.transform);

        SetTiktalikPose();

        Spawn("Tiktaalik", pos, rot);
    }

    private void OnPederpesDestroyed(object sender, Manager.PederpesEventArgs e)
    {
        isOtherAnimalDestroyed = true;
        AssignPoseOtherAnimalToTiktalik(e.Pederpes_Gameobject.transform);

        SetTiktalikPose();

        Spawn("Tiktaalik", pos, rot);

    }

    private void OnEnvironmentLevelCompletelyChanged(object sender, Manager.EnvironmentLevelEventArgs wp)
    {
        environmentLevel = wp.environmentLevel;

        if (!isOtherAnimalDestroyed) return;

        if (environmentLevel != Environment.Level.Ufer) return;

        SetTiktalikPose();
        Spawn("Tiktaalik", pos, rot);

    }

    private void OnTriggerButtonDestroyed(object sender, Manager.TriggerButtonEventArgs bt)
    {
        if (setting.ApplicationTyp == Setting.Application.Land) {
            crawl = true;
            swim = false;
        }
        else if (setting.ApplicationTyp == Setting.Application.TiefseeUfer) {
            SwimOrCrawl();
        }

    }

    private void SetTiktalikPose()
    {
        pos = new Vector3(TiktalikInitialPosition.transform.position.x, 0, TiktalikInitialPosition.transform.position.z);

        rot = Quaternion.Lerp(TiktalikInitialPosition.transform.rotation, Quaternion.LookRotation(TiktalikInitialPosition.transform.position - UnityCoordinateSystem.transform.position), 2);
        rot.x = 0;
        rot.z = 0;
    }

    private void Spawn(string name, Vector3 pos, Quaternion rot)
    {
        if (TiktalikInstance == null) {
            TiktalikInstance = (GameObject)Instantiate(TiktalikPrefab, pos, rot);
            TiktalikInstance.name = name;
        }

    }

    private IEnumerator PlayAnimationBasedOnDistance(bool swim, bool crawl)
    {
        while (Vector3.Distance(new Vector3(TiktalikInstance.transform.position.x, 0, TiktalikInstance.transform.position.z),
                                new Vector3(UnityCoordinateSystem.transform.position.x, 0, UnityCoordinateSystem.transform.position.z)) > threshhold) {
            this.swim = swim;
            this.crawl = crawl;
            yield return null;
        }
        // distanc between tiktalik and coordinateorigine <= threshhold
        if (swim)
            this.swim = !swim;
        if (crawl)
            this.crawl = !crawl;

        manager.OnTiktaliksInIdleState(this);
    }

    private void PlayAnimationBasedOnDistanceCoroutine(bool swim, bool crawl)
    {
        if (m_PlayAnimationForSecondsAndStopCoroutine != null) {
            StopCoroutine(PlayAnimationBasedOnDistance(swim, crawl));
            m_PlayAnimationForSecondsAndStopCoroutine = null;
        }
        m_PlayAnimationForSecondsAndStopCoroutine = StartCoroutine(PlayAnimationBasedOnDistance(swim, crawl));

    }

    private void AssignPoseOtherAnimalToTiktalik(Transform otherAnimalTransform)
    {
        TiktalikInitialPosition.transform.position = otherAnimalTransform.position;
        TiktalikInitialPosition.transform.rotation = otherAnimalTransform.rotation;
    }

}
