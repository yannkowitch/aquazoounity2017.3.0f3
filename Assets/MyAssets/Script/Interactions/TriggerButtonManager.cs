﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class TriggerButtonManager : MonoBehaviour
{
   
    [SerializeField] GameObject TriggerButton;
    [SerializeField] GameObject TriggerButtonContainer;

    [Header("Buttonspawn Position Range relativ to TriggerBtnContainerCoordinate ")]

    [Range(-3.0f, 0.0f)] [SerializeField] float negX_Range;
    [Range(0, 3.0f)] [SerializeField] float posX_Range;
    [Range(-3.0f, 0.0f)] [SerializeField] float negZ_Range;
    [Range(0, 3.0f)] [SerializeField] float posZ_Range;


    private GameObject triggerBtnClone;

    Manager manager;

    private void Awake()
    {
        manager = Manager.Instance;
    }

    // Use this for initialization
    private void Start()
    {
    }

    private void OnEnable()
    {
        manager.EnvironmentLevelInitialized += OnEnvironmentLevelInitialized;
        manager.OnEusthenopteroninIdleState += OnEusthenopteroninIdleState;
        manager.OnPederpesinIdleState += OnPederpesinIdleState;
        manager.TiktalikinIdleState += OnTiktalikInIdleState;
    }

    private void OnDisable()
    {
        manager.EnvironmentLevelInitialized -= OnEnvironmentLevelInitialized;
        manager.OnEusthenopteroninIdleState -= OnEusthenopteroninIdleState;
        manager.OnPederpesinIdleState -= OnPederpesinIdleState;
        manager.TiktalikinIdleState -= OnTiktalikInIdleState;
    }

    #region  EVENTHADLERS

    private void OnTiktalikInIdleState(object sender, EventArgs e)
    {
        SpawnTriggerButton();
    }

    private void OnPederpesinIdleState(object sender, EventArgs e)
    {
        SpawnTriggerButton();
    }

    private void OnEusthenopteroninIdleState(object sender, EventArgs e)
    {
        SpawnTriggerButton();
    }

    private void OnEnvironmentLevelInitialized(object sender, Manager.EnvironmentLevelEventArgs wp)
    {
        SpawnTriggerButton();
    }

    #endregion  EVENTHADLERS
    // x= -8.073418   y= -2.3671   z= 17.09308
    private void Spawn(GameObject gameobject, Vector3 position)
    {
        if (triggerBtnClone != null) return;
        // Vector3 randomPosition = new Vector3(UnityEngine.Random.Range(5, 12), position.y, UnityEngine.Random.Range(-14, -20));
        Vector3 randomPosition = new Vector3(UnityEngine.Random.Range(negX_Range, posX_Range), 0, UnityEngine.Random.Range(negZ_Range, posZ_Range));

        triggerBtnClone = Instantiate(gameobject, randomPosition, Quaternion.identity) as GameObject;
        triggerBtnClone.name = gameobject.name;
        triggerBtnClone.transform.SetParent(TriggerButtonContainer.transform, false);

    }

    private IEnumerator SpawnIE(GameObject gameobject, Vector3 position, float waitingTime)
    {
        yield return new WaitForSeconds(waitingTime);
        Spawn(gameobject, position);
    }

    private void SpawnTriggerButton()
    {
        Spawn(TriggerButton, TriggerButton.transform.position);
    }

}
