﻿using System.Collections;
using UnityEngine;


public class PederpesManager : MonoBehaviour
{
    public GameObject pederpesPrefab;
    private GameObject pederpesInstance;
    public float speed = 1.5f;
    public bool walk;
    public GameObject PederpesInitialPosition;
    [SerializeField] private GameObject UnityCoordinateSystem;
    private Coroutine m_PlayAnimationForSecondsAndStopCoroutine;
    private Vector3 position;
    private Quaternion rotation;
    private Manager manager;
    private int SpawnCounter = 0;


    [SerializeField] Vector3 PederpesInitPositionTest;
    private void Awake()
    {
        manager = Manager.Instance;
    }


    private void OnEnable()
    {
        manager.EnvironmentLevelInitialized += OnEnvironmentLevelInitialized;
        manager.OnTriggerButtonDestroyed += OnTriggerButtonDestroyed;
        manager.TiktalikDestroyed += OnTiktalikDestroyed;
        manager.OnPederpesCreated += OnPederpesCreated;
    }

    private void OnDisable()
    {
        manager.EnvironmentLevelInitialized -= OnEnvironmentLevelInitialized;
        manager.OnTriggerButtonDestroyed -= OnTriggerButtonDestroyed;
        manager.TiktalikDestroyed -= OnTiktalikDestroyed;
        manager.OnPederpesCreated -= OnPederpesCreated;
    }

    private void OnEnvironmentLevelInitialized(object sender, Manager.EnvironmentLevelEventArgs wp)
    {
        //Vector3 pos = new Vector3(-1, 0.5f, 0);
        Vector3 pos = PederpesInitPositionTest;

        Spawn("Pederpes", pos, Quaternion.LookRotation(pos - UnityCoordinateSystem.transform.position));
    }

    private void OnPederpesCreated(object sender, Manager.PederpesEventArgs e)
    {
        if (SpawnCounter > 1)
            walk = true;
    }

    private void OnTiktalikDestroyed(object sender, Manager.TiktalikEventArgs e)
    {
        AssignPoseOtherAnimalToPederpes(e.Tiktalik_Gameobject.transform);

        SetPederpesPose();
        Spawn("Pederpes", position, rotation);

        PlayAnimationForSecondsAndStopCoroutine();
    }


    private void OnTriggerButtonDestroyed(object sender, Manager.TriggerButtonEventArgs e)
    {
        walk = true;
    }



    private void SetPederpesPose()
    {
        position = new Vector3(PederpesInitialPosition.transform.position.x, 0, PederpesInitialPosition.transform.position.z); ;
        rotation = Quaternion.Lerp(PederpesInitialPosition.transform.rotation, Quaternion.LookRotation(PederpesInitialPosition.transform.position - UnityCoordinateSystem.transform.position), 2);
        rotation.x = 0;
        rotation.z = 0;
    }

    private void Spawn(string name, Vector3 position, Quaternion rotation)
    {
        if (pederpesInstance == null) {
            pederpesInstance = (GameObject)Instantiate(pederpesPrefab, position, rotation);
            pederpesInstance.name = name;
            SpawnCounter++;
        }

    }

    private IEnumerator PlayAnimationBasedOnDistance()
    {
        while (Vector3.Distance(new Vector3(pederpesInstance.transform.position.x, 0, pederpesInstance.transform.position.z),
                                 new Vector3(UnityCoordinateSystem.transform.position.x, 0, UnityCoordinateSystem.transform.position.z)) > 3) {
            walk = true;
            yield return null;
        }

        walk = false;
        manager.PederpesInIdleState(this);
    }

    private void PlayAnimationForSecondsAndStopCoroutine()
    {
        if (m_PlayAnimationForSecondsAndStopCoroutine != null) {
            StopCoroutine(PlayAnimationBasedOnDistance());
            m_PlayAnimationForSecondsAndStopCoroutine = null;
        }

        m_PlayAnimationForSecondsAndStopCoroutine = StartCoroutine(PlayAnimationBasedOnDistance());
    }

    private void AssignPoseOtherAnimalToPederpes(Transform otherAnimalTransform)
    {
        PederpesInitialPosition.transform.position = otherAnimalTransform.position;
        PederpesInitialPosition.transform.rotation = otherAnimalTransform.rotation;
    }
}
