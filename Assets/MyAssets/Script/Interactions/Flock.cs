﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flock : MonoBehaviour
{
    public float speed;
    private Vector3 averageHeading;
    private Vector3 averagePosition;
    private GlobalFlockManager globalFlock;
    private bool turning = false;
    //Animation anim;
    //Animator animator;
    // [SerializeField] float animSpeed = 1;
    // Use this for initialization
    private void Start()
    {
        globalFlock = FindObjectOfType<GlobalFlockManager>().GetComponent<GlobalFlockManager>();
        speed = Random.Range(globalFlock.minSpeed, globalFlock.maxSpeed);
        /*
        if (GetComponent<Animation>() != null)
        {
            anim = this.GetComponent<Animation>();
            foreach (AnimationState state in anim){ state.speed = globalFlock.animSpeed; }       
        }
        */
        //if (GetComponent<Animator>() != null) { animator = this.GetComponent<Animator>(); }
    }

    // Update is called once per frame
    private void Update()
    {
        turning = Vector3.Distance(transform.position, Vector3.zero) >= globalFlock.Tanksize ? true : false;
        if (turning)
        {
            Vector3 direction = Vector3.zero - transform.position;
            transform.rotation = Quaternion.Slerp(transform.rotation,
                                 Quaternion.LookRotation(direction),
                                 globalFlock.rotationSpeed * Time.deltaTime);

            speed = Random.Range(globalFlock.minSpeed, globalFlock.maxSpeed);
        }
        else
        {
            if (Random.Range(0, 5) < 1)
            {
                ApplyRules();
            }
        }
        // speed = Random.Range(globalFlock.minSpeed, globalFlock.maxSpeed);
        transform.Translate(0, 0, Time.deltaTime * speed);
    }

    private void ApplyRules()
    {
        GameObject[] gos;
        gos = globalFlock.AllFish;

        Vector3 goalPos = globalFlock.GoalPos;
        Vector3 vCenter = Vector3.zero;
        Vector3 vAvoid = Vector3.zero;

        float gSpeed = 0.01f;
        float dist;
        int groupSize = 0;

        foreach (GameObject go in gos)
        {
            if (go != this.gameObject)
            {
                dist = Vector3.Distance(go.transform.position, this.transform.position);

                if (dist <= globalFlock.neighbourDistance)
                {
                    vCenter += go.transform.position;
                    groupSize++;

                    if (dist < globalFlock.FishAvoidDistance)
                    {
                        vAvoid = vAvoid + (this.transform.position - go.transform.position);
                    }
                    Flock anotherFlock = go.GetComponent<Flock>();
                    gSpeed = gSpeed + anotherFlock.speed;
                }
            }
        }

        if (groupSize > 0)
        {
            vCenter = vCenter / groupSize + (goalPos - this.transform.position);
            speed = (gSpeed / groupSize);

            Vector3 direction = (vCenter + vAvoid) - transform.position;
            if (direction != Vector3.zero)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), globalFlock.rotationSpeed * Time.deltaTime);
            }
        }
    }
}
