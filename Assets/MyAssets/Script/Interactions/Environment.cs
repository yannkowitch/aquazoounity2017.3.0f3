﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Environment : MonoBehaviour
{
    private Suimono.Core.SuimonoObject suimonoObject;
    private Level environmentLevel;
    private float transitionTime;
    [SerializeField] private float timer = 0;
    private bool isEnvironmentHalfTransited = false;
    private Coroutine m_WaterPresetChangedCoroutine;  
    private string TiefseePreset, UferPreset;
    private Manager manager;
    private Setting setting;
    public enum Level
    {
        Tiefsee,
        Ufer,
        Land
    }
    public enum WaterPreset
    {
        BlueOceanwithWaves,
        BuoyancyScenePreset,
        CalmCarribeanBlue,
        Cartoon,
        HotSpringMurky,
        CalmClear,
        DeepDarkOcean
    }
    public Dictionary<WaterPreset, string> dictionary = new Dictionary<WaterPreset, string>();


    private void Awake()
    {
        manager = Manager.Instance;
        setting = Setting.Instance;
        environmentLevel = (setting.ApplicationTyp == Setting.Application.Land) ? Level.Land : Level.Tiefsee;
        
        transitionTime = setting.LevelTransitionTime;
       
        if (GameObject.Find("SUIMONO_Surface") != null)
            suimonoObject = GameObject.Find("SUIMONO_Surface").GetComponent<Suimono.Core.SuimonoObject>();

        dictionary.Add(WaterPreset.BlueOceanwithWaves, "Blue Ocean with Waves");
        dictionary.Add(WaterPreset.BuoyancyScenePreset, "Buoyancy Scene Preset");
        dictionary.Add(WaterPreset.CalmCarribeanBlue, "Calm Carribean Blue");
        dictionary.Add(WaterPreset.Cartoon, "Cartoon");
        dictionary.Add(WaterPreset.HotSpringMurky, "Hot Spring Murky Kopie");
        dictionary.Add(WaterPreset.CalmClear, "Calm Clear");
        dictionary.Add(WaterPreset.DeepDarkOcean, "Deep Dark Ocean Kopie");

        TiefseePreset = dictionary[WaterPreset.CalmCarribeanBlue];
        //TiefseePreset = dictionary[WaterPreset.BlueOceanwithWaves];
        UferPreset = dictionary[WaterPreset.BlueOceanwithWaves];
        //UferPreset = dictionary[WaterPreset.HotSpringMurky];

    }


    private void Start()
    {
        if (suimonoObject.presetSaveName != TiefseePreset)
            suimonoObject.SuimonoTransitionPreset(TiefseePreset, 0.2f);

        manager.OnEnvironmentLevelInitialized(environmentLevel);
    }

    private void OnEnable()
    {
        manager.OnTriggerButtonDestroyed += OnTriggerButtonDestroyed;
    }

    private void OnDisable()
    {
        manager.OnTriggerButtonDestroyed -= OnTriggerButtonDestroyed;
    }

    private void OnTriggerButtonDestroyed(object sender, Manager.TriggerButtonEventArgs e)
    {
        if (environmentLevel == Level.Land) return;
        ChangeEnvironmentLevel();
    }

    private void ChangeEnvironmentLevel()
    {
        if (environmentLevel != Level.Tiefsee && environmentLevel != Level.Ufer) return;

        string newPresetName = (environmentLevel == Level.Tiefsee) ? UferPreset : TiefseePreset;

        suimonoObject.SuimonoTransitionPreset(newPresetName, transitionTime);

        environmentLevel = (environmentLevel == Level.Tiefsee) ? Level.Ufer : Level.Tiefsee;

        if (m_WaterPresetChangedCoroutine != null)
        {
            StopCoroutine(WaterPresetChangedIE(environmentLevel));
            m_WaterPresetChangedCoroutine = null;
        }
        m_WaterPresetChangedCoroutine = StartCoroutine(WaterPresetChangedIE(environmentLevel));
    }

    public float Timer
    {
        get { return timer; }

        set
        {
            timer = value;

            if (timer >= transitionTime / 2 && !isEnvironmentHalfTransited)
            {
                manager.OnEnvironmentLevelHalfChanged(environmentLevel);
                isEnvironmentHalfTransited = true;
            }
        }
    }

    private IEnumerator WaterPresetChangedIE(Level environmentLevel)
    {
        while (timer <= transitionTime)
        {
            Timer += Time.deltaTime;
            yield return null; // make the loop wait until the next frame before continuing
        }

        Timer = 0;
        isEnvironmentHalfTransited = false;
        manager.OnEnvironmentLevelCompletelyChanged(environmentLevel);

    }
}
