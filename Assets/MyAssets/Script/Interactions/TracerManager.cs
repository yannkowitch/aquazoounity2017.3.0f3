﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TracerManager : MonoBehaviour
{

    [SerializeField] private Image UserFootTracerPrefab;
    [SerializeField] private Image AnimalTracerPrefab;
    [SerializeField] private Image ButtonTracerPrefab;
    [SerializeField] private Sprite ButtonTracerImage;
    private Image[] RemoteTargetTracerListe;
    private GameObject[] LocalTargetTracerListe;
    [SerializeField] private GameObject CircularProgressBar;

    [SerializeField] private float speed = 6;
    [SerializeField] GameObject TriggersPanel;
    int userFootTrackerStartIndex;
    string footName;
    string animalName;
    private int rightFootId, leftFootId;
    private Setting setting;
    private Manager manager;


    private void Awake()
    {
        manager = Manager.Instance;
        setting = Setting.Instance;

        if (TriggersPanel == null) throw new Exception("UserLeftfootTracker not found");

        if (UserFootTracerPrefab == null) throw new Exception("UserBoundingBoxPrfab not found");

        if (AnimalTracerPrefab == null) throw new Exception("AnimalTrackerImagePrfab not found");

        if (UserFootTracerPrefab == null) throw new Exception("UserBoundingBoxPrfab not found");

        RemoteTargetTracerListe = new Image[17];
        LocalTargetTracerListe = new GameObject[RemoteTargetTracerListe.Length];
        userFootTrackerStartIndex = (RemoteTargetTracerListe.Length - 12);

        Debug.Log("startIndex :" + userFootTrackerStartIndex);


    }

    private void Start()
    {
        if (setting.ApplicationTyp == Setting.Application.Land) {
            ButtonTracerPrefab.sprite = ButtonTracerImage;
        }

        for (int i = 0; i < RemoteTargetTracerListe.Length; i++) {
            switch (i) {
                case 0:
                    RemoteTargetTracerListe[i] = Instantiate(ButtonTracerPrefab, ButtonTracerPrefab.transform.position, ButtonTracerPrefab.transform.rotation);
                    RemoteTargetTracerListe[i].name = "ButtonTracker";
                    break;

                case 1:
                case 2:
                case 3:

                    if (i == 1) {
                        animalName = "EUSTHENOPTERON";
                    }
                    else if (i == 2) {
                        animalName = "PEDERPES";
                    }
                    else if (i == 3) {
                        animalName = "TIKTAALIK";
                    }

                    RemoteTargetTracerListe[i] = Instantiate(AnimalTracerPrefab, AnimalTracerPrefab.transform.position, AnimalTracerPrefab.transform.rotation);
                    RemoteTargetTracerListe[i].transform.GetChild(0).GetComponent<Text>().text = animalName;
                    RemoteTargetTracerListe[i].name = animalName;
                    break;

                case 4:
                    RemoteTargetTracerListe[i] = Instantiate(UserFootTracerPrefab, UserFootTracerPrefab.transform.position, UserFootTracerPrefab.transform.rotation);
                    RemoteTargetTracerListe[i].name = "MouseTracker";
                    break;
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:

                    leftFootId++;
                    RemoteTargetTracerListe[i] = Instantiate(UserFootTracerPrefab, UserFootTracerPrefab.transform.position, UserFootTracerPrefab.transform.rotation);
                    RemoteTargetTracerListe[i].name = "User" + (leftFootId) + "LeftFootTracker";

                    break;

                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                    rightFootId++;
                    RemoteTargetTracerListe[i] = Instantiate(UserFootTracerPrefab, UserFootTracerPrefab.transform.position, UserFootTracerPrefab.transform.rotation);
                    RemoteTargetTracerListe[i].name = "User" + (rightFootId) + "RightFootTracker";
                    break;
            }

            RemoteTargetTracerListe[i].transform.SetParent(TriggersPanel.gameObject.transform,false);

            RemoteTargetTracerListe[i].gameObject.SetActive(false);
        }


    }

    private void OnEnable()
    {
        manager.EnvironmentLevelCompletelyChanged += OnWaterPresetChanged;
        manager.TriggerButtonCreated += OnTriggerButtonCreated;
        manager.TriggerButtonEnable += OnTriggerButtonEnable;
        manager.OnTriggerButtonDestroyed += OnTriggerButtonDestroyed;
        manager.OnEusthenopteronCreated += OnEusthenopteronCreated;
        manager.OnEusthenopteronDestroyed += OnEusthenopteronDestroyed;
        manager.OnPederpesCreated += OnPederpesCreated;
        manager.OnPederpesDestroyed += OnPederpesDestroyed;
        manager.TiktalikCreated += OnTiktalikCreated;
        manager.TiktalikDestroyed += OnTiktalikDestroyed;
        manager.PlayerPrefabMouseCreated += OnPlayerPrefabMouseCreated;
        manager.PlayerPrefabMouseDestroyed += OnPlayerPrefabMouseDestroyed;


        manager.PlayerPrefabLeftFootInstantiated += OnPlayerPrefabLeftFootInstantiated;
        manager.PlayerPrefabLeftDestroyed += OnPlayerPrefabLeftDestroyed;

        manager.PlayerPrefabRightFootInstantiated += OnPlayerPrefabRightFootInstantiated;
        manager.PlayerPrefabRightDestroyed += OnPlayerPrefabRightDestroyed;

        manager.EusthenopteronUnderTheStairs += OnEusthenopteronIsUnderTheStairs;
        manager.EusthenopteronAwayFromTheStairs += OnEusthenopteronIsAwayFromTheStairs;

        manager.TiktalikUnderTheStairs += OnTiktalikIsUnderTheStairs;
        manager.TiktalikAwayFromTheStairs += OnTiktalikIsAwayFromTheStairs;

    }


    private void OnDisable()
    {
        manager.EnvironmentLevelCompletelyChanged -= OnWaterPresetChanged;
        manager.TriggerButtonCreated -= OnTriggerButtonCreated;
        manager.TriggerButtonEnable -= OnTriggerButtonEnable;
        manager.OnTriggerButtonDestroyed -= OnTriggerButtonDestroyed;
        manager.OnEusthenopteronCreated -= OnEusthenopteronCreated;
        manager.OnEusthenopteronDestroyed -= OnEusthenopteronDestroyed;
        manager.OnPederpesCreated -= OnPederpesCreated;
        manager.OnPederpesDestroyed -= OnPederpesDestroyed;
        manager.TiktalikCreated -= OnTiktalikCreated;
        manager.TiktalikDestroyed -= OnTiktalikDestroyed;
        manager.PlayerPrefabMouseCreated -= OnPlayerPrefabMouseCreated;
        manager.PlayerPrefabMouseDestroyed -= OnPlayerPrefabMouseDestroyed;

        manager.PlayerPrefabLeftFootInstantiated -= OnPlayerPrefabLeftFootInstantiated;
        manager.PlayerPrefabLeftDestroyed -= OnPlayerPrefabLeftDestroyed;

        manager.PlayerPrefabRightFootInstantiated -= OnPlayerPrefabRightFootInstantiated;
        manager.PlayerPrefabRightDestroyed -= OnPlayerPrefabRightDestroyed;

        manager.EusthenopteronUnderTheStairs -= OnEusthenopteronIsUnderTheStairs;
        manager.EusthenopteronAwayFromTheStairs -= OnEusthenopteronIsAwayFromTheStairs;


        manager.TiktalikUnderTheStairs -= OnTiktalikIsUnderTheStairs;
        manager.TiktalikAwayFromTheStairs -= OnTiktalikIsAwayFromTheStairs;

    }

    #region  EVENTHADLERS


    private void OnTriggerButtonEnable(object sender, Manager.TriggerButtonEventArgs bt)
    {

        ActivateTracerTarget(bt.TriggerButton, 0, true);
    }

    private void OnTriggerButtonCreated(object sender, Manager.TriggerButtonEventArgs bt)
    {

        ActivateTracerTarget(bt.TriggerButton, 0, true);
    }

    private void OnTriggerButtonDestroyed(object sender, Manager.TriggerButtonEventArgs bt)
    {
        ActivateTracerTarget(bt.TriggerButton, 0, false);
    }


    private void OnEusthenopteronCreated(object sender, Manager.EusthenopteronEventArgs e)
    {
        if (!setting.AnimalTrackerActiv) return;
        ActivateTracerTarget(e.Eusthenopteron_Gameobject, 1, true);
    }

    private void OnEusthenopteronIsUnderTheStairs(object sender, Manager.EusthenopteronEventArgs e)
    {
        if (e.Eusthenopteron_Gameobject == null) return;
        ActivateTargetTracerForStairs(e.Eusthenopteron_Gameobject, 1, false);

    }

    private void OnEusthenopteronIsAwayFromTheStairs(object sender, Manager.EusthenopteronEventArgs e)
    {
        if (e.Eusthenopteron_Gameobject == null) return;
        ActivateTargetTracerForStairs(e.Eusthenopteron_Gameobject, 1, true);
    }

    private void OnEusthenopteronDestroyed(object sender, Manager.EusthenopteronEventArgs e)
    {
        if (!setting.AnimalTrackerActiv) return;
        ActivateTracerTarget(e.Eusthenopteron_Gameobject, 1, false);
    }

    private void OnPederpesDestroyed(object sender, Manager.PederpesEventArgs e)
    {
        if (!setting.AnimalTrackerActiv) return;
        ActivateTracerTarget(e.Pederpes_Gameobject, 2, false);
    }

    private void OnPederpesCreated(object sender, Manager.PederpesEventArgs pe)
    {
        if (!setting.AnimalTrackerActiv) return;
        ActivateTracerTarget(pe.Pederpes_Gameobject, 2, true);
    }



    private void OnTiktalikCreated(object sender, Manager.TiktalikEventArgs e)
    {
        if (!setting.AnimalTrackerActiv) return;
        ActivateTracerTarget(e.Tiktalik_Gameobject, 3, true);
    }
    private void OnTiktalikIsAwayFromTheStairs(object sender, Manager.TiktalikEventArgs e)
    {
        if (e.Tiktalik_Gameobject == null) return;
        ActivateTargetTracerForStairs(e.Tiktalik_Gameobject, 3, true);
    }

    private void OnTiktalikIsUnderTheStairs(object sender, Manager.TiktalikEventArgs e)
    {
        if (e.Tiktalik_Gameobject == null) return;
        ActivateTargetTracerForStairs(e.Tiktalik_Gameobject, 3, false);
    }

    private void OnTiktalikDestroyed(object sender, Manager.TiktalikEventArgs e)
    {
        if (setting.AnimalTrackerActiv == false) return;
        ActivateTracerTarget(e.Tiktalik_Gameobject, 3, false);
    }

    private void OnPlayerPrefabMouseDestroyed(object sender, Manager.PlayerPrefabMouseEventArgs e)
    {
        if (setting.MouseTrackerActiv == false) return;
        ActivateTracerTarget(e.PlayerPrefabClone, 4, false);
    }

    private void OnPlayerPrefabMouseCreated(object sender, Manager.PlayerPrefabMouseEventArgs e)
    {
        if (setting.MouseTrackerActiv == false) return;
        ActivateTracerTarget(e.PlayerPrefabClone, 4, true);
    }

    private void OnMeganisopterDestroyed(object sender, Manager.MeganisopterEventArgs e)
    {

    }

    private void OnMeganisopterCreated(object sender, Manager.MeganisopterEventArgs e)
    {

    }

    private void OnPlayerPrefabRightFootInstantiated(object sender, Manager.PlayerPrefabFootEventArgs e)
    {
        if (setting.UserTrackerActiv == false) return;

        ActivateTracerTarget(e.playerPrefabRightFoots[e.userIndex], e.userIndex + userFootTrackerStartIndex + 6, true);
    }

    private void OnPlayerPrefabRightDestroyed(object sender, Manager.PlayerPrefabFootEventArgs e)
    {
        if (setting.UserTrackerActiv == false) return;
        ActivateTracerTarget(e.playerPrefabRightFoots[e.userIndex], e.userIndex + userFootTrackerStartIndex + 6, false);

    }

    private void OnPlayerPrefabLeftDestroyed(object sender, Manager.PlayerPrefabFootEventArgs e)
    {
        if (setting.UserTrackerActiv == false) return;
        ActivateTracerTarget(e.playerPrefabLeftFoots[e.userIndex], e.userIndex + userFootTrackerStartIndex, false);
    }

    private void OnPlayerPrefabLeftFootInstantiated(object sender, Manager.PlayerPrefabFootEventArgs e)
    {
        if (setting.UserTrackerActiv == false) return;
        ActivateTracerTarget(e.playerPrefabLeftFoots[e.userIndex], e.userIndex + userFootTrackerStartIndex, true);
    }

    private void OnWaterPresetChanged(object sender, Manager.EnvironmentLevelEventArgs wp)
    {
    }


    #endregion  EVENTHADLERS

    private void ActivateTracerTarget(GameObject LocalTargetTracer, int TargetTracerIndex, bool isActive)
    {
        for (int index = 0; index < LocalTargetTracerListe.Length; index++) {
            if (index == TargetTracerIndex) {
                if (LocalTargetTracerListe[index] == null) {
                    LocalTargetTracerListe[TargetTracerIndex] = LocalTargetTracer;
                    RemoteTargetTracerListe[TargetTracerIndex].gameObject.SetActive(true);
                    break;
                }
                else {
                    RemoteTargetTracerListe[TargetTracerIndex].gameObject.SetActive(isActive);
                    break;
                }

            }
        }

    }


    private void ActivateTargetTracerForStairs(GameObject LocalTargetTracer, int TargetTracerIndex, bool isActive)
    {
        for (int index = 0; index < LocalTargetTracerListe.Length; index++) {
            if (index == TargetTracerIndex) {
                if (LocalTargetTracerListe[index] == LocalTargetTracer) {
                    RemoteTargetTracerListe[TargetTracerIndex].gameObject.SetActive(isActive);
                    break;
                }

            }
        }
    }


    private void MoveTracerToCorrespondingTarget()
    {
        for (int i = 0; i < LocalTargetTracerListe.Length; i++) {
            if (LocalTargetTracerListe[i] != null) {
                if (LocalTargetTracerListe[i].activeInHierarchy) {
                    RemoteTargetTracerListe[i].transform.position = Vector3.Lerp(RemoteTargetTracerListe[i].transform.position, Camera.main.WorldToScreenPoint(LocalTargetTracerListe[i].transform.position), speed * Time.deltaTime);

                    if (LocalTargetTracerListe[0] != null) {
                        CircularProgressBar.transform.position = Camera.main.WorldToScreenPoint(LocalTargetTracerListe[0].transform.position);
                    }
                }
            }

        }
    }

    private void LateUpdate()
    {
        MoveTracerToCorrespondingTarget();
    }

}
