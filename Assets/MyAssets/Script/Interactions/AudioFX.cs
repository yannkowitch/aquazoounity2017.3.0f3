﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//[RequireComponent(typeof(AudioSource))]
public class AudioFX : MonoBehaviour
{
    [SerializeField] AudioClip[] Songs;
    private AudioSource audioSource;
    private AudioSource[] sources;
    private Manager manager;
    private Setting setting;

    private void Awake()
    {
        manager = Manager.Instance;
        setting = Setting.Instance;
    }

    private void Start()
    {
        sources = new AudioSource[Songs.Length];
        GameObject child = new GameObject();
        child.transform.parent = this.gameObject.transform;
        child.name = "AudioSources";

        for (int i = 0; i < Songs.Length; i++)
        {
            //create audiosource
            sources[i] = child.AddComponent<AudioSource>();
            sources[i].clip = Songs[i];
            sources[0].loop = true;
            sources[i].volume = 1.0f;

            if (setting.ApplicationTyp == Setting.Application.TiefseeUfer)              
            sources[0].Play();

        }
    }

    private void OnEnable()
    {
        manager.OnTriggerButtonDestroyed += OnTriggerButtonDestroyed;
        manager.LoadBarIncrementing += OnLoadBarIncrementing;
        manager.loadBarDecrementing += OnloadBarDecrementing;
        /*
        manager.OnTiktalikCreated += OnTiktalikCreated;
        manager.OnPederpesCreated += OnPederpesCreated;
        manager.OnEusthenopteronCreated += OnEusthenopteronCreated;
        manager.OnPederpesDestroyed += OnPederpesDestroyed;
        manager.OnTiktalikDestroyed += OnTiktalikDestroyed;
        manager.OnEusthenopteronDestroyed += OnEusthenopteronDestroyed;
        */
    }

    private void OnDisable()
    {
        manager.OnTriggerButtonDestroyed -= OnTriggerButtonDestroyed;
        manager.LoadBarIncrementing -= OnLoadBarIncrementing;
        manager.loadBarDecrementing -= OnloadBarDecrementing;
        /*
        manager.OnTiktalikCreated -= OnTiktalikCreated;
        manager.OnPederpesCreated -= OnPederpesCreated;
        manager.OnEusthenopteronCreated -= OnEusthenopteronCreated;
        manager.OnPederpesDestroyed -= OnPederpesDestroyed;
        manager.OnTiktalikDestroyed -= OnTiktalikDestroyed;
        manager.OnEusthenopteronDestroyed -= OnEusthenopteronDestroyed;
        */
    }

    private void OnloadBarDecrementing(object sender, EventArgs e)
    {
      //  Debug.Log("LoadBar is decrementing");
    }

    private void OnLoadBarIncrementing(object sender, EventArgs e)
    {
       // Debug.Log("LoadBar is Incrementing");
    }

    #region  EVENTHADLERS
    private void OnEusthenopteronDestroyed(object sender, Manager.EusthenopteronEventArgs e)
    {
        StopSong(3);
    }

    private void OnTiktalikDestroyed(object sender, Manager.TiktalikEventArgs e)
    {
        StopSong(2);
    }

    private void OnPederpesDestroyed(object sender, Manager.PederpesEventArgs e)
    {
        StopSong(4);
    }

    private void OnPederpesCreated(object sender, Manager.PederpesEventArgs e)
    {
        PlaySong(4, true);
    }

    private void OnEusthenopteronCreated(object sender, Manager.EusthenopteronEventArgs e)
    {
        PlaySong(3, true);
    }

    private void OnTiktalikCreated(object sender, Manager.TiktalikEventArgs e)
    {
        PlaySong(2, true);
    }
   
    private void OnTriggerButtonDestroyed(object sender, Manager.TriggerButtonEventArgs e)
    {
        PlaySong(1, false);
    }

    #endregion  EVENTHADLERS

    private void PlaySong(int index, bool loop)
    {
        sources[index].clip = Songs[index];
        sources[index].loop = loop;
        sources[index].Play();
    }

    private void StopSong(int index)
    {
        sources[index].Stop();
    }
    
}
