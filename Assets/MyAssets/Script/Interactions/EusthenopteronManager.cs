﻿using System;
using System.Collections;
using UnityEngine;

public class EusthenopteronManager : MonoBehaviour
{
   // private bool isOtherAnimalDestroyed; 
    public GameObject eusthenopteronTarget;
    [SerializeField] private float eusthenopteronTargetYPosition = -2;
    [SerializeField] private GameObject eusthenopteronPrefab;
    private GameObject eusthenopteronInstance;
    public float translationSpeed = 5;
    public bool isTurning;
    public bool move;
    public float rotationSpeed = 0.5f;
    public float minDepht;
    public float maxDepht;
    private Environment.Level environmentPresetState;
    [SerializeField] private GameObject EusthenopteronStartPosition;
    [SerializeField] private GameObject UnityCoordinateSytem;
    private Coroutine m_SetTurningValueCoroutine;
    private Vector3 position;
    private Quaternion rotation;
    private Manager manager;

    private void Awake()
    {
        manager = Manager.Instance;

        if (EusthenopteronStartPosition == null) throw new Exception("you need to assign EusthenopteronStartPosition");
    }

    private void Start()
    {
        eusthenopteronTarget.transform.position = new Vector3(UnityCoordinateSytem.transform.position.x, 0, UnityCoordinateSytem.transform.position.z);
        move = true;
  //      isOtherAnimalDestroyed = false;
    }

    public void SetTargetYPosition()
    {
        eusthenopteronTarget.transform.position = new Vector3(eusthenopteronTarget.transform.position.x, eusthenopteronTargetYPosition, eusthenopteronTarget.transform.position.z);
    }
    private void OnEnable()
    {
        manager.OnTriggerButtonDestroyed += OnTriggerButtonDestroyed;
       manager.EnvironmentLevelInitialized += OnEnvironmentLevelInitialized;
      
        manager.TiktalikDestroyed += OnTiktalikDestroyed;
        manager.OnEusthenopteronCreated += OnEusthenopteronCreated;
    }


    private void OnDisable()
    {
        manager.OnTriggerButtonDestroyed -= OnTriggerButtonDestroyed;
        manager.EnvironmentLevelInitialized -= OnEnvironmentLevelInitialized;
      
        manager.TiktalikDestroyed -= OnTiktalikDestroyed;
        manager.OnEusthenopteronCreated -= OnEusthenopteronCreated;
    }

    private void OnEnvironmentLevelInitialized(object sender, Manager.EnvironmentLevelEventArgs wp)
    {
            Spawn("Eusthenopteron");
            environmentPresetState = wp.environmentLevel;       
    }

    private void OnEusthenopteronCreated(object sender, Manager.EusthenopteronEventArgs e)
    {
//        isOtherAnimalDestroyed = false;
    }

    private void OnTiktalikDestroyed(object sender, Manager.TiktalikEventArgs e)
    {
//        isOtherAnimalDestroyed = true;
        AssignPoseOtherAnimalToEusthenopteron(e.Tiktalik_Gameobject.transform);
        SetEusthenopteronPose();
        Spawn("Eusthenopteron", position, rotation);
        PlayAnimationForSecondsAndStopCoroutine();
    }
  
    private void PlayAnimationForSecondsAndStopCoroutine()
    {
        if (m_PlayAnimationForSecondsAndStopCoroutine != null)
        {
            StopCoroutine(PlayAnimationForSecondsAndStop());
            m_PlayAnimationForSecondsAndStopCoroutine = null;
        }

        m_PlayAnimationForSecondsAndStopCoroutine = StartCoroutine(PlayAnimationForSecondsAndStop());

    }

    private void OnTriggerButtonDestroyed(object sender, Manager.TriggerButtonEventArgs bt)
    {
        if (bt.TriggerButton_Index != 0) return;       
            if (environmentPresetState == Environment.Level.Tiefsee)
                moveForward();        
    }

    private void PlayAnimation(string animationName)
    {
        move = true;
        eusthenopteronInstance.GetComponent<Animation>().Play(animationName);
    }

    private void StopAnimation(string animationName)
    {
        move = false;
        eusthenopteronInstance.GetComponent<Animation>().Stop(animationName);
    }

    private Coroutine m_PlayAnimationForSecondsAndStopCoroutine;

    private IEnumerator PlayAnimationForSecondsAndStop()
    {
        
        while (Vector3.Distance(new Vector3(eusthenopteronInstance.transform.position.x, 0, eusthenopteronInstance.transform.position.z),
        new Vector3(UnityCoordinateSytem.transform.position.x, 0, UnityCoordinateSytem.transform.position.z)) > 3)
        {
            move = true;
            yield return null;
        }

        isTurning = true;
        manager.EusthenopteronInIdleState(this);
    }

    private void Spawn(string name, Vector3 position, Quaternion rotation)
    {
        if (eusthenopteronInstance == null)
        {
            eusthenopteronInstance = Instantiate(eusthenopteronPrefab, position, rotation);
            eusthenopteronInstance.name = name;
        }           
    }

    private void Spawn(string name)
    {
        if (eusthenopteronInstance == null)
        {
            eusthenopteronInstance = Instantiate(eusthenopteronPrefab, eusthenopteronPrefab.transform.position, eusthenopteronPrefab.transform.rotation);
            eusthenopteronInstance.name = name;
        }

    }

    private void moveForward()
    {
        if (m_SetTurningValueCoroutine != null)
        {
            StopCoroutine(SetTurningValue());
            m_SetTurningValueCoroutine = null;
        }

        m_SetTurningValueCoroutine = StartCoroutine(SetTurningValue());
    }

    private IEnumerator SetTurningValue()
    {
        isTurning = true;
        yield return new WaitForSeconds(0.1f);
        isTurning = false;
    }

    private void AssignPoseOtherAnimalToEusthenopteron(Transform otherAnimalTransform)
    {
        EusthenopteronStartPosition.transform.position = otherAnimalTransform.position;
        EusthenopteronStartPosition.transform.rotation = otherAnimalTransform.rotation;
    }

    private void SetEusthenopteronPosition()
    {
        position = new Vector3(EusthenopteronStartPosition.transform.position.x, eusthenopteronPrefab.transform.position.y, EusthenopteronStartPosition.transform.position.z);
        position.y = -2;

    }

    private void SetEusthenopteronRotation()
    {
        rotation = EusthenopteronStartPosition.transform.rotation;
        rotation.x = 0;
        rotation.z = 0;
    }

    private void SetEusthenopteronPose()
    {
        SetEusthenopteronPosition();
        SetEusthenopteronRotation();
    }

}
