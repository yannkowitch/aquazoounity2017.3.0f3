﻿using System;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
#endif

public class ButtonController : MonoBehaviour
{
    private Manager manager;
    [SerializeField] GameObject saveButton;
    [SerializeField] GameObject startButton;
    private Color disabledColor, color;
    private Button saveButtonComponent;
    private bool isSave = false;
    private bool isBtnTrigger = false;
    private Setting setting;
    public void SaveData()
    {
        if (isSave) return;
        isSave = true;
        startButton.SetActive(true);
        saveButton.SetActive(false);
        Debug.Log("Recording complete" + this.name);
    }

    private void Awake()
    {
        setting = Setting.Instance;
        manager = Manager.Instance;

        saveButtonComponent = saveButton.GetComponent<Button>();


        disabledColor = saveButtonComponent.colors.disabledColor;
        color = saveButtonComponent.image.color;

        saveButtonComponent.image.color = disabledColor;
        saveButtonComponent.interactable = false;

        startButton.SetActive(false);
    }

    private void OnEnable()
    {
        manager.ProjectorCameraCalibrationTrigged += OnProjectorCameraCalibrationTrigged;
    }

    private void OnDisable()
    {
        manager.ProjectorCameraCalibrationTrigged -= OnProjectorCameraCalibrationTrigged;
    }

    private void OnProjectorCameraCalibrationTrigged(object sender, EventArgs e)
    {
        saveButtonComponent.interactable = true;
        saveButtonComponent.image.color = color;
    }

    public void LoadNextSceneAsync()
    {
        if (isBtnTrigger == true) return;
        startButton.SetActive(false);
        saveButton.SetActive(false);

        manager.OnLoadSceneTrigged(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);

        isBtnTrigger = true;
        /*
        if (setting.CurrentCalibrationOption == Setting.CalibrationOption.wholeSystem)
            StartCoroutine(FindObjectOfType<SceneFader>().FadeAndLoadScene(Fader.FadeDirection.In, 2));

        else if (setting.CurrentCalibrationOption == Setting.CalibrationOption.pro_unity)

            StartCoroutine(FindObjectOfType<SceneFader>().FadeAndLoadScene(Fader.FadeDirection.In, 3));
        else
            StartCoroutine(FindObjectOfType<SceneFader>().FadeAndLoadScene(Fader.FadeDirection.In, 2));
        */
    }
}
