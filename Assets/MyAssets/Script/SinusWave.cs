﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinusWave : MonoBehaviour
{
    [SerializeField] float maxRotAmp;
    [SerializeField] float maxScaleAmp;
    [SerializeField] private float minRotAmp;
    [SerializeField] private float minScaleAmp;
    private float Xrot, Yrot, Zrot;
    private float Xscale, Yscale, Zscale;
    private float rot_frequenz;
    private float scale_frequenz;

    private IEnumerator Start()
    {
        rot_frequenz = Random.Range(1, 3);
        scale_frequenz = 1f;

        InitRotation();
        InitScale();

        yield return new WaitForSeconds(Random.Range(0.1f, 2));

        while (true)
        {
            UpdateRotation();
            UpdateScale();
            yield return null;
        }
    }

    private void InitRotation()
    {
        Yrot = transform.rotation.y;
        Zrot = transform.rotation.z;
        minRotAmp = 2.92f;
    }

    private void UpdateRotation()
    {
        Xrot = maxRotAmp + minRotAmp * Mathf.Sin(Time.time * rot_frequenz);
        transform.rotation = Quaternion.Euler(Xrot - 90, Yrot, Zrot);
    }


    private void InitScale()
    {
        Zscale = transform.localScale.z;
        Yscale = transform.localScale.y;
        Xscale = transform.localScale.x;
        maxScaleAmp = Xscale;
        minScaleAmp = 0.05f;

        transform.localScale = new Vector3(Xscale, Yscale, Zscale);
    }

    private void UpdateScale()
    {
        Xscale = maxScaleAmp + minScaleAmp * Mathf.Sin(Time.time * scale_frequenz);
        Yscale = maxScaleAmp + minScaleAmp * Mathf.Sin(Time.time * scale_frequenz);

        transform.localScale = new Vector3(Xscale, Yscale, Zscale);
    }

}
