﻿
using UnityEngine;

public class Mouse : MonoBehaviour
{
    #region MEMBER_VARIABLES
    [SerializeField] private GameObject playerPrefab;
    private Transform surface;
    // [SerializeField] private GameObject[] surfaces;
    public GameObject PlayerPrefabClone { get; private set; }
    private Manager manager;
    private Setting setting;
    #endregion // MEMBER_VARIABLES

    // Use this for initialization
    private void Awake()
    {
        setting = Setting.Instance;
        manager = Manager.Instance;

        if (setting.ApplicationTyp == Setting.Application.Land)
        {
            /*
            if (GameObject.Find("Land_Surface") != null)
                surface = GameObject.Find("Land_Surface").transform;
            */
            surface = Game.Instance.surfaces[0].transform;
        }

        else if (setting.ApplicationTyp == Setting.Application.TiefseeUfer)
        {
            /*
             if (GameObject.Find("Suimono_Object") != null)
                 surface = GameObject.Find("Suimono_Object").transform;
            */
            surface = Game.Instance.surfaces[1].transform;
        }


    }


    private void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);


        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.transform == surface)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    if (hit.collider != null)
                    {
                        PlayerPrefabClone = Instantiate(playerPrefab, new Vector3(hit.point.x, surface.position.y, hit.point.z), transform.rotation);

                    }
                }

                if (PlayerPrefabClone != null)
                {
                    PlayerPrefabClone.transform.position = new Vector3(hit.point.x, PlayerPrefabClone.transform.position.y, hit.point.z);
                    PlayerPrefabClone.transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(Vector3.down), 3);

                    manager.OnPlayerPrefabMouseCreated(this);
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (PlayerPrefabClone != null)
            {
                manager.OnPlayerPrefabMouseDestroyed(this);
                Destroy(PlayerPrefabClone);

            }
        }

    }


}
