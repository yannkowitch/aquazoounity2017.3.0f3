﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SettingController : MonoBehaviour
{
    private Coroutine m_InitApplicationCorutine;
    [SerializeField] private Dropdown Dropdown_Application;

    [Range(0, 30)] [SerializeField] private int Fishamount;
    [SerializeField] Text FishAmountText;
    [SerializeField] private Slider Slider_FishAmount;


    [SerializeField] private Toggle Toggle_UserFootTracker;
    [SerializeField] private Text Text_ApplicationMode;

    [SerializeField] private Dropdown Dropdown_Calibration;
    // [SerializeField] private Dropdown Dropdown_ApplicationMode;
    // [SerializeField] private Dropdown Dropdown_SelectedFloor;

    [SerializeField] private Toggle Toggle_AutoHerunterfahren;

    [SerializeField] private Button Button_Apply;
    [SerializeField] private Text Text_Status;
    [SerializeField] private GameObject DeveloperPanel;
    private bool isSubmitButtonPressed;
    private Setting setting;


    private void Start()
    {

    }

    void Awake()
    {
        setting = Setting.Instance;
        Init_Text_ApplicationMode();

        FishAmountText.text = ((int)Slider_FishAmount.value).ToString();

        if (DeveloperPanel == null) throw new Exception("no  DeveloperPanel assigned");
        DesastivateDevelopperPanel();
    }
    public void DesastivateDevelopperPanel()
    {
        DeveloperPanel.SetActive(false);
        Manager.Instance.OnSettingPanelClosed();
    }

    private IEnumerator InitApplicationIE()
    {
        Init_Calibration();
        Init_Application();

        Init_Toggle_UserFootTracker();
        Init_Toggle_AutoHerunterfahren();

        Init_Text_ApplicationMode();

        if (Button_Apply != null) Button_Apply.gameObject.SetActive(false);
        if (Text_Status) Text_Status.text = "processing.....";
        yield return new WaitForSeconds(1.5f);
        DesastivateDevelopperPanel();

        ResetApplication();

    }


    private void ResetApplication()
    {
        isSubmitButtonPressed = false;
        if (Button_Apply != null) Button_Apply.gameObject.SetActive(true);
        if (Text_Status) Text_Status.text = "";
    }



    private void Init_Text_ApplicationMode()
    {
        if (Text_ApplicationMode.text == null) throw new Exception("no  Text_ApplicationMode.text assigned");
        Text_ApplicationMode.text = setting.ApplicationMode.ToString();
    }


    private void Init_Toggle_AutoHerunterfahren()
    {
        if (Toggle_AutoHerunterfahren == null) throw new Exception("no Toggle_UserFootFloorDistanceCkeck assigned");
        setting.AutoHerunterfahrenActiv = (Toggle_AutoHerunterfahren.isOn) ? true : false;
    }

    private void Init_Toggle_UserFootTracker()
    {
        if (Toggle_UserFootTracker == null) throw new Exception("no Toggle_UserFootTracker selected");
        setting.UserTrackerActiv = (Toggle_UserFootTracker.isOn) ? true : false;
    }

    private void Init_Calibration()
    {
        if (Dropdown_Calibration == null) throw new Exception("no Dropdown_Calibration selected");

        if (Dropdown_Calibration.value == 0)
            setting.CurrentCalibrationOption = Setting.CalibrationOption.wholeSystem;

        else if (Dropdown_Calibration.value == 1)
            setting.CurrentCalibrationOption = Setting.CalibrationOption.kinect_unity;

        else if (Dropdown_Calibration.value == 2)
            setting.CurrentCalibrationOption = Setting.CalibrationOption.pro_unity;

    }

    private void Init_Application()
    {
        if (Dropdown_Application == null) throw new Exception("no Dropdown_Projection selected");
        setting.ApplicationTyp = (Dropdown_Application.value == 0) ? Setting.Application.TiefseeUfer : Setting.Application.Land;
    }

    public void OnDropdowCalibrationValueChanged(int newValue)
    {
        Manager.Instance.OnAnySettingsValueIsChanged();
        if (Dropdown_Application.value == 0) {
            Debug.Log("Dropdown_Projection  ==0");
            return;
        }

        if (newValue == 0 || newValue == 2) {
            Dropdown_Calibration.value = 1;
        }
        //Dropdown_Calibration.value = 1;
        setting.CurrentCalibrationOption = Setting.CalibrationOption.kinect_unity;

    }

    public void OnDropdowProjectionValueChanged(int newValue)
    {
        Manager.Instance.OnAnySettingsValueIsChanged();
        if (newValue == 1) {
            Debug.Log("DropdowProjection ValueChanged " + newValue);
            Dropdown_Calibration.value = newValue;
            setting.CurrentCalibrationOption = Setting.CalibrationOption.kinect_unity;

            Dropdown_Calibration.interactable = false;
            Slider_FishAmount.interactable = false;
        }
        else if (newValue == 0) {
            Dropdown_Calibration.interactable = true;
            Slider_FishAmount.interactable = true;

        }
    }

    public void OnSubmitButtonPressed()
    {
        if (isSubmitButtonPressed == true) return;

        //if the Coroutine reference m_InitApplicationCorutine is not set to an instance 
        if (m_InitApplicationCorutine != null) {
            //stop the coroutine named InitApplicationIE
            StopCoroutine(InitApplicationIE());
            // set the reference m_InitApplicationCorutine to null
            m_InitApplicationCorutine = null;
        }
        // set the coroutine reference m_InitApplicationCorutine to 
        m_InitApplicationCorutine = StartCoroutine(InitApplicationIE());
        Debug.Log(m_InitApplicationCorutine);

        isSubmitButtonPressed = true;
    }

    public void OnSliderFishAmountValueChanged()
    {
        if (setting == null) return;
        Manager.Instance.OnAnySettingsValueIsChanged();
        setting.FishAmount = ((int)Slider_FishAmount.value);
        FishAmountText.text = ((int)Slider_FishAmount.value).ToString();
    }


    public void SelectProjection(int index)
    {
        setting.ApplicationTyp = (index == 0) ? Setting.Application.TiefseeUfer : Setting.Application.Land;
    }

    public void SelectApplicationMode(int index)
    {
        setting.ApplicationMode = (index == 0) ? Setting.Configuration.Debug : Setting.Configuration.Release;
    }

    public void SelectFloor(int index)
    {
        setting.DetectedFloor = Setting.Floor.fromKinect;
    }

    public void EnableUserFootToFloorDistanceCheck(bool value)
    {
        setting.IsDistanceToFloorCheck = value;
    }

    public void EnableUserFootTracker(bool value)
    {
        setting.UserTrackerActiv = value;
    }

    public void SetAutoHerunterfahrenActiv()
    {
        setting.AutoHerunterfahrenActiv = Toggle_AutoHerunterfahren.isOn;
        Debug.Log("value change: " + Toggle_AutoHerunterfahren.isOn);
    }

    public void SetUserFootTrackerActiv()
    {
        setting.UserTrackerActiv = Toggle_UserFootTracker.isOn;
        Debug.Log("value change: " + Toggle_UserFootTracker.isOn);
    }

}
