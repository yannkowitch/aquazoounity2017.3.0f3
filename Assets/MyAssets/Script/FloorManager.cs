﻿
using System;
using UnityEngine;

public class FloorManager : MonoBehaviour
{
    public FloorInterface iFloor { set; get; }
    private KinectDetectedFloor KinectFloor;
    private Manager manager;
    private Setting setting;

    public void Start() { }

    // Use this for initialization
    private void Awake()
    {
        manager = Manager.Instance;
        setting = Setting.Instance;
        KinectFloor = new KinectDetectedFloor();
        iFloor = KinectFloor;
        Debug.Log("selected floor " + iFloor);
    }


    private void OnEnable()
    {
        manager.BodyFrameArrived += OnBodyFrameArrived;

    }

    private void OnDisable()
    {
        manager.BodyFrameArrived -= OnBodyFrameArrived;


    }

   
    private void OnBodyFrameArrived(object sender, Manager.KinectSensorEventArgs e)
    {
        KinectFloor.floorClipPlane = e.frame.FloorClipPlane;
      
    }

}
