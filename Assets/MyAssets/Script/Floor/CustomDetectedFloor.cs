﻿using UnityEngine;

public class CustomDetectedFloor : FloorInterface
{
    // Abstand zum Nullpunkt
    private float d;

    // Normaler Vektor von der Ebene
    private Vector3 n;

    public CustomDetectedFloor()
    {
        Debug.Log("customFloor initialized");
        DefineEquationOfPlane();
    }
   
    private void DefineEquationOfPlane()
    {
        ProCamData proCamData = DataManager.LoadData();
        // 3 Punkten in der Ebene
        Vector3 p1 = proCamData.Kinect_3DPoint1;
        Vector3 p2 = proCamData.Kinect_3DPoint2;
        Vector3 p3 = proCamData.Kinect_3DPoint3;

        // Richtungsvektoren u, v
        Vector3 u = (p3 - p2);
        Vector3 v = (p1 - p2);

        // Normaler Vektor n zur der Ebene
        n = Vector3.Cross(u, v);

        // Das Skalarprodukt von b und n -> Abstand Ebene zum Nullpunkt
        d = Vector3.Dot(p2, n);

        // Hesse'sche Normalform bedingung: |n| =1; und d >= 0
        if (d < 0)
        {
            d = -d;
            n = -n;
        }

        // Norm oder Betrag des Normalen Vektors n
        float n_Norm = Mathf.Sqrt(n.x * n.x + n.y * n.y + n.z * n.z);

        // Normaler Vektor normieren
        n = n / n_Norm;

        d = d / n_Norm;

        Debug.Log("equation of plane is defined");

        Debug.Log("Adstand der CustomFloor zum Ursprung: "+d);
    }

    // Calculates the distance between any 3D point and the floor (plane).
    public float GetFloorTo3DPointDistance(Vector3 point)
    {
        float result = Mathf.Abs(Vector3.Dot(point, n) - d);
        return result;
    }

    //describes the height where the Kinect sensor is positioned! the distance between the floor and the origin of the coordinate system
    public float GetFloorToSensorDistance()
    {
        return d;
    }
}
