﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface FloorInterface
{
    float GetFloorTo3DPointDistance(Vector3 point);
    //describes the height where the Kinect sensor is positioned!
    float GetFloorToSensorDistance();
}
