﻿using UnityEngine;

public class KinectDetectedFloor : FloorInterface
{
    public Windows.Kinect.Vector4 floorClipPlane { get;  set; }
   
    public KinectDetectedFloor()
    {
        Debug.Log("kinectFloor initialized");
    }

    // Calculates the distance between any 3D point and the floor (plane).
    public float GetFloorTo3DPointDistance(Vector3 point3D)
    {
     //   Debug.Log("KinectDetectedFloortest");
        float numerator = floorClipPlane.X * point3D.x + floorClipPlane.Y * point3D.y + floorClipPlane.Z * point3D.z + floorClipPlane.W;
        float denominator = Mathf.Sqrt(floorClipPlane.X * floorClipPlane.X + floorClipPlane.Y * floorClipPlane.Y + floorClipPlane.Z * floorClipPlane.Z);
        return (numerator / denominator);
    }

    //describes the height where the Kinect sensor is positioned! the distance between the floor and the origin of the coordinate system
    public float GetFloorToSensorDistance()
    {
        return floorClipPlane.W; 
    }

}
