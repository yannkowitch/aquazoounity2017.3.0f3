﻿using UnityEngine;

[System.Serializable]
public class ProCamData
{
    public Vector3 Kinect_3DPoint1, Kinect_3DPoint2, Kinect_3DPoint3, Kinect_3DPoint4;
    public Vector3 unity_3DPoint1, unity_3DPoint2, unity_3DPoint3, unity_3DPoint4;
}
