﻿
using UnityEngine;

public class MaterialFader : IClient
{
    private Material material;
    public float fadeSpeed { set; get; }


    public MaterialFader(GameObject gameobject)
    {
        material = gameobject.GetComponent<MeshRenderer>().material;
    }

    float IClient.fadeSpeed
    {
        get
        {
            return fadeSpeed;
        }

        set
        {
            fadeSpeed = value;
        }
    }

    void IClient.SetColorImage(ref float alpha, Fader.FadeDirection fadeDirection)
    {
        if (material != null)
        {
            material.color = new Color(material.color.r, material.color.g, material.color.b, alpha);
            alpha += Time.deltaTime * (1.0f / fadeSpeed) * ((fadeDirection == Fader.FadeDirection.Out) ? -1 : 1);
        }
    }



}
