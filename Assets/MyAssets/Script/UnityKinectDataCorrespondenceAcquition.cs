﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityKinectDataCorrespondenceAcquition : MonoBehaviour
{
    public ProCamData calibrationData { get; private set; }
    private GameObject[] unityReferencePoints;
    private Vector3 targetPosition;
    [SerializeField] GameObject cubePointer;
    private byte currentIndex;
    [SerializeField] private Vector3 kinect_UserJoint_RightFoot;

    [SerializeField] private Vector3 kinect_UserJoint_LeftFoot;

    [SerializeField] GameObject ReferencePointsParent;


    [Header("Time to wait befor the user move to the next point ")]
    [SerializeField] private float waitingTime = 10;

    [Header("Time to wait befor the first correspodence point appears in the picture")]
    [SerializeField] private float startWaitingTime = 10;

    [Header("TiefSee Setting: ReferencePoints orientation ")]
    [SerializeField] private float x_Rot, y_Rot, z_Rot;

    [Header("Land Setting: ReferencePoints orientation ")]
    [SerializeField] private float X_Rot, Y_Rot, Z_Rot;
    byte maxIndex;
    private Coroutine m_TriggerCorrespondenceAcquitionCoroutine;
    private Setting setting;
    private Manager manager;

    private void Awake()
    {
        setting = Setting.Instance;
        manager = Manager.Instance;
        calibrationData = new ProCamData();
        unityReferencePoints = new GameObject[4];


    }

    IEnumerator startCorrespondenceAcquition()
    {
        yield return new WaitForSeconds(5);
        unityReferencePoints[currentIndex].SetActive(true);
        targetPosition = unityReferencePoints[currentIndex].transform.position;
        Debug.Log("go to " + unityReferencePoints[currentIndex].name);

        TriggerCorrespondenceAcquition();
    }

    private void Start()
    {
        currentIndex = 0;
        maxIndex = 3;

        RotateReferencePoints();

        DesactivateAllunityReferencePoints();

        if (setting.ApplicationTyp != Setting.Application.Land) return;
        StartCoroutine(startCorrespondenceAcquition());

    }

    private void OnSavedRecordedPointsLoaded(object sender, EventArgs e)
    {
        StartCoroutine(startCorrespondenceAcquition());
    }

    private void OnEnable()
    {
        manager.leftFootTracked += OnleftFootTracked;
        manager.RightFootTracked += OnRightFootTracked;
        manager.SavedRecordedPointsLoaded += OnSavedRecordedPointsLoaded;
    }

    private void OnDisable()
    {
        manager.leftFootTracked -= OnleftFootTracked;
        manager.RightFootTracked -= OnRightFootTracked;
        manager.SavedRecordedPointsLoaded -= OnSavedRecordedPointsLoaded;
        
    }

    #region CALLBACK FUNCTIONS

    private void OnleftFootTracked(object sender, Manager.KinectSensorEventArgs e)
    {
        this.kinect_UserJoint_LeftFoot = e.leftFootPositionInUnity;
    }

    private void OnRightFootTracked(object sender, Manager.KinectSensorEventArgs e)
    {
        this.kinect_UserJoint_RightFoot = e.rightFootPositionInUnity;
    }
    #endregion //CALLBACK FUNCTIONS

    private void RotateReferencePoints()
    {
        if (setting.ApplicationTyp == Setting.Application.TiefseeUfer)

            ReferencePointsParent.transform.rotation = Quaternion.Euler(x_Rot, y_Rot, z_Rot);
        else if (setting.ApplicationTyp == Setting.Application.TiefseeUfer)
            ReferencePointsParent.transform.rotation = Quaternion.Euler(X_Rot, Y_Rot, Z_Rot);

    }

    private void DesactivateAllunityReferencePoints()
    {
        if (unityReferencePoints != null)
        {
            for (int i = 0; i < unityReferencePoints.Length; i++)
            {
                unityReferencePoints[i] = GameObject.FindWithTag("Point" + (i + 1));
                unityReferencePoints[i].SetActive(false);
            }
        }
        else
        {
            throw new Exception("UnityReferencePoints need to be initialized");
        }

    }

    private IEnumerator TriggerCorrespondenceAcquitionIE()
    {
        Debug.Log("first AutoCapture  in " + waitingTime + "seconds");
        /*
        yield return new WaitForSeconds(waitingTime);
        Capture_Kinect3DPoint_WithRespective_Unity3DPoint();
        yield return new WaitForSeconds(waitingTime);
        Capture_Kinect3DPoint_WithRespective_Unity3DPoint();
        yield return new WaitForSeconds(waitingTime);
        Capture_Kinect3DPoint_WithRespective_Unity3DPoint();
        yield return new WaitForSeconds(waitingTime);
        Capture_Kinect3DPoint_WithRespective_Unity3DPoint();
        */

        for (int i = 0; i < unityReferencePoints.Length; i++)
        {
            yield return new WaitForSeconds(waitingTime);
            Capture_Kinect3DPoint_WithRespective_Unity3DPoint();
        }
    }


    private void TriggerCorrespondenceAcquition()
    {
        if (m_TriggerCorrespondenceAcquitionCoroutine != null)
        {
            StopCoroutine(TriggerCorrespondenceAcquitionIE());
            m_TriggerCorrespondenceAcquitionCoroutine = null;
        }
        m_TriggerCorrespondenceAcquitionCoroutine = StartCoroutine(TriggerCorrespondenceAcquitionIE());
    }


    private void Set_Kinect3DPoint_WithRespective_Unity3DPoint(int positionIndex)
    {
        switch (positionIndex)
        {
            case 0:
                if (setting.ApplicationMode == Setting.Configuration.Release)
                {
                    calibrationData.Kinect_3DPoint1 = kinect_UserJoint_RightFoot;
                }

                else if (setting.ApplicationMode == Setting.Configuration.Debug)
                {
                    calibrationData.Kinect_3DPoint1 = new Vector3(0.4f, -1.0f, 3.1f);
                }

                calibrationData.unity_3DPoint1 = unityReferencePoints[0].transform.position;


                break;
            case 1:
                if (setting.ApplicationMode == Setting.Configuration.Release)
                {
                    calibrationData.Kinect_3DPoint2 = kinect_UserJoint_LeftFoot;
                }

                else if (setting.ApplicationMode == Setting.Configuration.Debug)
                {
                    calibrationData.Kinect_3DPoint2 = new Vector3(-0.7f, -1.0f, 3.1f);
                }

                calibrationData.unity_3DPoint2 = unityReferencePoints[1].transform.position;


                break;

            case 2:
                if (setting.ApplicationMode == Setting.Configuration.Release)
                {
                    calibrationData.Kinect_3DPoint3 = kinect_UserJoint_LeftFoot;
                }

                else if (setting.ApplicationMode == Setting.Configuration.Debug)
                {
                    calibrationData.Kinect_3DPoint3 = new Vector3(-0.6f, -1.0f, 2.2f);
                }

                calibrationData.unity_3DPoint3 = unityReferencePoints[2].transform.position;


                break;

            case 3:
                if (setting.ApplicationMode == Setting.Configuration.Release)
                {
                    calibrationData.Kinect_3DPoint4 = kinect_UserJoint_RightFoot;
                }

                else if (setting.ApplicationMode == Setting.Configuration.Debug)
                {
                    calibrationData.Kinect_3DPoint4 = new Vector3(0.5f, -0.5f, 2.3f);
                }

                calibrationData.unity_3DPoint4 = unityReferencePoints[3].transform.position;


                break;

            default:

                break;

        }

        Debug.Log(unityReferencePoints[positionIndex] + " captured");
    }

    private void Capture_Kinect3DPoint_WithRespective_Unity3DPoint()
    {
        if (currentIndex <= maxIndex)
        {
            Set_Kinect3DPoint_WithRespective_Unity3DPoint(currentIndex);

            manager.OnPointCaptured(this);
            unityReferencePoints[currentIndex].SetActive(false);

            currentIndex++;

            if (currentIndex < unityReferencePoints.Length)
            {
                unityReferencePoints[currentIndex].SetActive(true);
                targetPosition = unityReferencePoints[currentIndex].transform.position;

                Debug.Log("go to " + unityReferencePoints[currentIndex].name);
            }

            if (currentIndex <= maxIndex)
                return;

            manager.OnPointsCaptured(this);
            manager.OnLoadSceneTrigged(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        }

    }


    private void LateUpdate()
    {
        targetPosition.y = 0;
        cubePointer.transform.position = Vector3.Lerp(cubePointer.transform.position, targetPosition, 1.5f * Time.deltaTime);
    }

   



}
