﻿
using UnityEngine;

public class UnityKinectDataSaver : MonoBehaviour
{
    private bool dataSaved;
    Manager manager;

    // Use this for initialization
    private void Awake()
    {
        manager = Manager.Instance;
    }

    private void Start()
    {

    }

    private void OnEnable()
    {
        manager.PointsCaptured += OnPointsCaptured;
    }

    private void OnDisable()
    {
        manager.PointsCaptured -= OnPointsCaptured;
    }

    private void OnPointsCaptured(object sender, Manager.UnityKinectDataCorrespondenceAcquitionEventArgs e)
    {
        if (dataSaved) return;
        Debug.Log("data speichern");
        DataManager.SaveData(e.calibrationData);
        Debug.Log("data :" + e.calibrationData);
        dataSaved = true;
    }

}
