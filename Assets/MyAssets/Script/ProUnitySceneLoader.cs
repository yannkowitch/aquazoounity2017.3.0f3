﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProUnitySceneLoader : MonoBehaviour
{
    Manager manager;
    Setting setting;
    // Use this for initialization
    void Start()
    {

    }
    private void Awake()
    {
        manager = Manager.Instance;
        setting = Setting.Instance;
    }

    private void OnEnable()
    {
        manager.LoadSceneTrigged += OnLoadSceneTrigged;
    }

    private void OnDisable()
    {
        manager.LoadSceneTrigged -= OnLoadSceneTrigged;
    }

    private void OnLoadSceneTrigged(object sender, LoadSceneEventArgs e)
    {
        if (e.activeSceneBuildIndex != UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex) return;

        switch (setting.CurrentCalibrationOption)
        {
            case Setting.CalibrationOption.wholeSystem:
                StartCoroutine(FindObjectOfType<SceneFader>().FadeAndLoadScene(Fader.FadeDirection.In, 2));
                break;
            case Setting.CalibrationOption.kinect_unity:
                break;
            case Setting.CalibrationOption.pro_unity:
                StartCoroutine(FindObjectOfType<SceneFader>().FadeAndLoadScene(Fader.FadeDirection.In, 3));
                break;
            default:
                break;
        }
    }


}
