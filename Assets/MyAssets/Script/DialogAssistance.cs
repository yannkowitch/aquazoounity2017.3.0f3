﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


public class DialogAssistance : MonoBehaviour
{
    public GameObject Dialog { set; get; }
    public Text TextUI { set; get; }
    private Coroutine m_ActivationCoroutine;
    private Coroutine m_TextTypingCoroutine = null;
    public bool isTextTyping { private set; get; }
    public string lastTypedText { private set; get; }
    private Manager manager;

    private void Start()
    {

    }

    private void Awake()
    {
        manager = Manager.Instance;
    }
   
    public void ActivateDialogCanvas(bool activate, float delay)
    {
        if (m_ActivationCoroutine != null)
        {
            StopCoroutine(m_ActivationCoroutine);
            m_ActivationCoroutine = null;
        }

        m_ActivationCoroutine = StartCoroutine(ActivateDialogCanvasIE(activate, delay));
    }

    private IEnumerator ActivateDialogCanvasIE(bool activate, float delay)
    {
        yield return new WaitForSeconds(delay);
        if (Dialog == null) yield return null;
        Dialog.SetActive(activate);
    }

    public void TypeText(string text, float delay)
    {
        if (m_TextTypingCoroutine != null)
        {
            StopCoroutine(m_TextTypingCoroutine);
            m_TextTypingCoroutine = null;
        }
        m_TextTypingCoroutine = StartCoroutine(TypeTextIE(text, delay));
    }

    private IEnumerator TypeTextIE(string TextStr, float delay)
    {

        yield return new WaitForSeconds(delay);

        if (TextUI == null) yield return null;

        TextUI.text = "";
        manager.OnTextIsTypingHasBegan(TextStr);
        isTextTyping = true;

        foreach (char letter in TextStr.ToCharArray())
        {
            TextUI.text += letter;
            //sentence at the end

            if (TextUI.text.Length == TextStr.Length)
            {
                manager.OnTextTypingEnded(TextStr);
                lastTypedText = TextStr;
                isTextTyping = false;
            }
            else
            {
                manager.OnTextTyping(TextStr);
            }
            // wait a single frame
            //yield return null;
            yield return new WaitForSeconds(0.03f);
        }
    }

}
