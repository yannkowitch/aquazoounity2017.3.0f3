﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using System;

public class KinectUnityGameobjectsDestroyer : MonoBehaviour
{
    private bool mChangeLevel = false;
    private Manager manager;
    private Setting setting;
    /*
    [Header("Time to wait before loading the next scene ")]
    [SerializeField] private float waitingTime = 3;
    */
    [SerializeField] private GameObject[] LandComponents;
    [SerializeField] private GameObject[] TiefseeUferComponents;
    private Coroutine m_LoadNextSceneIECoroutine;
    private MainCameraHandler cameraHandler;
    // Use this for initialization
    private void Awake()
    {
        manager = Manager.Instance;
        setting = Setting.Instance;
        cameraHandler = FindObjectOfType<MainCameraHandler>() as MainCameraHandler;
    }

    private void Start()
    {
        switch (setting.ApplicationTyp)
        {
            case Setting.Application.TiefseeUfer:
                if (cameraHandler != null && cameraHandler.enabled)
                    cameraHandler.enabled = false;

                foreach (GameObject go in LandComponents)
                    if (go != null)
                        Destroy(go);
                break;
            case Setting.Application.Land:
                if (cameraHandler != null && !cameraHandler.enabled)
                    cameraHandler.enabled = true;

                foreach (GameObject go in TiefseeUferComponents)
                    if (go != null)
                        Destroy(go);
                break;
            default:
                break;
        }

    }




    /*
    private void OnEnable()
    {
        manager.OnPointsCaptured += OnPointsCaptured;
    }

    private void OnDisable()
    {
        manager.OnPointsCaptured -= OnPointsCaptured;
    }
 
    private void OnPointsCaptured(object sender, EventArgs e)
    {

        if (mChangeLevel == true) return;
     
        StartCoroutine(LoadNextSceneAsync(3, waitingTime));
        mChangeLevel = true;
    }
  
    private IEnumerator LoadNextSceneAsync(int i,float waitingTime)
    {
        yield return new WaitForSeconds(waitingTime);
        StartCoroutine(FindObjectOfType<SceneFader>().FadeAndLoadScene(Fader.FadeDirection.In, i));
    }
    */
}
