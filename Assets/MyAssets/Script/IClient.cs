﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IClient
{
    float fadeSpeed { set; get; }
    void SetColorImage(ref float alpha, Fader.FadeDirection fadeDirection);
}

