﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerButton : MonoBehaviour
{
    public int Index { get; private set; }   
    private bool isApplicationQuitting = false;
    private Manager manager;

    private void Awake()
    {
        manager = Manager.Instance;
    }

    private void Start()
    {
        if (this.gameObject.name.Equals("TriggerButton"))
        {
            Index = 0;
           
            Debug.Log("indexierung erfolreich");
        }

        manager.OnTriggerButtonCreated(this);
    }

    private void OnApplicationQuit()
    {
        isApplicationQuitting = true;
    }

    private void OnDestroy()
    {
        if (!isApplicationQuitting)
        {
            manager.TriggerButtonIsDestroyed(this);
        }
    }

    private void OnEnable()
    {
        manager.OnTriggerButtonEnabled(this);
    }


}
