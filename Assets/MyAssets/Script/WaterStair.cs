﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterStair : MonoBehaviour
{
    [SerializeField] float waterStairlerpSpeed = 0.3f;
    public GameObject waterStair;
    public float waterStairMinHeight = 0;
    public float waterStairMaxHeight = 0;
    // Use this for initialization
    void Start()
    {
        waterStairMaxHeight = waterStair.transform.position.x;
        waterStairMinHeight = waterStairMaxHeight - 4;
    }

    // Update is called once per frame
   private void OscInitializeWaterStairReceive(Environment.Level currentSuimonoPreset)
    {
        float wasserTreppedHeight = (currentSuimonoPreset == Environment.Level.Tiefsee) ? waterStairMaxHeight : waterStairMinHeight;
        Vector3 waterTreppeStartPosition = new Vector3(wasserTreppedHeight, waterStair.transform.position.y, waterStair.transform.position.z);
        waterStair.transform.position = waterTreppeStartPosition;
    }
    private void OscTransformWaterReceive(Environment.Level currentSuimonoPreset)
    {
        //  send osc message hier
        TransformWaterWaterStair(currentSuimonoPreset);
    }

    private void TransformWaterWaterStair(Environment.Level currentSuimonoPreset)
    {
        Direction waserTreppeMovedirection = (currentSuimonoPreset == Environment.Level.Tiefsee) ? Direction.Up : Direction.Down;
        StartCoroutine(MoveWaserTreppe(waserTreppeMovedirection, currentSuimonoPreset));

    }

    private IEnumerator MoveWaserTreppe(Direction direction, Environment.Level currentSuimonoPreset)
    {

        float xOffset = (direction == Direction.Up) ? waterStairMaxHeight : waterStairMinHeight;
        Vector3 endWaterTreppePosition = new Vector3(xOffset, waterStair.transform.position.y, waterStair.transform.position.z);

        while (Vector3.Distance(waterStair.transform.position, endWaterTreppePosition) >= 0.25f)
        {
            if (waterStair != null)
                waterStair.transform.position = Vector3.Lerp(waterStair.transform.position, endWaterTreppePosition, (waterStairlerpSpeed) * Time.deltaTime);

            yield return null; // make the loop wait until the next frame before continuing
        }
    }



}
