﻿using System;
using System.Collections;
using UnityEngine;

public class KinectUnitySceneLoader : MonoBehaviour
{

    private Manager manager;
    private Setting setting;
    [Header("Time to wait before loading the next scene ")]
    [SerializeField] private float waitingTime = 3;
    // Use this for initialization
    void Start()
    {

    }
    private void Awake()
    {
        manager = Manager.Instance;
        setting = Setting.Instance;
    }

    private void OnEnable()
    {
        manager.LoadSceneTrigged += OnLoadSceneTrigged;

    }

    private void OnDisable()
    {
        manager.LoadSceneTrigged -= OnLoadSceneTrigged;

    }

    private void OnPointsCaptured(object sender, Manager.UnityKinectDataCorrespondenceAcquitionEventArgs e)
    {
        throw new NotImplementedException();
    }

    private void OnLoadSceneTrigged(object sender, LoadSceneEventArgs e)
    {
        if (e.activeSceneBuildIndex != UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex) return;

        StartCoroutine(LoadNextSceneAsync(3, waitingTime));
    }
    private IEnumerator LoadNextSceneAsync(int i, float waitingTime)
    {
        yield return new WaitForSeconds(waitingTime);
        StartCoroutine(FindObjectOfType<SceneFader>().FadeAndLoadScene(Fader.FadeDirection.In, i));
    }
}
