﻿using UnityEngine;

public class CoordinateMapper3D
{
    public Matrix4x4 CalculateTransformationMatrix(Vector3 _p1K, Vector3 _p2K, Vector3 _p3K, Vector3 _p4K, Vector3 _p1U, Vector3 _p2U, Vector3 _p3U, Vector3 _p4U)
    {
        Matrix4x4 TransformationMatrix = new Matrix4x4();
        Matrix4x4 UnityMatrix = new Matrix4x4();
        Matrix4x4 KinectMatrix = new Matrix4x4();

        // Überführung aller 3d Kinect-Vektoren in HomogeneKoordinaten
        Vector4 p1K = ConvertPointsToHomogeneous(_p1K);
        Vector4 p2K = ConvertPointsToHomogeneous(_p2K);
        Vector4 p3K = ConvertPointsToHomogeneous(_p3K);
        Vector4 p4K = ConvertPointsToHomogeneous(_p4K);

        KinectMatrix.SetColumn(0, p1K);
        KinectMatrix.SetColumn(1, p2K);
        KinectMatrix.SetColumn(2, p3K);
        KinectMatrix.SetColumn(3, p4K);


        // Überführung aller 3d Unity-Vektoren in HomogeneKoordinaten
        Vector4 p1U = ConvertPointsToHomogeneous(_p1U);
        Vector4 p2U = ConvertPointsToHomogeneous(_p2U);
        Vector4 p3U = ConvertPointsToHomogeneous(_p3U);
        Vector4 p4U = ConvertPointsToHomogeneous(_p4U);


        UnityMatrix.SetColumn(0, p1U);
        UnityMatrix.SetColumn(1, p2U);
        UnityMatrix.SetColumn(2, p3U);
        UnityMatrix.SetColumn(3, p4U);

  
        TransformationMatrix = UnityMatrix * KinectMatrix.inverse;

        // set the 4th row of the TransformationMatrix to [0 0 0 1]
        TransformationMatrix.SetRow(3, new Vector4(0, 0, 0, 1));

        return TransformationMatrix;

    }

    public Vector3 Transform3dPointFromKinectToUnitySpace(Matrix4x4 TransformationMatrix, Vector3 Kinect3DPoint)
    {
        Vector3 Unity3DPoint = TransformationMatrix.MultiplyPoint3x4(Kinect3DPoint);

        return Unity3DPoint;
    }

    private Vector4 ConvertPointsToHomogeneous(Vector3 V3)
    {
        Vector4 V4 = new Vector4(V3.x, V3.y, V3.z, 1);
        return V4;
    }

}
