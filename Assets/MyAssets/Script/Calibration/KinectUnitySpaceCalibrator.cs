﻿
using System;
using System.Collections;
using UnityEngine;
using Utils;

public class KinectUnitySpaceCalibrator : Singleton<KinectUnitySpaceCalibrator>
{
    private CoordinateMapper3D mapper3D;
    private ProCamData calibrationData;
    private Setting setting;
    private Manager manager;
  
    /* 
     * transform (kinect)righthandcoordinate  into  (Unity)lefthandcoordinate and transform kinectcoordinatensystem into unity worlcoordinatensystem
     * KoordinatenSystemWechselMatrix (BasisWechselMatrix) kodiert die  transformation, die jeder Punkt im Kinect koordinatensystem aus de Sicht von Unity Koordinatensystem beschriebt 
     * 1- die  3 ersten SpaltenVektoren (RotationMatrix) repräsitieren  die 3 Basisvektoren von der Kinect im (relativ zu) Unity-Koordinatensystem
     * 2- die letzte SpalteVektor (Verschiebungsvektor) repräsentiert die Verschiebung von der Ursprungsvektor der Kinect  im (relativ zu) Unity-Koordinatesystem
    */
    [SerializeField] private Matrix4x4 transformationMatrix = Matrix4x4.identity;

    [SerializeField] bool showDebug = false;

    public Vector3 GetKinectPositionInUnity()
    {
        Matrix4x4 M = transformationMatrix;
        return new Vector3(M.m03, M.m13, M.m23);
    }

    public Quaternion QuaternionFromMatrix(Matrix4x4 m)
    {
        return Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1));
    }

    public Quaternion GetKinectOrientation()
    {
        Matrix4x4 M = transformationMatrix;
        M.m03 = 0;
        M.m13 = 0;
        M.m23 = 0;


        Quaternion quaternion = QuaternionFromMatrix(M);

        return quaternion;
        //    Debug.Log(transformationMatrix.euler)
    }

    private void OnEnable()
    {
        manager.KinectUnityCalibrationDataLoaded += OnKinectUnityCalibrationDataLoaded;      
    }

    private void OnDisable()
    {
        manager.KinectUnityCalibrationDataLoaded -= OnKinectUnityCalibrationDataLoaded;     
    }

    private void OnKinectUnityCalibrationDataLoaded(object sender, ProCamDataArgs e)
    {
        // load kinect and UnityData for calibration
        calibrationData = e.ProCamData;
        
        transformationMatrix = mapper3D.CalculateTransformationMatrix(calibrationData.Kinect_3DPoint1, calibrationData.Kinect_3DPoint2, calibrationData.Kinect_3DPoint3, calibrationData.Kinect_3DPoint4,
                                                                calibrationData.unity_3DPoint1, calibrationData.unity_3DPoint2, calibrationData.unity_3DPoint3, calibrationData.unity_3DPoint4);

        manager.OnCalibrationMatrixCalculated();

        Debug.Log("transformationMatrix");
        Debug.Log(transformationMatrix);

        if (showDebug)
        {

            //Debug.Log("InverseTransformationMatrix");
            //Debug.Log(transformationMatrix.inverse);

            Debug.Log("unityHeight : " + calibrationData.unity_3DPoint4.y);
            Debug.Log("kinectMappedHeight : " + calibrationData.Kinect_3DPoint4.y);

            Debug.Log("detrminant : " + transformationMatrix.determinant);
            Debug.Log("Point1_Kinect : " + calibrationData.Kinect_3DPoint1);
            Debug.Log("Point2_Kinect : " + calibrationData.Kinect_3DPoint2);
            Debug.Log("Point3_Kinect : " + calibrationData.Kinect_3DPoint3);
            Debug.Log("Point4_Kinect : " + calibrationData.Kinect_3DPoint4);

            Debug.Log("Point1_Unity : " + MapKinect3DPointToUnity3DPoint(calibrationData.Kinect_3DPoint1));
            Debug.Log("Point2_Unity : " + MapKinect3DPointToUnity3DPoint(calibrationData.Kinect_3DPoint2));
            Debug.Log("Point3_Unity : " + MapKinect3DPointToUnity3DPoint(calibrationData.Kinect_3DPoint3));
            Debug.Log("Point4_Unity : " + MapKinect3DPointToUnity3DPoint(calibrationData.Kinect_3DPoint4));
            Debug.Log("randomPoint4Y_Unity : " + MapKinect3DPointToUnity3DPoint(new Vector3(calibrationData.Kinect_3DPoint4.x, -1f, calibrationData.Kinect_3DPoint4.z)));
        }
    }

    // Use this for initialization
    private void Awake()
    {
        manager = Manager.Instance;
        setting = Setting.Instance;
        mapper3D = new CoordinateMapper3D();      
    }

    // Update is called once per frame
    public Vector3 MapKinect3DPointToUnity3DPoint(Vector3 kinectPoint)
    {
        Vector3 unityMappedPoint = mapper3D.Transform3dPointFromKinectToUnitySpace(transformationMatrix, kinectPoint);

        return unityMappedPoint;
    }
}
