﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class KinectUnityCalibrationDataLoader : MonoBehaviour
{
    private Setting setting;
    private Manager manager;

    private void Awake()
    {
        setting = Setting.Instance;
        manager = Manager.Instance;
    }

    private IEnumerator Start()
    {
        if (setting.ApplicationTyp == Setting.Application.TiefseeUfer) yield return null;
        StartCoroutine(LoadCalibrationData(setting.WaitingTimeBeforCalibratingKinectUnity));
    }

    private IEnumerator LoadCalibrationData(float waitingTime)
    {
        yield return new WaitForSeconds(waitingTime);
        DataManager.LoadData();
    }

    private void OnEnable()
    {
        if (setting.ApplicationTyp != Setting.Application.TiefseeUfer) return;
            manager.ProjectorCameraCalibrationTrigged += OnProjectorCameraCalibrationTrigged;
    }

    private void OnDisable()
    {
        if (setting.ApplicationTyp != Setting.Application.TiefseeUfer) return;
            manager.ProjectorCameraCalibrationTrigged -= OnProjectorCameraCalibrationTrigged;
    }

    private void OnProjectorCameraCalibrationTrigged(object sender, EventArgs e)
    {
        StartCoroutine(LoadCalibrationData(setting.WaitingTimeBeforCalibratingProjectorUnity));
    }

}
