﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class ProjectorUnityCameraCalibration : MonoBehaviour
{

    [Header("Time to wait befor the first correspodence point appears in the picture")]
    [SerializeField] private float startWaitingTime = 1;
    private Manager manager;
    private Setting setting;
    PointsRecordReplay pointsRecordReplay;

    private void Awake()
    {
        setting = Setting.Instance;
        manager = Manager.Instance;
        pointsRecordReplay = FindObjectOfType<PointsRecordReplay>() as PointsRecordReplay;

    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(startWaitingTime);

        if (setting.ApplicationTyp == Setting.Application.TiefseeUfer)
        {
            pointsRecordReplay.LoadSavedRecordedPoints();
        }
       // FindObjectOfType<CorrespondenceAcquisition>().ToggleCalibrating();

    }
    private void OnEnable()
    {
        manager.ProjectorCameraCalibrationTrigged += OnProjectorCameraCalibrationTrigged;
    }

    private void OnDisable()
    {
        manager.ProjectorCameraCalibrationTrigged -= OnProjectorCameraCalibrationTrigged;
    }

    private void OnProjectorCameraCalibrationTrigged(object sender, EventArgs e)
    {
        if (setting.ApplicationTyp != Setting.Application.TiefseeUfer) return;

        pointsRecordReplay.enabled = false;
        FindObjectOfType<CorrespondenceAcquisition>().ToggleCalibrating();
    }

}
