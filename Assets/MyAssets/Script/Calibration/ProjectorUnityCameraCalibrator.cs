﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ProjectorUnityCameraCalibrator : MonoBehaviour
{

    private Manager manager;
    private Setting setting;
    private PointsRecordReplay pointsRecordReplay;


    private void Awake()
    {
        setting = Setting.Instance;
        manager = Manager.Instance;
        pointsRecordReplay = FindObjectOfType<PointsRecordReplay>();

    }

    private IEnumerator Start()
    {
        manager.SceneIsStarted();

        if (setting.ApplicationTyp != Setting.Application.TiefseeUfer)
        {
            Debug.Log("land");
            yield return null;
        }

        Debug.Log("tiefsee");
        yield return new WaitForSeconds(setting.WaitingTimeBeforCalibratingProjectorUnity);

        CalibrateUnityCameraWithProjector();
    }

    private void CalibrateUnityCameraWithProjector()
    {
        pointsRecordReplay.LoadSavedRecordedPoints();
    }

    private void OnEnable()
    {
        manager.ProjectorCameraCalibrationTrigged += OnProjectorCameraCalibrationTrigged;
    }

    private void OnDisable()
    {
        manager.ProjectorCameraCalibrationTrigged -= OnProjectorCameraCalibrationTrigged;
    }

    private void OnProjectorCameraCalibrationTrigged(object sender, EventArgs e)
    {
        if (setting.ApplicationTyp != Setting.Application.TiefseeUfer)
        {
           // Debug.Log("land");
            return;
        }
       // Debug.Log("tiefsee");
        pointsRecordReplay.enabled = false;
        FindObjectOfType<CorrespondenceAcquisition>().ToggleCalibrating();
    }
}
