﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraHandler : MonoBehaviour
{
    private Camera CameraObject;
    [Range(30, 60)]
    public float fieldOfView = 40;   //50   ,58
    [Range(10, 20)]
    public float height = 20;        //14  , 20
    private Vector3 Position;


    // Use this for initialization
    void Start()
    {
        CameraObject = this.gameObject.GetComponent<Camera>() as Camera;
        Position = CameraObject.transform.position;
        if (!CameraObject.orthographic)
            CameraObject.orthographic = true;
        CameraObject.orthographicSize = 7;
        // height = Position.y;
        // fieldOfView = CameraObject.fieldOfView;

    }

    // Update is called once per frame
    void LateUpdate()
    {
        //set the Camera fieldOfView
        CameraObject.fieldOfView = fieldOfView;

        //set the Camera Height
        this.Position.y = height;

        //set the Camera position
        CameraObject.transform.position = this.Position;
    }
}
