﻿using UnityEngine;
using Windows.Kinect;
using System.Collections.Generic;


public class KinectSensor : MonoBehaviour
{
    #region MEMBER_VARIABLES    
    private Windows.Kinect.KinectSensor sensor = null;
    private BodyFrameReader bodyReader = null;
    private Body[] bodies = null;
    // private CameraIntrinsics cameraIntrinsics;

    private KinectJointFilter filter;

    private Dictionary<int, ulong> collectionOfUsers = new Dictionary<int, ulong>();
    private int usersAmount = 0;
    public bool showDebug;
    // create a reference of event publischer object
    private Manager manager;

    #endregion // MEMBER_VARIABLES

    // Use this for initialization

    private void Awake()
    {
        manager = Manager.Instance;
    }


    private void Start()
    {

        sensor = Windows.Kinect.KinectSensor.GetDefault();

        //on setup:  create an instance of the class, one for each body if you want to filter more bodies:
        filter = new KinectJointFilter();
        //   filter.Init(0.25f, 0.25f, 0.25f, 0.03f, 0.05f)|| filter.Init(0.75f, 0.1f, 0.0f, 0.01f, 0.03f); // change params if you want
        filter.Init(0.85f, 0.1f, 0.0f, 0.01f, 0.03f);

        if (sensor != null)
        {
            bodyReader = sensor.BodyFrameSource.OpenReader();
            bodyReader.FrameArrived += BodyReader_FrameArrived;
            if (!sensor.IsOpen)
            {
                sensor.Open();

                if (showDebug)
                    Debug.Log("Kinect is open");
            }
        }
    }
    /// <summary>
    ///  This Callback/eventhandler BodyReader_FrameArrived  will be called whenever a new BodyFrame is available.
    /// </summary>

    private void BodyReader_FrameArrived(object sender, BodyFrameArrivedEventArgs e)
    {
        using (BodyFrame frame = e.FrameReference.AcquireFrame())
        {
            if (frame == null) return;

            // if (frame != null) {
            manager.OnBodyFrameArrived(this, frame);

            // detect bodies
            bodies = new Body[frame.BodyFrameSource.BodyCount];
            frame.GetAndRefreshBodyData(bodies);

            if (bodies == null) return;

            for (int userID = 0; userID < bodies.Length; userID++)
            {
                Body body = bodies[userID];

                if (body != null)
                {
                    //if any body/user is tracked...
                    if (body.IsTracked)
                    {
                        if (!collectionOfUsers.ContainsKey(userID))
                        {
                            collectionOfUsers.Add(userID, body.TrackingId);
                            usersAmount = collectionOfUsers.Count;

                            if (showDebug)
                            {
                                Debug.Log("user " + userID + " is tracked");
                                Debug.Log("usersamount : " + usersAmount);
                            }
                        }

                        foreach (Windows.Kinect.Joint joint in body.Joints.Values)
                        {
                            //if any joint is tracked...
                            if (joint.TrackingState == TrackingState.Tracked || joint.TrackingState == TrackingState.Inferred)
                            {
                                // get a valid and tracked body & pass to the filter object
                                filter.UpdateFilter(body);

                                if (joint.JointType == JointType.FootLeft)
                                {
                                    //filtered LeftFootJoint
                                    CameraSpacePoint filteredLeftFootJoint = filter.GetFilteredJoint((int)joint.JointType);

                                    Vector3 LeftFootJoint = ConvertCameraSpacePointtoVector3(filteredLeftFootJoint);
                                    // Vector3 LeftFootImagepoint = ConvertCameraSpacePointtoVector3(MapKinectCameraPointToKinectDepthSpacePoint(filteredLeftFootJoint));

                                    manager.OnLeftFootTracked(this, LeftFootJoint, Vector3.zero, userID);

                                }

                                if (joint.JointType == JointType.FootRight)
                                {
                                    //filtered LeftFootJoint
                                    CameraSpacePoint filteredRightFootJoint = filter.GetFilteredJoint((int)joint.JointType);

                                    Vector3 RightFootJoint = ConvertCameraSpacePointtoVector3(filteredRightFootJoint);
                                    // Vector3 RightFootImagepoint = ConvertCameraSpacePointtoVector3(MapKinectCameraPointToKinectDepthSpacePoint(filteredRightFootJoint));

                                    manager.OnRightFootTracked(this, Vector3.zero, RightFootJoint, userID);

                                }
                            }
                        }
                    }

                    else
                    {
                        if (collectionOfUsers.ContainsKey(userID))
                        {
                            collectionOfUsers.Remove(userID);
                            usersAmount = collectionOfUsers.Count;
                            if (showDebug)
                            {
                                Debug.Log("user " + userID + " lost");
                                Debug.Log(" usersamount : " + usersAmount);
                            }

                            manager.OnBodyLost(this, usersAmount, userID);
                        }
                    }
                }
            }

        }
    }

    /// <summary>
    ///  convert  kinect Camera SpacePoint to unity vector3
    /// </summary> 
    private Vector3 ConvertCameraSpacePointtoVector3(CameraSpacePoint cameraSpacePoint)
    {
        Vector3 UnityVector3 = new Vector3();
        UnityVector3.x = cameraSpacePoint.X;
        UnityVector3.y = cameraSpacePoint.Y;
        UnityVector3.z = cameraSpacePoint.Z;

        return UnityVector3;
    }


    private DepthSpacePoint MapKinectCameraPointToKinectDepthSpacePoint(CameraSpacePoint cameraSpacePoint)
    {
        DepthSpacePoint depthSpacePoint = sensor.CoordinateMapper.MapCameraPointToDepthSpace(cameraSpacePoint);

        return depthSpacePoint;
    }

    private Vector3 ConvertKinectDepthSpacePointIntoUnityVector3(DepthSpacePoint depthSpacePoint)
    {
        Vector3 Vect3 = new Vector3();
        Vect3.x = depthSpacePoint.X;
        Vect3.y = depthSpacePoint.Y;
        Vect3.z = 0;

        return Vect3;
    }


    private void OnApplicationQuit()
    {
        if (bodyReader != null)
        {
            bodyReader.Dispose();
            bodyReader = null;
        }

        if (sensor != null)
        {
            if (sensor.IsOpen)
            {
                sensor.Close();
            }
            sensor = null;
            Debug.Log("kinect is closed");
        }

    }


}