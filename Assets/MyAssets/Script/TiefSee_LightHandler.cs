﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiefSee_LightHandler : MonoBehaviour
{

    private bool isApplicationQuit;

    private void OnApplicationQuit()
    {
        isApplicationQuit = true;
    }

    private void OnDestroy()
    {
        if (isApplicationQuit) return;

        sui_demo_LightHandler sui_Demo_LightHandler;
        sui_Demo_LightHandler = FindObjectOfType<sui_demo_LightHandler>().GetComponent<sui_demo_LightHandler>();

        if (sui_Demo_LightHandler != null)
            Destroy(sui_Demo_LightHandler);
    }
}
