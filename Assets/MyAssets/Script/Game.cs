﻿using UnityEngine;
using Utils;

public class Game : Singleton<Game>
{

    private bool mChangeLevel = true;
    [SerializeField] private bool m_DestroyMeganisoptera = false;
    [SerializeField] GameObject[] comp;
    [SerializeField] GameObject[] SuimonoComponents;
    [SerializeField] GameObject[] LandComponents;
    [SerializeField] GameObject _Light;
    [SerializeField] public GameObject[] surfaces;
    private Manager manager;
    private Setting setting;

    private void Awake()
    {
        setting = Setting.Instance;
        manager = Manager.Instance;
        SetUpSamplaData();
    }

    // Use this for initialization
    private void SetUpSamplaData()
    {
        if (surfaces[0] == null)
            if (GameObject.Find("Land_Surface") != null)
                surfaces[0] = GameObject.Find("Land_Surface");

        if (surfaces[1] == null)
            if (GameObject.Find("Suimono_Object") != null)
                surfaces[1] = GameObject.Find("Suimono_Object");

        if (setting.ApplicationTyp == Setting.Application.Land)
        {

            TiefSee_LightHandler _TiefSee_LightHandler;
            _TiefSee_LightHandler = FindObjectOfType<TiefSee_LightHandler>();
            if (_TiefSee_LightHandler != null)
                Destroy(_TiefSee_LightHandler);


            FindObjectOfType<TextFXTiefSee>().enabled = false;
            Destroy(FindObjectOfType<TextFXTiefSee>().gameObject);

            //FindObjectOfType<Interpolator>().GetComponent<Interpolator>().enabled = false;
            //Destroy(FindObjectOfType<Interpolator>().gameObject);

            FindObjectOfType<EusthenopteronManager>().enabled = false;
            Destroy(FindObjectOfType<EusthenopteronManager>().gameObject);

            FindObjectOfType<CorrespondenceAcquisition>().enabled = false;
            FindObjectOfType<PointsRecordReplay>().enabled = false;
            FindObjectOfType<GlobalFlockManager>().enabled = false;
            Destroy(FindObjectOfType<GlobalFlockManager>().gameObject);
            FindObjectOfType<ui_suimonoHandler>().enabled = false;
            FindObjectOfType<ui_suimonoFps>().enabled = false;
            FindObjectOfType<MainCameraHandler>().enabled = true;

            FindObjectOfType<AnimalTracerManager>().enabled = false;
            Destroy(FindObjectOfType<AnimalTracerManager>().gameObject);

            FindObjectOfType<AnimalPositionRelativToStairTracker>().enabled = false;
            Destroy(FindObjectOfType<AnimalPositionRelativToStairTracker>().gameObject);

            FindObjectOfType<EnvironmentTransformation>().enabled = false;
            Destroy(FindObjectOfType<EnvironmentTransformation>());

            foreach (GameObject c in comp)
                if (c != null)
                    Destroy(c);


            foreach (GameObject suimonoComponent in SuimonoComponents)
                if (suimonoComponent != null)
                    Destroy(suimonoComponent);

        }
        else if (setting.ApplicationTyp == Setting.Application.TiefseeUfer)
        {
            Land_LightHandler landArea_LightHandler;
            landArea_LightHandler = FindObjectOfType<Land_LightHandler>();
            if (landArea_LightHandler != null)
                Destroy(landArea_LightHandler);

            _Light.GetComponent<Light>().shadowStrength = 0.5f;

            // FindObjectOfType<ui_suimonoHandler>().GetComponent<ui_suimonoHandler>().enabled = false;
            // if (SuimonoComponents[0] != null) Destroy(SuimonoComponents[0]);

            FindObjectOfType<PederpesManager>().enabled = false;
            Destroy(FindObjectOfType<PederpesManager>().gameObject);

            if (m_DestroyMeganisoptera)
            {
                FindObjectOfType<MeganisopteraManager>().enabled = false;
                Destroy(FindObjectOfType<MeganisopteraManager>().gameObject);
            }


            FindObjectOfType<PlantsAnimator>().enabled = false;
            Destroy(FindObjectOfType<PlantsAnimator>().gameObject);

            FindObjectOfType<Timer>().GetComponent<Timer>().enabled = false;
            Destroy(FindObjectOfType<Timer>().gameObject);

            foreach (GameObject LandComponent in LandComponents)
                if (LandComponent != null) Destroy(LandComponent);
        }

    }

}
