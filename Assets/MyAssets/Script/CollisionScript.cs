﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CollisionScript : MonoBehaviour
{
    //public Collision GetCollisionEnter { get; private set; }
   // public Collision GetCollisionStay { get; private set; }
    //public Collision GetCollisionExit { get; private set; }
    public Collision GetCollision { get; private set; }
    public Collider GetCollider { get; private set; }
    private bool isApplicationQuitting;
    private Manager manager;

    private void Awake()
    {
        manager = Manager.Instance;
    }

    private void OnCollisionEnter(Collision collision)
    {
        //this.GetCollisionEnter = collision;
        this.GetCollision = collision;
        manager.OnUserCollisionEntered(this);
    }

    private void OnCollisionStay(Collision collision)
    {
        //this.GetCollisionStay = collision;
        this.GetCollision = collision;
        manager.OnUserCollisionStayed(this);
    }

    private void OnCollisionExit(Collision collision)
    {
        //this.GetCollisionExit = collision;
        this.GetCollision = collision;
        manager.OnUserCollisionExited(this);
    }

    private void OnTriggerEnter(Collider collider)
    {
      
        this.GetCollider = collider;
        manager.OnUserTriggerEntered(this);
    }

    private void OnApplicationQuit()
    {
        isApplicationQuitting = true;
    }
    private void OnDestroy()
    {
        if (!isApplicationQuitting)
            manager.PlayerPrefabIsDestroyed(this);
    }

}
