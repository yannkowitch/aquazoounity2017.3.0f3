﻿using System;
using System.Collections;
using UnityEngine;

public class Meganisoptera : MonoBehaviour
{
    private MeganisopteraManager meganisopteraManager;
    private Animator animator;
    private Manager manager;
    private bool isApplicationQuitting;
    private bool isdestroyable;

    private void Awake()
    {
        manager = Manager.Instance;
        animator = GetComponent<Animator>();
        meganisopteraManager = FindObjectOfType<MeganisopteraManager>();
    }

    // Use this for initialization
    private IEnumerator Start()
    {
        manager.MeganisopterIsCreated(this);
        isdestroyable = false;

        yield return new WaitForSeconds(3);
        isdestroyable = true;
    }


    // Update is called once per frame
    private void Update()
    {
        animator.SetBool("fly", meganisopteraManager.fly);

        // if (!meganisopteraManager.fly) return;

        if (!meganisopteraManager.isFollowTarget)
            FlyForward();
        else
            FlyToTarget();

    }

    private void FlyForward()
    {
        this.transform.Translate(-Vector3.forward * meganisopteraManager.flySpeed * Time.deltaTime * 2, Space.Self);
    }

    private void FlyToTarget()
    {
        Vector3 direction = (meganisopteraManager.MeganisopterTargets[meganisopteraManager.targetIndex].transform.position - this.transform.position).normalized;
        if (direction != Vector3.zero)
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(-direction), 6 * Time.deltaTime);

        transform.position = Vector3.Lerp(this.transform.position, meganisopteraManager.MeganisopterTargets[meganisopteraManager.targetIndex].transform.position, meganisopteraManager.flySpeed * Time.deltaTime);
    }

    private void OnApplicationQuit()
    {
        isApplicationQuitting = true;
    }

    private void OnDestroy()
    {
        if (!isApplicationQuitting)
            manager.MeganisopterIsDestroyed(this);
    }

    private void OnTriggerEnter(Collider otherCollider)
    {

        if (otherCollider.gameObject.tag != "animalDestroyerCollider") return;

        if (!isdestroyable) return;
        if (this.gameObject == null) return;
            Destroy(this.gameObject);


    }
}
