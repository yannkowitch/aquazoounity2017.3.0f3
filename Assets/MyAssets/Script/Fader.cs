﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fader : MonoBehaviour
{
    FadeDirection fadeDirection = FadeDirection.In;

    public enum FadeDirection
    {
        In, //Alpha = 1
        Out // Alpha = 0
    }
}
