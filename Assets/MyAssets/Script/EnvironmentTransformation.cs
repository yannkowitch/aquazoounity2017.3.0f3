﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum Direction
{
    Up,
    Down
}

public class EnvironmentTransformation : MonoBehaviour
{
    [SerializeField] private float underWaterGroundlerpSpeed = 0.3f;
    [SerializeField] private float waterStairlerpSpeed = 0.3f;
    [SerializeField] private bool visibility;
    private float underwatergroundMaxHeight;//= -11.589f
    [SerializeField] private float undergroundMinHeight;//= -20f
    private float wasserStairMinHeight = 0;
    private float wasserStairMaxHeight = 0;
    [SerializeField] private GameObject underWaterGround;
    [SerializeField] private GameObject waterStair;
    private Coroutine m_MoveUnderWaterGroundCoroutine, m_MoveWaterStairGOCoroutine;
    private Environment.Level environmentPresetState;
    private Manager manager;

    private void Awake()
    {
        manager = Manager.Instance;
    }
    
    private void Start()
    {
        underwatergroundMaxHeight = underWaterGround.transform.position.y;
        wasserStairMaxHeight = waterStair.transform.position.x;
        wasserStairMinHeight = wasserStairMaxHeight - 4;
    }

    private void OnEnable()
    {
        manager.EnvironmentLevelHalfChanged += OnEnvironmentLevelHalfChanged;
        manager.EnvironmentLevelInitialized += OnEnvironmentLevelInitialized;
    }

    private void OnDisable()
    {
        manager.EnvironmentLevelHalfChanged -= OnEnvironmentLevelHalfChanged;
        manager.EnvironmentLevelInitialized -= OnEnvironmentLevelInitialized;
    }

    private void OnEnvironmentLevelHalfChanged(object sender, Manager.EnvironmentLevelEventArgs wp)
    {
        this.environmentPresetState = wp.environmentLevel;
        TransformEnvironment(this.environmentPresetState);
    }

    #region  EVENTHADLERS

    private void OnEnvironmentLevelInitialized(object sender, Manager.EnvironmentLevelEventArgs wp)
    {
        this.environmentPresetState = wp.environmentLevel;

        if (underWaterGround == null) return;

        float undergroundHeight = (environmentPresetState == Environment.Level.Tiefsee) ? undergroundMinHeight : underwatergroundMaxHeight;
        Vector3 undergroundStartPosition = new Vector3(underWaterGround.transform.position.x, undergroundHeight, underWaterGround.transform.position.z);
        underWaterGround.transform.position = undergroundStartPosition;

        InitializeWaterWaterStair(environmentPresetState);

        if (!visibility) return;

        underWaterGround.SetActive(environmentPresetState == Environment.Level.Tiefsee ? false : true);

    }

    #endregion  EVENTHADLERS

    private IEnumerator MoveUnderWaterGroundIE(Direction direction, Environment.Level currentSuimonoPreset)
    {
        float height = (direction == Direction.Up) ? underwatergroundMaxHeight : undergroundMinHeight;
        Vector3 UnderWaterGroundEndPosition = new Vector3(underWaterGround.transform.position.x, height, underWaterGround.transform.position.z);

        if (visibility)
        {
            if (currentSuimonoPreset == Environment.Level.Ufer)
            {
                underWaterGround.SetActive(true);
            }
        }


        while (Vector3.Distance(underWaterGround.transform.position, UnderWaterGroundEndPosition) >= 0.25f)
        {
            if (underWaterGround != null)
            {
                underWaterGround.transform.position = Vector3.Lerp(underWaterGround.transform.position, UnderWaterGroundEndPosition, (underWaterGroundlerpSpeed) * Time.deltaTime);
            }

            yield return null; // make the loop wait until the next frame before continuing
        }
        
        manager.OnEnvironmentTransformationEnded(this);

        if (visibility)
        {
            if (currentSuimonoPreset == Environment.Level.Tiefsee)
            {
                underWaterGround.SetActive(false);
            }
        }
    }

    private void TransformUnderWaterGround(Environment.Level environmentPresetState)
    {
        Direction UnderGroundMovedirection = (environmentPresetState == Environment.Level.Tiefsee) ? Direction.Down : Direction.Up;

        if (m_MoveUnderWaterGroundCoroutine != null)
        {
            StopCoroutine(m_MoveUnderWaterGroundCoroutine);
            m_MoveUnderWaterGroundCoroutine = null;
        }
        m_MoveUnderWaterGroundCoroutine = StartCoroutine(MoveUnderWaterGroundIE(UnderGroundMovedirection, environmentPresetState));
    }

    private IEnumerator MoveWaterStairGOIE(Direction direction, Environment.Level environmentPresetState)
    {

        float xOffset = (direction == Direction.Up) ? wasserStairMaxHeight : wasserStairMinHeight;
        Vector3 endWaterTreppePosition = new Vector3(xOffset, waterStair.transform.position.y, waterStair.transform.position.z);

        while (Vector3.Distance(waterStair.transform.position, endWaterTreppePosition) >= 0.25f)
        {
            if (waterStair != null)
                waterStair.transform.position = Vector3.Lerp(waterStair.transform.position, endWaterTreppePosition, (waterStairlerpSpeed) * Time.deltaTime);

            yield return null; // make the loop wait until the next frame before continuing
        }
    }

    private void TransformWaterWaterStair(Environment.Level environmentPresetState)
    {
        Direction waserTreppeMovedirection = (environmentPresetState == Environment.Level.Tiefsee) ? Direction.Up : Direction.Down;

        if (m_MoveWaterStairGOCoroutine != null)
        {
            StopCoroutine(m_MoveWaterStairGOCoroutine);
            m_MoveWaterStairGOCoroutine = null;
        }
        m_MoveWaterStairGOCoroutine = StartCoroutine(MoveWaterStairGOIE(waserTreppeMovedirection, environmentPresetState));
    }

    private void InitializeWaterWaterStair(Environment.Level environmentPresetState)
    {
        float wasserTreppedHeight = (environmentPresetState == Environment.Level.Tiefsee) ? wasserStairMaxHeight : wasserStairMinHeight;
        Vector3 waterTreppeStartPosition = new Vector3(wasserTreppedHeight, waterStair.transform.position.y, waterStair.transform.position.z);
        waterStair.transform.position = waterTreppeStartPosition;
    }

    private void TransformEnvironment(Environment.Level environmentPresetState)
    {
        TransformUnderWaterGround(environmentPresetState);
        //  send osc message hier
        TransformWaterWaterStair(environmentPresetState);
    }

}
