﻿using System;
using UnityEngine;
using Utils;
using Windows.Kinect;

public class Manager : Singleton<Manager>
{
    bool KinectShowDebug = false;
    bool prefabMouseShowDebug = false;
    bool collisionShowDebug = false;
    bool showDebug = false;


    #region Event Type 
    /*
        as menbers of the event sender (the Class/object that raised the event. In this case Manager.cs)
        an event is a message senr by an object to signal the occurrence of an action. this action can be caused by user interaction, 
        such as a button click or it can result from some other programm logic, such as a changing proprety´s value
        to respond to an event, we are going to define an Eventhandler in the event receiver. 
        This method musst match the signature of the delegate for the event we are handling
        
    */
    public event EventHandler ProjectorCameraCalibrationTrigged;
    public event EventHandler SavedRecordedPointsLoaded;
    public event EventHandler<EnvironmentLevelEventArgs> EnvironmentLevelInitialized, EnvironmentLevelCompletelyChanged, EnvironmentLevelHalfChanged;
    public event EventHandler<TriggerButtonEventArgs> TriggerButtonCreated, TriggerButtonEnable, OnTriggerButtonDestroyed;
    public event EventHandler<PederpesEventArgs> OnPederpesCreated, OnPederpesinIdleState, OnPederpesDestroyed;
    public event EventHandler<TiktalikEventArgs> TiktalikCreated, TiktalikUnderTheStairs, TiktalikAwayFromTheStairs, TiktalikinIdleState, TiktalikDestroyed;
    public event EventHandler<MeganisopterEventArgs> OnMeganisopterCreated, OnMeganisopterDestroyed;
    public event EventHandler<EusthenopteronEventArgs> OnEusthenopteronCreated, EusthenopteronUnderTheStairs, EusthenopteronAwayFromTheStairs, OnEusthenopteroninIdleState, OnEusthenopteronDestroyed;
    public event EventHandler<CollisionEventArgs> UserCollisionEnter, UserCollisionStay, UserCollisionExit, OnPlayerPrefabDestroyed;
    public event EventHandler<TriggerEventArgs> UserTriggerEnter;
    public event EventHandler LoadingBarFilled, LoadBarIncrementing, loadBarDecrementing;
    public event EventHandler<PlayerPrefabMouseEventArgs> PlayerPrefabMouseCreated, PlayerPrefabMouseDestroyed;
    public event EventHandler<UnityKinectDataCorrespondenceAcquitionEventArgs> PointCaptured, PointsCaptured;
    public event EventHandler EnvironmentTransformationEnded;

    public event EventHandler<KinectSensorEventArgs> BodyFrameArrived, leftFootTracked, RightFootTracked, BodyLost;
    public event EventHandler<PlayerPrefabFootEventArgs> PlayerPrefabLeftFootInstantiated, PlayerPrefabRightFootInstantiated, PlayerPrefabLeftDestroyed, PlayerPrefabRightDestroyed;
    public event EventHandler<TiktalikEnvironmentStateEventArgs> TiktalikEnvironmentStateChanged;
    public event EventHandler<TriggerEventArgs> AnimalTrackerTriggerColliderEnter, AnimalTrackerTriggerColliderExit;
    public event EventHandler<ProCamDataArgs> KinectUnityCalibrationDataLoaded;
    public event EventHandler CalibrationMatrixCalculated;
    public event EventHandler OnSceneStarted;
    public event EventHandler SettingsPanelOpened, SettingsPanelClosed;
    public event EventHandler<TextTypingEventArgs> TextTypingBegan, TextTyping, TextTypingEnded;

    public event EventHandler<LoadSceneEventArgs> LoadSceneTrigged;
    public event EventHandler AnySettingsValueChanged;


    public event EventHandler TimerStarted, TimerEnded;

    public event EventHandler PlantsAnimationStarted;

    public event EventHandler TiktaalikCrawled, PederpesWalked;

    public event EventHandler<DateTimeManagerEventArgs> MaxTimeReached;
    #endregion


    #region RAISED EVENTS
    public void OnMaxTimeReached(string maxTime)
    {
        if (MaxTimeReached != null) {
            Debug.Log("MaxTime Reached");
            MaxTimeReached(this, new DateTimeManagerEventArgs(maxTime));
        }
    }
    public void OnPederpesWalked()
    {
        if (PederpesWalked != null)
            PederpesWalked(this, null);
    }
    public void OnTiktaalikCrawled()
    {
        if (TiktaalikCrawled != null) {
            TiktaalikCrawled(this, null);
        }
    }

    public void OnPlantsAnimationStarted()
    {
        if (PlantsAnimationStarted != null) {
            PlantsAnimationStarted(this, null);
        }
    }
    public void OnTimerStarted()
    {
        if (TimerStarted != null)
            TimerStarted(this, null);
    }

    public void OnTimerEnded()
    {
        if (TimerEnded != null)
            TimerEnded(this, null);
    }
    /*
    public void TriggerButtonisExited()
    {
        if (OnTriggerButtonExited!= null)
        {
            OnTriggerButtonExited(this, null);
            Debug.Log("button is exit");
        }
    }
    public void TriggerButtonisEntered()
    {
        if (OnTriggerButtonEntered != null)
        {
            OnTriggerButtonEntered(this, null);
            Debug.Log("button is enetred");
        }
          
    }
    */
    public void OnAnySettingsValueIsChanged()
    {
        if (AnySettingsValueChanged != null)
            AnySettingsValueChanged(this, null);
    }

    public void OnLoadSceneTrigged(int activeSceneBuildIndex)
    {
        if (LoadSceneTrigged != null)
            LoadSceneTrigged(this, new LoadSceneEventArgs(activeSceneBuildIndex));
    }

    public void OnTextTypingEnded(string Text)
    {
        if (TextTypingEnded != null) {
            TextTypingEnded(this, new TextTypingEventArgs(Text));

        }
    }
    public void OnTextTyping(string Text)
    {
        if (TextTyping != null) {
            TextTyping(this, new TextTypingEventArgs(Text));

        }
    }
    public void OnTextIsTypingHasBegan(string Text)
    {
        if (TextTypingBegan != null) {
            TextTypingBegan(this, new TextTypingEventArgs(Text));

        }
    }

    /// <summary>
    /// This method raises "OnApplicationStarted" event, when the application get started
    /// </summary>
    public void SceneIsStarted()
    {
        if (OnSceneStarted != null) {
            OnSceneStarted(this, null);
            if (showDebug)
                Debug.Log("Application started");
        }

    }

    /// <summary>
    ///  This method raises "OnCalibrationMatrixCalculated" event, when the Matrix, that maps Kinect to Unity 3dpoint, is Calculated
    /// </summary>
    public void OnCalibrationMatrixCalculated()
    {
        if (CalibrationMatrixCalculated != null) {
            if (showDebug)
                Debug.Log("CalibrationMatrix Calculated");
            CalibrationMatrixCalculated(this, null);
        }

    }

    /// <summary>
    /// This method raises "OnCalibrationMatrixCalculated" event, when the Matrix, that maps Kinect to Unity 3dpoint, is loaded
    /// </summary>
    /// <param name="proCamData"></param>
    public void OnKinectUnityCalibrationDataLoaded(ProCamData proCamData)
    {
        if (KinectUnityCalibrationDataLoaded != null) {
            if (showDebug)
                Debug.Log("KinectUnityCalibrationData Is Loaded ");

            KinectUnityCalibrationDataLoaded(this, new ProCamDataArgs(proCamData));
        }
    }

    public void OnAnimalTrackerTriggerColliderEnter(Collider otherCollider)
    {
        if (AnimalTrackerTriggerColliderEnter != null) {
            if (showDebug)
                Debug.Log("AnimalTrackerTriggerCollider Is Entered ");
            AnimalTrackerTriggerColliderEnter(this, new TriggerEventArgs(otherCollider));
        }
    }
    public void OnAnimalTrackerTriggerColliderExit(Collider otherCollider)
    {
        if (AnimalTrackerTriggerColliderExit != null) {
            if (showDebug)
                Debug.Log("AnimalTrackerTriggerCollider Is exit ");
            AnimalTrackerTriggerColliderExit(this, new TriggerEventArgs(otherCollider));
        }
    }

    public void OnEusthenopteronUnderTheStairs(GameObject eusthenopteronGameobject)
    {
        if (EusthenopteronUnderTheStairs != null) {
            if (showDebug)
                Debug.Log("Eusthenopteron is Under The Stairs ");
            EusthenopteronUnderTheStairs(this, new EusthenopteronEventArgs(eusthenopteronGameobject));
        }
    }

    public void OnEusthenopteronAwayFromTheStairs(GameObject eusthenopteronGameobject)
    {
        if (EusthenopteronAwayFromTheStairs != null) {

            if (showDebug)
                Debug.Log("Eusthenopteron is Away From The Stairs ");
            EusthenopteronAwayFromTheStairs(this, new EusthenopteronEventArgs(eusthenopteronGameobject));
        }
    }

    public void OnTiktalikUnderTheStairs(GameObject tiktalikGameobject)
    {
        if (TiktalikUnderTheStairs != null) {
            if (showDebug)
                Debug.Log("Tiktalik is Under The Stairs ");
            TiktalikUnderTheStairs(this, new TiktalikEventArgs(tiktalikGameobject));
        }
    }

    public void OnTiktalikAwayFromTheStairs(GameObject tiktalikGameobject)
    {
        if (TiktalikAwayFromTheStairs != null) {

            if (showDebug)
                Debug.Log("Tiktalik is Away From The Stairs ");
            TiktalikAwayFromTheStairs(this, new TiktalikEventArgs(tiktalikGameobject));
        }
    }

    public void OnLoadBarIncrementing()
    {
        if (LoadBarIncrementing != null) {
            if (showDebug)
                Debug.Log("LoadBar is incrementing ");
            LoadBarIncrementing(this, null);
        }
    }

    public void OnLoadBarDecrementing()
    {
        if (loadBarDecrementing != null) {
            if (showDebug)
                Debug.Log("LoadBar is decrementing ");
            loadBarDecrementing(this, null);
        }
    }

    /// <summary>
    /// this method is called, when the savedrecordedpoints to calibrate unity with the projector, are loaded
    /// </summary>
    /// <param name="pointsRecordReplay"></param>
    /// 
    public void OnSavedRecordedPointsLoaded()
    {
        if (SavedRecordedPointsLoaded != null) {
            if (showDebug)
                Debug.Log("SavedRecordedPoints are Loaded ");
            SavedRecordedPointsLoaded(this, null);
        }
    }

    public void OnProjectorCameraCalibrationTrigged()
    {
        if (ProjectorCameraCalibrationTrigged != null) {
            if (showDebug)
                Debug.Log("Calibration trigged");
            ProjectorCameraCalibrationTrigged(this, null);
        }
    }

    public void OnTiktalikEnvironmentStateChanged(Tiktalik.EnvironmentState environmentState)
    {
        if (TiktalikEnvironmentStateChanged != null) {
            if (showDebug)
                Debug.Log("TiktalikEnvironmentState is Changed ");
            TiktalikEnvironmentStateChanged(this, new TiktalikEnvironmentStateEventArgs(environmentState));
        }
    }

    public void OnEnvironmentLevelInitialized(Environment.Level environmentState)
    {
        if (EnvironmentLevelInitialized != null) {
            EnvironmentLevelInitialized(this, new EnvironmentLevelEventArgs(environmentState));
            if (showDebug)
                Debug.Log("waterpreset is initialized! ");
        }
    }

    public void OnEnvironmentLevelCompletelyChanged(Environment.Level environmentState)
    {
        if (EnvironmentLevelCompletelyChanged != null) {
            EnvironmentLevelCompletelyChanged(this, new EnvironmentLevelEventArgs(environmentState));
            if (showDebug)
                Debug.Log("waterpreset is changed. new EnvironmentState " + new EnvironmentLevelEventArgs(environmentState).environmentLevel);
        }
    }

    public void OnEnvironmentLevelHalfChanged(Environment.Level environmentState)
    {
        if (EnvironmentLevelHalfChanged != null) {
            if (showDebug)
                Debug.Log("halftime is reached! ");
            EnvironmentLevelHalfChanged(this, new EnvironmentLevelEventArgs(environmentState));
        }
    }

    public void OnTriggerButtonCreated(TriggerButton triggerButton)
    {
        if (TriggerButtonCreated != null) {
            TriggerButtonCreated(this, new TriggerButtonEventArgs(triggerButton.Index, triggerButton.gameObject));

            if (!showDebug) return;
            Debug.Log("TriggerButton is created! ");
        }
    }

    public void OnTriggerButtonEnabled(TriggerButton triggerButton)
    {
        if (TriggerButtonEnable != null) {
            TriggerButtonEnable(this, new TriggerButtonEventArgs(triggerButton.Index, triggerButton.gameObject));

            if (!showDebug) return;
            Debug.Log("TriggerButton is enabled! ");
        }
    }

    public void TriggerButtonIsDestroyed(TriggerButton triggerButton)
    {
        if (OnTriggerButtonDestroyed != null) {
            OnTriggerButtonDestroyed(this, new TriggerButtonEventArgs(triggerButton.Index, triggerButton.gameObject));

            if (!showDebug) return;
            Debug.Log("TriggerButton is destroyed! ");
        }
    }

    public void EusthenopteronIsCreated(Eusthenopteron eusthenopteron)
    {
        if (OnEusthenopteronCreated != null) {
            if (showDebug)
                Debug.Log("Eusthenopteron is Created! ");
            OnEusthenopteronCreated(this, new EusthenopteronEventArgs(eusthenopteron.gameObject));
        }
    }

    public void EusthenopteronIsDestroyed(Eusthenopteron eusthenopteron)
    {
        if (OnEusthenopteronDestroyed != null) {
            if (showDebug)
                Debug.Log("Eusthenopteron is destroyed! ");
            OnEusthenopteronDestroyed(this, new EusthenopteronEventArgs(eusthenopteron.gameObject));
        }
    }

    public void EusthenopteronInIdleState(EusthenopteronManager eusthenopteronManager)
    {
        if (OnEusthenopteroninIdleState != null) {
            if (showDebug)
                Debug.Log("Eusthenopteron is in Idlestate! ");
            OnEusthenopteroninIdleState(this, null);
        }
    }

    public void MeganisopterIsCreated(Meganisoptera mg)
    {
        if (OnMeganisopterCreated != null) {
            OnMeganisopterCreated(this, new MeganisopterEventArgs(mg.gameObject));

            if (!showDebug) return;
            Debug.Log("Meganisopter is Created! ");
        }
    }

    public void MeganisopterIsDestroyed(Meganisoptera mg)
    {
        if (OnMeganisopterDestroyed != null) {
            OnMeganisopterDestroyed(this, new MeganisopterEventArgs(mg.gameObject));

            if (!showDebug) return;
            Debug.Log("Meganisopter is Destroyed! ");
        }
    }

    public void PederpesIsCreated(Pederpes pd)
    {
        if (OnPederpesCreated != null) {
            OnPederpesCreated(this, new PederpesEventArgs(pd.gameObject));

            if (!showDebug) return;
            Debug.Log("Pederpes is Created! ");
        }
    }

    public void PederpesInIdleState(PederpesManager pdm)
    {
        if (OnPederpesinIdleState != null) {
            OnPederpesinIdleState(this, null);

            if (!showDebug) return;
            Debug.Log("Pederpes is in idlestate! ");
        }
    }

    public void PederpesIsDestroyed(Pederpes pd)
    {
        if (OnPederpesDestroyed != null) {
            OnPederpesDestroyed(this, new PederpesEventArgs(pd.gameObject));

            if (!showDebug) return;
            Debug.Log("Pederpes is destroyed! ");
        }
    }

    public void OnTiktalikCreated(Tiktalik tiktalik)
    {
        if (TiktalikCreated != null) {
            if (showDebug)
                Debug.Log("Tiktalik is Created! ");
            TiktalikCreated(this, new TiktalikEventArgs(tiktalik.gameObject));
        }
    }

    public void OnTiktaliksInIdleState(TiktalikManager tkm)
    {
        if (TiktalikinIdleState != null) {
            if (showDebug)
                Debug.Log("Tiktalik is in idlestate! ");
            TiktalikinIdleState(this, null);
        }
    }

    public void OnTiktalikDestroyed(Tiktalik tk)
    {
        if (TiktalikDestroyed != null) {
            if (showDebug)
                Debug.Log("Tiktalik is destroyed! ");
            TiktalikDestroyed(this, new TiktalikEventArgs(tk.gameObject));
        }
    }

    public void PlayerPrefabIsDestroyed(CollisionScript col)
    {
        if (OnPlayerPrefabDestroyed != null) {
            if (prefabMouseShowDebug)
                Debug.Log("PlayerPrefab is destroyed! ");

            OnPlayerPrefabDestroyed(this, new CollisionEventArgs());
        }
    }

    public void OnUserCollisionEntered(CollisionScript col)
    {
        if (UserCollisionEnter != null) {
            if (collisionShowDebug)
                Debug.Log("UserCollision is Entered! ");

            UserCollisionEnter(this, new CollisionEventArgs(col.GetCollision, null, null));
        }
        //FireOnUserCollisionEnter(new CollisionEvent(cs.GetCollisionEnter, null, null));
    }

    public void OnUserCollisionStayed(CollisionScript col)
    {
        if (UserCollisionStay != null) {
            if (collisionShowDebug)
                Debug.Log("UserCollision is Stayed ! ");

            UserCollisionStay(this, new CollisionEventArgs(null, col.GetCollision, null));
        }
    }

    public void OnUserCollisionExited(CollisionScript col)
    {
        if (UserCollisionExit != null) {
            if (collisionShowDebug)
                Debug.Log("userCollision is Exited ! ");

            UserCollisionExit(this, new CollisionEventArgs(null, null, col.GetCollision));
        }
    }

    public void OnUserTriggerEntered(CollisionScript col)
    {
        if (UserTriggerEnter != null) {
            UserTriggerEnter(this, new TriggerEventArgs(col.GetCollider, null, null));
            if (collisionShowDebug)
                Debug.Log("usertrigger entered");
        }
    }

    public void OnLoadingBarFilled(RadialProgress rp)
    {
        if (LoadingBarFilled != null) {
            if (showDebug)
                Debug.Log("LoadingBar is Filled! ");
            LoadingBarFilled(this, null);
        }
    }

    public void OnPlayerPrefabMouseCreated(Mouse im)
    {
        if (PlayerPrefabMouseCreated != null) {
            PlayerPrefabMouseCreated(this, new PlayerPrefabMouseEventArgs(im.PlayerPrefabClone));

            if (!prefabMouseShowDebug) return;
            Debug.Log("PlayerPrefabMouse is Created");
        }
    }

    public void OnPlayerPrefabMouseDestroyed(Mouse im)
    {
        if (PlayerPrefabMouseDestroyed != null) {
            PlayerPrefabMouseDestroyed(this, new PlayerPrefabMouseEventArgs(im.PlayerPrefabClone));

            if (!prefabMouseShowDebug) return;
            Debug.Log("PlayerPrefabMouse is Destroyed");

        }
    }

    public void OnPointsCaptured(UnityKinectDataCorrespondenceAcquition ukd)
    {
        if (PointsCaptured != null) {
            Debug.Log("Points is Captured");
            PointsCaptured(this, new UnityKinectDataCorrespondenceAcquitionEventArgs(ukd.calibrationData));
        }
    }

    public void OnPointCaptured(UnityKinectDataCorrespondenceAcquition ukd)
    {
        if (PointCaptured != null) {
            Debug.Log("Point is Captured");
            PointCaptured(this, new UnityKinectDataCorrespondenceAcquitionEventArgs());
        }
    }

    public void PartikelIsDestroyed(ProgressBarManager pb)
    {

    }

    public void OnEnvironmentTransformationEnded(EnvironmentTransformation et)
    {
        if (EnvironmentTransformationEnded != null) {
            if (showDebug)
                Debug.Log("EnvironmentTransformation is Ended");
            EnvironmentTransformationEnded(this, null);
        }
    }

    public void OnBodyFrameArrived(KinectSensor kinectSensor, BodyFrame frame)
    {
        if (BodyFrameArrived != null) {
            BodyFrameArrived(this, new KinectSensorEventArgs(frame));

            if (!KinectShowDebug) return;
            Debug.Log("BodyFrame is arrived");
        }
    }

    public void OnLeftFootTracked(KinectSensor kinectSensor, Vector3 leftFootPositionInUnity, Vector3 rightFootPositionInUnity, int userIndex)
    {
        if (leftFootTracked != null) {
            if (KinectShowDebug)
                Debug.Log("leftFoot is Tracked");
            leftFootTracked(this, new KinectSensorEventArgs(leftFootPositionInUnity, rightFootPositionInUnity, userIndex));
        }
    }

    public void OnRightFootTracked(KinectSensor kinectSensor, Vector3 footPositionAsUnityVector3, Vector3 rightFootPositionInUnity, int userIndex)
    {
        if (RightFootTracked != null) {
            RightFootTracked(this, new KinectSensorEventArgs(footPositionAsUnityVector3, rightFootPositionInUnity, userIndex));

            if (!KinectShowDebug) return;
            Debug.Log("rightFoot is Tracked");

        }
    }


    public void OnBodyLost(KinectSensor kinectSensor, int usersAmount, int userID)
    {
        if (BodyLost != null) {
            Debug.Log("userBody Lost");
            BodyLost(this, new KinectSensorEventArgs(usersAmount, userID));
        }
    }

    public void OnPlayerPrefabLeftFootInstantiated(RipplesFx ripplesFx, int userId, GameObject[] plPref_LftFoots, GameObject[] plPref_RghtFoots)
    {
        if (PlayerPrefabLeftFootInstantiated != null) {
            if (showDebug)
                Debug.Log("PlayerPrefabLeftFoot is instantiated");
            PlayerPrefabLeftFootInstantiated(this, new PlayerPrefabFootEventArgs(userId, plPref_LftFoots, plPref_RghtFoots));
        }
    }

    public void OnPlayerPrefabRightFootInstantiated(RipplesFx ripplesFx, int userID, GameObject[] playerPrefabLeftFoots, GameObject[] playerPrefabRightFoots)
    {
        if (PlayerPrefabRightFootInstantiated != null) {
            PlayerPrefabRightFootInstantiated(this, new PlayerPrefabFootEventArgs(userID, playerPrefabLeftFoots, playerPrefabRightFoots));

            if (!showDebug) return;
            Debug.Log("PlayerPrefabRightFoot is instantiated");
        }
    }

    public void OnPlayerPrefabLeftFootDestroyed(RipplesFx ripplesFx, int userID, GameObject[] playerPrefabLeftFoots, GameObject[] playerPrefabRightFoots)
    {
        if (PlayerPrefabLeftDestroyed != null) {
            PlayerPrefabLeftDestroyed(this, new PlayerPrefabFootEventArgs(userID, playerPrefabLeftFoots, playerPrefabRightFoots));

            if (!showDebug) return;
            Debug.Log("PlayerPrefabLeftFoot is destroyed");
        }
    }

    public void OnPlayerPrefabRightFootDestroyed(RipplesFx ripplesFx, int userID, GameObject[] playerPrefabLeftFoots, GameObject[] playerPrefabRightFoots)
    {
        if (PlayerPrefabRightDestroyed != null) {
            PlayerPrefabRightDestroyed(this, new PlayerPrefabFootEventArgs(userID, playerPrefabLeftFoots, playerPrefabRightFoots));

            if (!showDebug) return;
            Debug.Log("PlayerPrefabLeftFoot is destroyed");
        }
    }

    public void OnSettingPanelOpened()
    {
        if (SettingsPanelOpened != null) {
            SettingsPanelOpened(this, null);
            if (showDebug) Debug.Log("SettingsPanel is Opened");
        }
    }

    public void OnSettingPanelClosed()
    {
        if (SettingsPanelClosed != null) {
            SettingsPanelClosed(this, null);
            if (showDebug) Debug.Log("SettingsPanel is Opened");
        }
    }
    #endregion

    #region event data classes
    //a event data class contains properties that specific to the event being raised

    public class EnvironmentLevelEventArgs : EventArgs
    {
        public Environment.Level environmentLevel;

        public EnvironmentLevelEventArgs(Environment.Level environmentLevel)
        {
            this.environmentLevel = environmentLevel;
        }
    }

    public class TriggerButtonEventArgs : EventArgs
    {
        public int TriggerButton_Index { get; private set; }
        public GameObject TriggerButton { get; private set; }

        public TriggerButtonEventArgs(int TriggerButton_Index, GameObject TriggerButton)
        {
            this.TriggerButton_Index = TriggerButton_Index;
            this.TriggerButton = TriggerButton;
        }

    }

    public class EusthenopteronEventArgs : EventArgs
    {

        public GameObject Eusthenopteron_Gameobject { get; private set; }

        public EusthenopteronEventArgs()
        {

        }
        public EusthenopteronEventArgs(GameObject Eusthenopteron_Gameobject)
        {
            this.Eusthenopteron_Gameobject = Eusthenopteron_Gameobject;
        }
    }

    public class PederpesEventArgs : EventArgs
    {
        public GameObject Pederpes_Gameobject { get; private set; }

        public PederpesEventArgs()
        {

        }
        public PederpesEventArgs(GameObject gameobject)
        {
            this.Pederpes_Gameobject = gameobject;
        }
    }

    public class TiktalikEventArgs : EventArgs
    {
        public GameObject Tiktalik_Gameobject { get; private set; }

        public TiktalikEventArgs()
        {

        }
        public TiktalikEventArgs(GameObject Tiktalik_Gameobject)
        {
            this.Tiktalik_Gameobject = Tiktalik_Gameobject;
        }
    }

    public class MeganisopterEventArgs : EventArgs
    {
        public GameObject Meganisopter_Gameobject { get; private set; }

        public MeganisopterEventArgs()
        {

        }

        public MeganisopterEventArgs(GameObject gameobject)
        {
            this.Meganisopter_Gameobject = gameobject;
        }

    }

    public class TriggerEventArgs : EventArgs
    {
        public Collider triggerEnter { get; private set; }
        public Collider triggerStay { get; private set; }
        public Collider triggerExit { get; private set; }
        public Collider otherCollider { get; private set; }

        public TriggerEventArgs(Collider triggerEnter, Collider triggerStay, Collider triggerExit)
        {
            this.triggerEnter = triggerEnter;
            this.triggerStay = triggerStay;
            this.triggerExit = triggerExit;
        }

        public TriggerEventArgs(Collider otherCollider)
        {
            this.otherCollider = otherCollider;
        }
    }

    public class CollisionEventArgs : EventArgs
    {
        public Collision collisionEnter { get; private set; }
        public Collision collisionStay { get; private set; }
        public Collision collisionExit { get; private set; }

        public CollisionEventArgs()
        {

        }

        public CollisionEventArgs(Collision collisionEnter, Collision collisionStay, Collision collisionExit)
        {
            this.collisionEnter = collisionEnter;
            this.collisionStay = collisionStay;
            this.collisionExit = collisionExit;
        }

    }

    public class PlayerPrefabMouseEventArgs : EventArgs
    {
        public GameObject PlayerPrefabClone { get; private set; }

        public PlayerPrefabMouseEventArgs(GameObject PlayerPrefabClone)
        {
            this.PlayerPrefabClone = PlayerPrefabClone;
        }
    }

    public class UnityKinectDataCorrespondenceAcquitionEventArgs : EventArgs
    {
        public ProCamData calibrationData { get; private set; }

        public UnityKinectDataCorrespondenceAcquitionEventArgs()
        {

        }

        public UnityKinectDataCorrespondenceAcquitionEventArgs(ProCamData calibrationData)
        {
            this.calibrationData = calibrationData;
        }
    }

    public class KinectSensorEventArgs : EventArgs
    {
        public BodyFrame frame { get; set; }
        public Vector3 leftFootPositionInUnity { get; private set; }
        public Vector3 rightFootPositionInUnity { get; private set; }
        public Vector3 footPositionInUnity { get; private set; }
        public int userID { get; private set; }
        public int usersAmount { get; private set; }

        public KinectSensorEventArgs(BodyFrame frame)
        {
            this.frame = frame;
        }

        public KinectSensorEventArgs(int usersAmount)
        {
            this.usersAmount = usersAmount;
        }

        public KinectSensorEventArgs(int usersAmount, int userID)
        {
            this.usersAmount = usersAmount;
            this.userID = userID;
        }

        public KinectSensorEventArgs(Vector3 leftFootPositionInUnity, Vector3 rightFootPositionInUnity, int userID)
        {
            this.leftFootPositionInUnity = leftFootPositionInUnity;
            this.rightFootPositionInUnity = rightFootPositionInUnity;
            this.userID = userID;
        }

    }

    public class PlayerPrefabFootEventArgs : EventArgs
    {
        public int userIndex { get; private set; }

        public GameObject[] playerPrefabLeftFoots { get; private set; }
        public GameObject[] playerPrefabRightFoots { get; private set; }

        public PlayerPrefabFootEventArgs(int userID)
        {
            this.userIndex = userID;
        }

        public PlayerPrefabFootEventArgs(int userID, GameObject[] playerPrefabLeftFoots, GameObject[] playerPrefabRightFoots)
        {
            this.userIndex = userID;
            this.playerPrefabLeftFoots = playerPrefabLeftFoots;
            this.playerPrefabRightFoots = playerPrefabRightFoots;
        }
    }

    public class TiktalikEnvironmentStateEventArgs : EventArgs
    {
        public Tiktalik.EnvironmentState environmentState { get; private set; }

        public TiktalikEnvironmentStateEventArgs(Tiktalik.EnvironmentState environmentState)
        {
            this.environmentState = environmentState;
        }

    }

    #endregion
}

public class DateTimeManagerEventArgs : EventArgs
{
    public string MaxTime { get; set; }
    public DateTimeManagerEventArgs(string maxTime)
    {
        this.MaxTime = maxTime;

    }
}

public class LoadSceneEventArgs : EventArgs
{
    public int activeSceneBuildIndex { set; get; }
    public LoadSceneEventArgs(int activeSceneBuildIndex)
    {
        this.activeSceneBuildIndex = activeSceneBuildIndex;
    }
}

public class TextTypingEventArgs : EventArgs
{
    public string Text { set; get; }

    public TextTypingEventArgs(string Text)
    {
        this.Text = Text;
    }
}

public class ProCamDataArgs : EventArgs
{
    public ProCamData ProCamData { get; private set; }

    public ProCamDataArgs(ProCamData ProCamData)
    {
        this.ProCamData = ProCamData;
    }
}