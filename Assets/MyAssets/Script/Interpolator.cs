﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Interpolator : MonoBehaviour
{
    [SerializeField] protected GameObject gameobject;
    [SerializeField] protected float fadeInSpeed = 15;
    [SerializeField] protected float fadeOutSpeed = 1.5f;
    private Fader.FadeDirection fadeDirection = Fader.FadeDirection.In;
    private IClient iclient;
    private Manager Manager;

    private void Awake()
    {
        Manager = Manager.Instance;
        iclient = new MaterialFader(gameobject);
        iclient.fadeSpeed = 0.8f;
    }

    private void Start()
    {
    }

    private void OnEnable()
    {
        if (Setting.Instance.ApplicationTyp != Setting.Application.TiefseeUfer) return;
        StartCoroutine(Fade(fadeDirection));
        Manager.EnvironmentLevelHalfChanged += OnEnvironmentLevelHalfChanged;
    }

    private void OnEnvironmentLevelHalfChanged(object sender, Manager.EnvironmentLevelEventArgs e)
    {
        if (fadeDirection == Fader.FadeDirection.In)
        {
            iclient.fadeSpeed = fadeOutSpeed;
            StartCoroutine(Fade(Fader.FadeDirection.Out));
            fadeDirection = Fader.FadeDirection.Out;
        }

        else
        {
            iclient.fadeSpeed = fadeInSpeed;
            StartCoroutine(Fade(Fader.FadeDirection.In));
            fadeDirection = Fader.FadeDirection.In;
        }

    }

    protected IEnumerator Fade(Fader.FadeDirection fadeDirection)
    {
        float alpha = (fadeDirection == Fader.FadeDirection.Out) ? 1 : 0;
        float fadeEndValue = (fadeDirection == Fader.FadeDirection.Out) ? 0 : 1;

        if (fadeDirection == Fader.FadeDirection.Out)
        {
            while (alpha >= fadeEndValue)
            {
                SetColorImage(ref alpha, fadeDirection);
                yield return null; // make the loop wait until the next frame before continuing
            }
            gameobject.SetActive(false);
        }
        else if (fadeDirection == Fader.FadeDirection.In)
        {
            gameobject.SetActive(true);
            while (alpha <= fadeEndValue)
            {
                SetColorImage(ref alpha, fadeDirection);
                yield return null;
            }
        }
    }

    protected void SetColorImage(ref float alpha, Fader.FadeDirection fadeDirection)
    {
        iclient.SetColorImage(ref alpha, fadeDirection);
    }

    /*
    protected void SetColorImage(ref float alpha, FadeDirection fadeDirection)
    {
        
        if (material != null)
        {
            material.color = new Color(material.color.r, material.color.g, material.color.b, alpha);
            alpha += Time.deltaTime * (1.0f / fadeSpeed) * ((fadeDirection == FadeDirection.Out) ? -1 : 1);
        }
       
    }
    */
}
