﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantsAnimator : MonoBehaviour {
    [SerializeField] GameObject[] Plants;
    Manager manager;
	// Use this for initialization
	private void Start () {

      //  yield return new WaitForSeconds(1)
        manager.OnPlantsAnimationStarted();
    }
    private void Awake()
    {
        manager = Manager.Instance;
        foreach (GameObject plant in Plants)
        {
            plant.AddComponent<SinusWave>();
        }
    }
  
}
