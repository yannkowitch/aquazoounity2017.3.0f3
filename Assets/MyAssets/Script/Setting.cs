﻿using UnityEngine;
using Utils;

public class Setting : Singleton<Setting>
{
    [SerializeField] float levelTransitionTime = 15;
    [SerializeField, Tooltip("max amount of fish"), Range(0, 30)] int fishAmount = 10;

    [SerializeField] CalibrationOption calibrationOption = CalibrationOption.wholeSystem;
    [SerializeField] Application applicationTyp = Application.TiefseeUfer;
    [SerializeField] private Configuration applicationMode = Configuration.Debug;
    [SerializeField] private Floor detectedFloor = Floor.fromKinect;

    [SerializeField] private bool isDistanceToFloorCheck = true;


    [SerializeField] private double floortoJointMinDistance = 0.25f;
    [SerializeField] private bool autoHerunterfahrenActiv = true;
    [SerializeField] private bool userFootTracerActiv = true;
    [SerializeField] private bool mouseTracerActiv = true;
    [SerializeField] private bool animalTracerActiv = true;
    //  [SerializeField] private bool buttonTracerActiv = false;

    [SerializeField] float waitingTimeToCalibrateProjectorUnityCam = 2f;
    [SerializeField] float waitingTimeToCalibrateKinectUnity = 2f;

    public float WaitingTimeBeforCalibratingProjectorUnity
    {
        set
        {
            waitingTimeToCalibrateProjectorUnityCam = value;
        }
        get
        {
            return waitingTimeToCalibrateProjectorUnityCam;
        }
    }
    public float WaitingTimeBeforCalibratingKinectUnity
    {
        set
        {
            waitingTimeToCalibrateKinectUnity = value;
        }
        get
        {
            return waitingTimeToCalibrateKinectUnity;
        }
    }

    public double FloortoFootMinDistance
    {
        set
        {
            floortoJointMinDistance = value;
        }
        get
        {
            return floortoJointMinDistance;
        }
    }

    public bool IsDistanceToFloorCheck
    {
        set
        {
            isDistanceToFloorCheck = value;
        }
        get
        {
            return isDistanceToFloorCheck;
        }
    }

    public bool AnimalTrackerActiv
    {
        set
        {
            animalTracerActiv = value;
        }
        get
        {
            return animalTracerActiv;
        }
    }

    public bool MouseTrackerActiv
    {
        set
        {
            mouseTracerActiv = value;
        }
        get
        {
            return mouseTracerActiv;
        }
    }

    public bool UserTrackerActiv
    {
        set
        {
            userFootTracerActiv = value;
        }
        get
        {
            return userFootTracerActiv;
        }
    }

    public float LevelTransitionTime
    {
        set
        {
            levelTransitionTime = value;
            //Debug.Log(levelTransitionTime);
        }
        get
        {
            return levelTransitionTime;
        }
    }

    public int FishAmount
    {
        set
        {
            fishAmount = value;
            //Debug.Log(levelTransitionTime);
        }
        get
        {
            return fishAmount;
        }
    }

    public Configuration ApplicationMode
    {
        set
        {
            applicationMode = value;
        }
        get
        {
            return applicationMode;
        }
    }

    public Floor DetectedFloor
    {
        set
        {
            detectedFloor = value;
        }
        get
        {
            return detectedFloor;
        }
    }

    public Application ApplicationTyp
    {
        set
        {

            applicationTyp = value;
        }
        get
        {
            return applicationTyp;
        }
    }

    public CalibrationOption CurrentCalibrationOption
    {
        set
        {
            calibrationOption = value;
        }
        get
        {
            return calibrationOption;
        }
    }

    public bool AutoHerunterfahrenActiv
    {
        get
        {
            return autoHerunterfahrenActiv;
        }

        set
        {
            autoHerunterfahrenActiv = value;
        }
    }

    public enum Floor
    {
        fromKinect

    }

    public enum Configuration
    {
        Debug,
        Release
    }

    public enum Application
    {
        TiefseeUfer,
        Land
    }

    public enum CalibrationOption
    {
        wholeSystem,
        kinect_unity,
        pro_unity
    }


    // Use this for initialization
    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

}
