﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class KinectUnityCalibrationAssistance : CalibrationAssistanceBase
{
    [SerializeField] private Text LeftFootPositionText;
    [SerializeField] private Text RightFootPositionText;
    // Manager manager;

    private void Start()
    {
        manager.SceneIsStarted();
        base.StartBase();
    }

    private void Awake()
    {      
        base.AwakeBase();       
    }

    private void OnEnable()
    {
        manager.TextTypingEnded += OnTextTypingEnded;
        manager.PointCaptured += OnPointCaptured;
        manager.PointsCaptured += OnPointsCaptured;
        // manager.OnPointCaptureBegining += OnPointCaptureBegining;
        manager.leftFootTracked += OnleftFootTracked;
        manager.RightFootTracked += OnRightFootTracked;
        manager.SavedRecordedPointsLoaded += OnSavedRecordedPointsLoaded;
    }

    private void OnDisable()
    {
        manager.TextTypingEnded -= OnTextTypingEnded;
        manager.PointCaptured -= OnPointCaptured;
        manager.PointsCaptured -= OnPointsCaptured;
        //  manager.OnPointCaptureBegining -= OnPointCaptureBegining;
        manager.leftFootTracked -= OnleftFootTracked;
        manager.RightFootTracked -= OnRightFootTracked;
        manager.SavedRecordedPointsLoaded -= OnSavedRecordedPointsLoaded;
    }

    private void OnSavedRecordedPointsLoaded(object sender, EventArgs e)
    {
        if (instructions.Length == 0) return;

        dialogAssistance.TypeText(instructions[0], 1);
    }

    private void OnleftFootTracked(object sender, Manager.KinectSensorEventArgs e)
    {
        if (LeftFootPositionText != null)
            LeftFootPositionText.text = e.leftFootPositionInUnity.ToString();
    }

    private void OnRightFootTracked(object sender, Manager.KinectSensorEventArgs e)
    {
        if (RightFootPositionText != null)
            RightFootPositionText.text = e.rightFootPositionInUnity.ToString();

    }

    private void OnTextTypingEnded(object sender, TextTypingEventArgs e)
    {
        if (instructions.Length == 0) return;

        if (e.Text.Equals(instructions[0]))
        {
            dialogAssistance.TypeText(instructions[1], 7f);
        }
        else if (e.Text.Equals(instructions[5]))
        {
            dialogAssistance.ActivateDialogCanvas(false, 3f);
        }
    }
    /*
    private void OnPointCaptureBegining(object sender, EventArgs e)
    {
        if (instructions.Length == 0) return;

        DialogAssistenz.TypeText(instructions[0], 1);
    }
    */

    private void OnPointsCaptured(object sender, Manager.UnityKinectDataCorrespondenceAcquitionEventArgs e)
    {
        if (instructions.Length == 0) return;

        dialogAssistance.TypeText(instructions[5], 1);
    }



    private void OnPointCaptured(object sender, Manager.UnityKinectDataCorrespondenceAcquitionEventArgs e)
    {
        if (instructions.Length == 0) return;

        string lastTypedText = dialogAssistance.lastTypedText;


        if (lastTypedText == instructions[1])
        {
            dialogAssistance.TypeText(instructions[2], 0.1f);
        }
        else if (lastTypedText == instructions[2])
        {
            dialogAssistance.TypeText(instructions[3], 0.1f);
        }
        else if (lastTypedText == instructions[3])
        {
            dialogAssistance.TypeText(instructions[4], 0.1f);
        }
    }


}
