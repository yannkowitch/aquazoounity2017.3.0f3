﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class CalibrationAssistanceBase : MonoBehaviour
{

    [SerializeField] protected GameObject Dialog;
    [SerializeField] protected Text TextUI;
    [SerializeField] protected string[] instructions;
    protected DialogAssistance dialogAssistance;
    protected Manager manager;
    //protected bool isTextTyping;

    // Use this for initialization
    protected void StartBase()
    {
        dialogAssistance = FindObjectOfType<DialogAssistance>();
        if (TextUI == null) throw new Exception(" no Object of type DialogAssistenz founded");


        if (Dialog == null) throw new Exception(" DialogGameobject reference is not set to an Instance");
        dialogAssistance.Dialog = this.Dialog;


        if (TextUI == null) throw new Exception(" DialogAssistenz reference is not set to an Instance");
        dialogAssistance.TextUI = this.TextUI;

    }

    protected void AwakeBase()
    {
        manager = Manager.Instance;
    }
}
