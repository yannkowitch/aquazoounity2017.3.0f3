﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextFXTiefSee : TextTyper
{
    [SerializeField] string[] sentences;
    // [SerializeField] Text[] textComponents;
    //[SerializeField] TextMesh[] textMeshComponents;
    [SerializeField] TextMesh mTextMesh;
    private Manager manager;
    private Setting setting;
   // private Environment.Level environmentLevel;


    // Use this for initialization
    void Awake()
    {
        manager = Manager.Instance;
        setting = Setting.Instance;
        // textComponent = GameObject.FindGameObjectWithTag("text").GetComponent<Text>();

    }

    private void OnEnable()
    {
        if (setting.ApplicationTyp == Setting.Application.Land) return;

        manager.OnTriggerButtonDestroyed += OnTriggerButtonDestroyed;
        manager.EnvironmentLevelHalfChanged += OnEnvironmentLevelHalfChanged;
        manager.EnvironmentLevelCompletelyChanged += OnEnvironmentLevelCompletelyChanged;
        manager.TriggerButtonCreated += OnTriggerButtonCreated;
        manager.EnvironmentLevelInitialized += OnEnvironmentLevelInitialized;
        manager.OnEusthenopteroninIdleState += OnEusthenopteroninIdleState;
        manager.TiktalikinIdleState += OnTiktalikinIdleState;
        manager.OnEusthenopteronCreated += OnEusthenopteronCreated;
        manager.TiktalikCreated += OnTiktalikCreated;
    }


    private void OnDisable()
    {
        if (setting.ApplicationTyp == Setting.Application.Land) return;

        manager.OnTriggerButtonDestroyed -= OnTriggerButtonDestroyed;
        manager.EnvironmentLevelHalfChanged -= OnEnvironmentLevelHalfChanged;
        manager.EnvironmentLevelCompletelyChanged -= OnEnvironmentLevelCompletelyChanged;
        manager.TriggerButtonCreated -= OnTriggerButtonCreated;
        manager.EnvironmentLevelInitialized -= OnEnvironmentLevelInitialized;
        manager.OnEusthenopteroninIdleState -= OnEusthenopteroninIdleState;
        manager.TiktalikinIdleState -= OnTiktalikinIdleState;
        manager.OnEusthenopteronCreated -= OnEusthenopteronCreated;
        manager.TiktalikCreated -= OnTiktalikCreated;
    }

    private void OnEnvironmentLevelInitialized(object sender, Manager.EnvironmentLevelEventArgs e)
    {
        //environmentLevel = e.environmentLevel;
        TypeText(mTextMesh, sentences[0]);
    }

    private void OnTiktalikCreated(object sender, Manager.TiktalikEventArgs e)
    {
        TypeText(mTextMesh, sentences[1]);
    }

    private void OnEusthenopteronCreated(object sender, Manager.EusthenopteronEventArgs e)
    {
        TypeText(mTextMesh, sentences[0]);
    }

    private void OnTiktalikinIdleState(object sender, Manager.TiktalikEventArgs e)
    {

    }

    private void OnEusthenopteroninIdleState(object sender, Manager.EusthenopteronEventArgs e)
    {

    }

    private void OnTriggerButtonCreated(object sender, Manager.TriggerButtonEventArgs e)
    {

    }

    private void OnEnvironmentLevelCompletelyChanged(object sender, Manager.EnvironmentLevelEventArgs e)
    {
        //environmentLevel = e.environmentLevel;

    }

    private void OnEnvironmentLevelHalfChanged(object sender, Manager.EnvironmentLevelEventArgs e)
    {

    }

    private void OnTriggerButtonDestroyed(object sender, Manager.TriggerButtonEventArgs e)
    {

    }


}
