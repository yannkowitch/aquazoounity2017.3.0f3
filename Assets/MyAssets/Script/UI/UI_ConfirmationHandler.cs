﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UI_ConfirmationHandler : MonoBehaviour
{
    [SerializeField] GameObject ConfirmationPanel;
    [SerializeField] Animator animator;
    [SerializeField] Text maxTime;
    private int startTime;
    [SerializeField] Text Message;
    private int waitingTime;
    protected readonly int m_HashActivePara = Animator.StringToHash("Active");
    // Use this for initialization
    void Start()
    {
        DateTimeManager dateTimeManager = FindObjectOfType<DateTimeManager>();
        waitingTime = dateTimeManager.WaitingTime;
        if (dateTimeManager == null) {
            Debug.LogError("no component DateTimeManager founded");
        }
        startTime = waitingTime;
    }
    private void OnEnable()
    {
        Manager.Instance.MaxTimeReached += MaxTimeReached;
    }

    private void MaxTimeReached(object sender, DateTimeManagerEventArgs e)
    {
        maxTime.text = "festgelegte Ausschaltzeit : " + e.MaxTime;
        StartCoroutine(StartTimer());
    }

    public void OnAppExitConfirmed(bool value)
    {
        animator.SetBool(m_HashActivePara, value);
    }

    private void OnGUI()
    {
        Message.text = "System wird in " + waitingTime + " automatisch ausgeschatet !!";
    }

    private IEnumerator StartTimer()
    {
        while (waitingTime > 0) {
            waitingTime--;
            yield return new WaitForSeconds(1);
        }
        Debug.Log("time is 0 ");
    }

}
