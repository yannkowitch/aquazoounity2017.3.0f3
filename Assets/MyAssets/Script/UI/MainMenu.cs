﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
#endif

public class MainMenu : MonoBehaviour
{
    private Setting setting;
    private bool mChangeLevel = true;
    public bool isSettingsPanelOpen { set; get; }

    public int Time
    {
        get
        {
            return time;
        }


    }

    private Manager manager;
    private bool autoStartActiv = true;
    [SerializeField] private Button calibrateButton;
    [SerializeField] private Button simulationButton;
    [SerializeField] private Button settingButton;
    [SerializeField] GameObject cover;
    [SerializeField] Text coverLabel;
    [SerializeField] int time = 10;

    public void Awake()
    {
        manager = Manager.Instance;
        setting = Setting.Instance;

    }

    private void OnEnable()
    {
        manager.SettingsPanelOpened += OnSettingsPanelOpened;
        manager.SettingsPanelClosed += OnSettingsPanelClosed;
    }

    private IEnumerator Start()
    {
        var startTime = time;
        StartCoroutine(Timer());

        yield return new WaitForSeconds(startTime);

        if (autoStartActiv == true) {
            StartSimaulation();
        }

    }
    private IEnumerator Timer()
    {
        while (time > 0) {
            time--;
            yield return new WaitForSeconds(1);
        }
        Debug.Log("time is 0 ");
    }

    private void OnGUI()
    {
        coverLabel.text = "AUTO START IN  " + time.ToString();
    }
    public void OnSimulationButtonClicked()
    {
        if (isSettingsPanelOpen == true) return;
        if (mChangeLevel == false) return;
        StartSimaulation();

        FindObjectOfType<SettingController>().DesastivateDevelopperPanel();
        mChangeLevel = false;
    }

    private void StartSimaulation()
    {
#if UNITY_EDITOR
        LoadNextSceneAsync(3);

#endif
#if UNITY_EDITOR == false
            LoadLevelWhenNo_UNITY_EDITOR();
#endif
    }

    public void DesactivateAutoActiveStart()
    {
        if (time <= 0) return;
        autoStartActiv = false;
        cover.SetActive(false);
    }

    private void OnSettingsPanelClosed(object sender, EventArgs e)
    {
        isSettingsPanelOpen = false;
        calibrateButton.interactable = true;
        simulationButton.interactable = true;
    }

    private void OnSettingsPanelOpened(object sender, EventArgs e)
    {
        isSettingsPanelOpen = true;
        calibrateButton.interactable = false;
        simulationButton.interactable = false;
    }

    private void LoadLevelWhenNo_UNITY_EDITOR()
    {
        // if (mChangeLevel == false) return;
        int sceneIndex = 0;

        switch (setting.ApplicationTyp) {
            case Setting.Application.TiefseeUfer:
                if (!File.Exists("recording.xml")) {
                    sceneIndex = 1;
                }
                else if (File.Exists("recording.xml")) {
                    sceneIndex = !File.Exists("capturing.xml") ? 2 : 3;
                }
                break;
            case Setting.Application.Land:

                sceneIndex = !File.Exists("capturing.xml") ? 2 : 3;
                break;
            default:
                break;
        }


        LoadNextSceneAsync(sceneIndex);
        //  mChangeLevel = false;
    }

    public void OnCalibrationButtonClicked()
    {
        if (isSettingsPanelOpen == true) return;
        if (mChangeLevel == false) return;

        int sceneIndex = 0;
        if (setting.ApplicationTyp == Setting.Application.TiefseeUfer) {
            switch (setting.CurrentCalibrationOption) {
                case Setting.CalibrationOption.wholeSystem:
                    sceneIndex = 1;
                    break;

                case Setting.CalibrationOption.kinect_unity:
                    sceneIndex = 2;
                    break;

                case Setting.CalibrationOption.pro_unity:
                    sceneIndex = 1;
                    break;

                default:
                    break;
            }

        }
        else if (setting.ApplicationTyp == Setting.Application.Land) {
            sceneIndex = 2;
        }

        LoadNextSceneAsync(sceneIndex);
        FindObjectOfType<SettingController>().DesastivateDevelopperPanel();
        mChangeLevel = false;

    }


    public void OnSettingButtonClicked()
    {
        isSettingsPanelOpen = (isSettingsPanelOpen == true) ? false : true;
    }

    public void OnExitButtonClicked()
    {
        Debug.Log("quit");
        if (mChangeLevel == false) return;

        StartCoroutine(FindObjectOfType<SceneFader>().QuitApplication());
        /*
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
        */

        FindObjectOfType<SettingController>().DesastivateDevelopperPanel();
        mChangeLevel = false;
    }

    private void LoadNextSceneAsync(int i)
    {
        StartCoroutine(FindObjectOfType<SceneFader>().FadeAndLoadScene(Fader.FadeDirection.In, i));
    }

    private void OnDisable()
    {
        manager.SettingsPanelOpened -= OnSettingsPanelOpened;
        manager.SettingsPanelClosed -= OnSettingsPanelClosed;

    }
}
