﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectorKinectCalibrationAssitance : CalibrationAssistanceBase
{
    
    private void Start()
    {
        base.StartBase();
    }

    // Update is called once per frame
    private void Awake()
    {
        base.AwakeBase();
    }

    private void OnEnable()
    {
        manager.TextTypingEnded += OnTextTypingEnded;
      
    }

    private void OnDisable()
    {
        manager.TextTypingEnded -= OnTextTypingEnded;
      
    }

    private void OnTextTypingEnded(object sender, TextTypingEventArgs e)
    {
        if (instructions.Length == 0) return;
        if (e.Text.Equals(instructions[0]))
        {
            dialogAssistance.TypeText(instructions[1], 7f);
        }
    }
}
