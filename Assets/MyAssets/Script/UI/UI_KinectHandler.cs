﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UI_KinectHandler : MonoBehaviour
{
    [SerializeField] private Text LeftFootPositionStr;
    [SerializeField] private Text RightFootPositionStr;
    [SerializeField] private Text LeftFootToFloorDistStr;
    [SerializeField] private Text RightFootToFloorDistStr;
    [SerializeField] private Text MinDistanceStr;
    [SerializeField] private Text CalibrationStatusStr;
    private GameObject kinectPanel;
    [SerializeField] private Slider sliderGroundedDist;
    // [SerializeField] private RipplesFx ripplesFx;
    private Coroutine m_ActivateCalibrationStatusStrCoroutine, m_DesactivateKinectPanelCoroutine;
    private Manager manager;
    private Setting setting;
    // Use this for initialization

    private void Awake()
    {
        manager = Manager.Instance;
        setting = Setting.Instance;
        kinectPanel = GameObject.FindGameObjectWithTag("kinectPanel");

        if (sliderGroundedDist != null) {
            //  ripplesFx = FindObjectOfType<RipplesFx>();
            //  sliderGroundedDist.value = (float)ripplesFx.FloortoFootMinDistance;
            sliderGroundedDist.value = (float)setting.FloortoFootMinDistance;

        }

        if (CalibrationStatusStr != null)
            CalibrationStatusStr.enabled = false;

        if (kinectPanel != null) {
            if (m_DesactivateKinectPanelCoroutine != null) {
                StopCoroutine(DesactivateKinectPanel(1.5f));
                m_DesactivateKinectPanelCoroutine = null;
            }
            m_DesactivateKinectPanelCoroutine = StartCoroutine(DesactivateKinectPanel(1.5f));
        }


    }

    private IEnumerator DesactivateKinectPanel(float waitingTime)
    {
        yield return new WaitForSeconds(waitingTime);
        kinectPanel.SetActive(false);
    }

    private IEnumerator ActivateCalibrationStatusStr(float waitingTime)
    {
        yield return new WaitForSeconds(waitingTime);
        CalibrationStatusStr.enabled = true;
    }

    private void OnEnable()
    {
        manager.leftFootTracked += OnleftFootTracked;
        manager.RightFootTracked += OnRightFootTracked;

    }

    private void OnRightFootTracked(object sender, Manager.KinectSensorEventArgs e)
    {
        if (RightFootPositionStr != null)
            RightFootPositionStr.text = e.rightFootPositionInUnity.ToString();

        if (RightFootToFloorDistStr != null)
            RightFootToFloorDistStr.text = "";
    }

    private void OnleftFootTracked(object sender, Manager.KinectSensorEventArgs e)
    {
        if (LeftFootPositionStr != null)
            LeftFootPositionStr.text = e.leftFootPositionInUnity.ToString();

        if (LeftFootToFloorDistStr != null)
            LeftFootToFloorDistStr.text = "";

    }

    private void OnPointsCaptured(ProCamData data)
    {
        if (kinectPanel != null)
            StartCoroutine(DesactivateKinectPanel(0.1f));

        if (CalibrationStatusStr != null) {
            if (m_ActivateCalibrationStatusStrCoroutine != null) {
                StopCoroutine(ActivateCalibrationStatusStr(0.25f));
                m_ActivateCalibrationStatusStrCoroutine = null;
            }
            m_ActivateCalibrationStatusStrCoroutine = StartCoroutine(ActivateCalibrationStatusStr(0.25f));
        }

    }

    private void DisplayRightFootState(Vector3 position, int userID)
    {
        if (RightFootPositionStr != null)
            RightFootPositionStr.text = position.ToString();

        if (RightFootToFloorDistStr != null)
            RightFootToFloorDistStr.text = "";
    }

    private void OnDisable()
    {
        manager.leftFootTracked -= OnleftFootTracked;
        manager.RightFootTracked -= OnRightFootTracked;
        //   EventsManager.OnPointsCaptured -= OnPointsCaptured;     
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space)) {
            if (setting.ApplicationMode == Setting.Configuration.Debug) {

                if (kinectPanel != null) {
                    if (kinectPanel.activeInHierarchy) {
                        kinectPanel.SetActive(false);
                    }
                    else
                        kinectPanel.SetActive(true);
                }

            }


        }

        if (sliderGroundedDist != null) {
            setting.FloortoFootMinDistance = sliderGroundedDist.value;

            sliderGroundedDist.value = (float)setting.FloortoFootMinDistance;


            MinDistanceStr.text = (sliderGroundedDist.value * 100).ToString() + " cm";
        }
    }
}
