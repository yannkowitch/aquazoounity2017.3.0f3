﻿using System.Collections;
using UnityEngine;

public class Timer : TextTyper
{
    [SerializeField] int Time1;
    [SerializeField] int Time2;
    [SerializeField] int step;
    //[SerializeField] int result;
    [SerializeField] TextMesh mTextMesh;
    [SerializeField] int holder;
    Coroutine timer1Coroutine = null;
    Coroutine timer2Coroutine = null;
    // Use this for initialization
    private Manager manager;
    [SerializeField] private Phase phase = Phase.two;

    void Awake()
    {
        manager = Manager.Instance;
    }


    enum Phase
    {

        one,
        two
    }

    private void StartTimerOne()
    {
        if (timer1Coroutine != null) {
            StopCoroutine(StartTimer1IE());
            timer1Coroutine = null;
        }

        timer1Coroutine = StartCoroutine(StartTimer1IE());
    }
    private void StartTimerTwo()
    {
        if (timer2Coroutine != null) {
            StopCoroutine(StartTimer2IE());
            timer2Coroutine = null;
        }

        timer2Coroutine = StartCoroutine(StartTimer2IE());
    }

    private void OnEnable()
    {
        manager.EnvironmentLevelInitialized += OnEnvironmentLevelInitialized;

        manager.OnTriggerButtonDestroyed += OnTriggerButtonDestroyed;

    }

    private void OnDisable()
    {
        manager.EnvironmentLevelInitialized -= OnEnvironmentLevelInitialized;
        manager.OnTriggerButtonDestroyed -= OnTriggerButtonDestroyed;

    }

    private void OnTriggerButtonDestroyed(object sender, Manager.TriggerButtonEventArgs e)
    {
        if (phase == Phase.one) {
            StartTimerOne();

            phase = Phase.two;
        }

        else if (phase == Phase.two) {
            StartTimerTwo();

            phase = Phase.one;
        }


    }

    private void OnEnvironmentLevelInitialized(object sender, Manager.EnvironmentLevelEventArgs e)
    {
        if (phase == Phase.one)
            TypeText(mTextMesh, "VOR " + Time1 + "  JAHREN");
        else if (phase == Phase.two)
            TypeText(mTextMesh, "VOR " + Time2 + "  JAHREN");
    }

    private IEnumerator StartTimer1IE()
    {
        holder = Time1;
        if (holder > Time2) yield return null;
        manager.OnTimerStarted();
        Debug.Log("start time one");
        while (holder < Time2) {
            holder += step;
            if (holder > Time2) holder = Time2;
            //Debug.Log(holder);
            mTextMesh.text = "VOR " + holder + "  JAHREN";
            yield return null;

        }
        manager.OnTimerEnded();
        Debug.Log("fertig");
    }

    private IEnumerator StartTimer2IE()
    {
        holder = Time2;
        if (holder < Time1) yield return null;
        manager.OnTimerStarted();

        while (holder > Time1) {
            holder -= step;
            if (holder < Time1) holder = Time1;
            //Debug.Log(holder);
            mTextMesh.text = "VOR " + holder + "  JAHREN";
            yield return null;

        }
        manager.OnTimerEnded();
        Debug.Log("fertig");
    }


}
