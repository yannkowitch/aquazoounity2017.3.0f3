﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalPositionRelativToStairTracker : MonoBehaviour
{
    private Manager manager;
 
    private void Awake()
    {
        manager = Manager.Instance;
    }


    private void OnTriggerEnter(Collider other)
    {        
        manager.OnAnimalTrackerTriggerColliderEnter(other);
    }

    private void OnTriggerExit(Collider other)
    {       
        manager.OnAnimalTrackerTriggerColliderExit(other);
    }
}
